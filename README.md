# README #
This readme is written in markdown, and is easier to read from a browser by opening `DDCPM.html` in the `doc` directory, or a pdf
viewer by opening `DDCPM.pdf` in the `doc` directory. To do so you first need to:
- Install doxygen
- Navigate to doc/doxygen and call make

## Domain Decomposition solvers for the Closest Point Method (DD-CPM) ##
This software provides an MPI capable implementation of the closest point method written on top of the PETSc framework for scientific
computing.

Custom Schwarz type domain decomposition solvers to handle elliptic problems arising from the closest point method are included.
Dirichlet and Robin transmission conditions have both been implemented to provide restricted additive Schwarz (RAS) and optimized
restricted additive Schwarz (ORAS) solvers. These solvers can be embedded within any of the PETSc Krylov solvers as preconditioners.

## Getting started ##
Starting with the DD-CPM library proceeds in three main steps: installing dependencies, building this library, and running the
example problems.

### Dependencies ###
The user should install each of these dependencies prior to building the DD-CPM library. Note that PETSc is capable of installing
other dependencies and can thus shorten this list somewhat. Also, on personal systems most of these can be installed through whatever
package manager is available, and on managed compute clusters most, if not all, of these will be available as modules.

- PETSc: Visit <https://www.mcs.anl.gov/petsc/documentation/installation.html> for installation instructions. This is by far the most
involved installation step, and as such, some additional instructions can be found below.
- MPI: MPICH2/3, and OpenMPI have all been tested and work. If you don't have MPI already installed, you should install it with PETSc
by adding `--download-mpich` as a configuration flag.
- HDF5: Versions 1.12.0 is known to work, though any version after 1.10.0 should work. The parallel version of HDF5 must be installed
and needs to be compatible with your version of MPI. HDF5 can also be installed with PETSc by passing `--download-hdf5` as a
configuration flag.
- MayaVI (Optional): The emitted hdf5 files can be visualized using MayaVI through the provided python script. This tool can be
installed either through pip via `pip install --user mayavi` or through the package manager on many systems. Refer to
<https://docs.enthought.com/mayavi/mayavi/> for more information.

#### Notes on installing PETSc ####
PETSc certainly has the most involved installation process out of the dependencies. The PETSc installation instructions
<https://www.mcs.anl.gov/petsc/documentation/installation.html> are quite detailed, though we add some commentary here. The
`./configure` step handles the majority of the work, and has many useful flags for setting various options. PETSc can download other
packages by adding `--download-PACKAGENAME` to the configure command. Some potentially useful flags of this type are:
- `--download-mpich`
- `--download-hdf5`
- `--download-parmetis`
- `--download-ptscotch`
- `--download-chaco`
- `--download-suitesparse`
- `--download-superlu`
- `--download-superlu_dist` (Distributed version of the former)
- `--download-mumps`

with the first two being dependencies this software has. The three following packages are mesh partitioners, and at least one of these should be included so that reasonable subdomains can be generated. The remaining packages are all sparse direct solvers that may be of use in
different circumstances.

A configuration where the dependencies are already present on the system, with a few of the optional solvers desired and optimization
turned on, might look like:

`./configure PETSC_ARCH=gnu-opt COPTFLAGS=-O3 -march=native -mtune=native CXXOPTFLAGS=-O3 -march=native -mtune=native FOPTFLAGS=-O3 --with-cc=mpicc --with-cxx=mpicxx --with-cxx-dialect=C++11 --with-fc=mpif90 --with-debugging=0 --download-parmetis --download-suitesparse --download-superlu`

PETSc could be configured for a system needing all dependencies by the configure command:

`./configure --with-cxx-dialect=C++11  --download-f2cblaslapack --download-cmake --download-mpich --download-parmetis --download-scalapack --download-mumps --download-hdf5`

Note that PETSc relies on the environment variables `PETSC_DIR` and `PETSC_ARCH`. The first is the full path to the PETSc directory,
and the second is the name of a subdirectory inside of `PETSC_DIR` that corresponds to the configuration step performed. In the
first configuration example, `PETSC_ARCH=gnu-opt` will override the default `PETSC_ARCH` to set the name `gnu-opt`. The `PETSC_ARCH`
variable exists to allow a user to have several PETSc configurations on one system without conflict.

Finally, after the configure step, PETSc will guide you through the compilation process by emitting the next required commands for
you to copy/paste.

### Building DD-CPM ###
Installation is only described for Linux/unix type systems. MacOS installation should proceed in mostly the same way. Windows based
installations will require some work, but if you made it through the dependencies, then the following should help enough.

After downloading/cloning DD-CPM, change to its directory (where this README lives). Ensure that the environment variables
`PETSC_DIR` and `PETSC_ARCH` are set from the PETSc installation step. Running `echo $PETSC_DIR` is a good way to check that these
are still set. These may be set via `export PETSC_DIR=/path/to/petsc PETSC_ARCH=arch-name`. Verify that `mpirun` is available in
your path (e.g. by running `whereis mpirun`). If you elected to install MPI from PETSc, you may need to create an alias to `mpirun`
as `alias mpirun=$PETSC_DIR/$PETSC_ARCH/bin/mpirun`.

The following commands will build the library and examples:
1. Build the dd-cpm library and all of the examples by running `make examples`
   - You can also build only the library using `make lib` if you don't care about the examples
   - You can add the flag `-j` to build in parallel
2. Test the build by running `make test`

### Running the examples ###
The `bin` subdirectory should now have these binaries present:
- DDCPPoisson.ex
- DDCPHeat.ex
- DDCPBiharmonic.ex
- DDCPGrayScott.ex
- DDCPSchnackenberg.ex
- DDCPFitzhughNagumo.ex

The source files producing these examples are present in the `examples` subdirectory. In the remainder of this section, we catalog
example calls to a couple of these binaries and demonstrate some of the available options.

Calling these examples generally fit this pattern:
- `mpirun -n` *nprocs* `bin/DDCPExample -infile` *path/to/inputfile* `-ksp_converged_reason -cp_pc_ras`
It is critical that the number of subdomains is a multiple of the number of processors if the DD solvers/preconditioners are to be
used. If your MPI installation provides `mpirun` instead of `mpirun`, substitute it as necessary in the following.

There are many optional flags to override values set in the input file. Additionally, most PETSc flags can be set as well (like
`-ksp_converged_reason` above). The flag `-help` will show all options that can be set, as well as give several sample commands for
running each example. By default the examples will write output files to the `data` subdirectory. This behavior can be overidden in
three different ways:
- Change the line `FileBase data` to `FileBase path/to/desired/location` in the input file used
- Add the flag `-pp_fbase path/to/desired/location` to the command used when running the example
- Make `data` a symlink to your desired location, e.g. `ln -s path/to/desired/location data`
Note also that relative and absolute paths both work, but relative paths are expanded with respect to where you call the executable
from, not where the executable or input files are located.

To solve the shifted Poisson equation on the circle using 2 processes, run:
- `mpirun -n 2 bin/DDCPPoisson.ex -infile inputFiles/circle.icpm -ksp_converged_reason -petscpartitioner_type parmetis -pc_type ddcpm`

Most settings involved can be overridden from the command line. For instance, to solve the same equation as before using 12
partitions and Robin transmission conditions one may run:
- `mpirun -n 2 bin/DDCPPoisson.ex -infile inputFiles/circle.icpm -ksp_converged_reason -mesh_nparts 12 -mesh_nover 4 -petscpartitioner_type parmetis -pc_type ddcpm -pc_ddcpm_robfo true`

The Gray-Scott reaction diffusion equation can be solved on a disc with homogeneous Neumann conditions via:
- `mpirun -n 4 bin/DDCPGrayScott.ex -infile inputFiles/disc.icpm -mesh_res 100 -mesh_nparts 12 -mesh_nover 4 -petscpartitioner_type parmetis -mesh_npoll 800 -pc_type ddcpm -pc_ddcpm_robfo true -time_final 6000 -ts_monitor`

In this example the CPM is only being used to enforce the boundary condition. Several new flags are needed to control the
time-stepping options. `-time_final 6000` sets the terminal time of the method to 6000. The flag `-time_dt 0.05` sets the
initial time step size to 0.05, though the default time stepper is adaptive and this step size will change as the solution advances.
The additional option `-mesh_npoll 800` sets the post processing methods to write solution on the polling surface using 800
points in the angular direction and 400 points in the radial direction.

As a final example we solve the Schnackenberg reaction diffusion equation on a hemisphere. This illustrates how to implement user
defined surfaces and boundary conditions.
- `mpirun -n 4 bin/DDCPSchnackenberg.ex -infile inputFiles/user.icpm -mesh_res 20 -mesh_nparts 8 -mesh_nover 4 -petscpartitioner_type parmetis -pc_type ddcpm -time_final 100 -time_dt 1.e-3 -pp_plotfreq 100 -ts_monitor`

User defined surfaces are implemented by providing, at a minimum, a closest point function within the driver code. Mesh and operator
construction can be aided by also providing distance and surface normal functions. The driver for this example shows these functions
for a unit hemisphere defined for z>0. Additionally, the circular boundary is split into segments with homogeneous Dirichlet and
homogeneous Neumann conditions on different parts.

Sample calls for the other example driver programs can be found passing the `-help` flag to them.

## Troubleshooting ##
Problems inevitably arise when installing new software. Hopefully the solutions to any issues can be found by asking a few
questions.
### The build fails ###
- Are `PETSC_DIR` and `PETSC_ARCH` set correctly, i.e. do `echo $PETSC_DIR` and `echo $PETSC_ARCH` return correctly?
- Can some PETSc example problems be run successfully?

### The examples fail ###
- Does the system know how to find MPI, i.e. does `whereis mpirun` return empty?
  - Using `--download-mpich` during the PETSc configuration and build step may not make `mpirun` visible system-wide. Finding
  `mpirun` in the `PETSC_ARCH` and creating an alias: `alias mpirun=/path/to/mpirun` (absolute) can fix this.
- Are there multiple MPI implementations installed, i.e. does `whereis mpirun` return several options?
- Are the input files and ply files in readable locations?

