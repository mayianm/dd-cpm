#!/bin/bash

echo "Test Poisson solve on a circle in serial"
bin/DDCPPoisson.ex -infile test/circle.icpm -cp_pc_ras -ksp_converged_reason -dd_trans_robfo &> test/test1.out
if [ $(grep CONVERGED test/test1.out | wc -l) -eq 1 ]
then
    echo Pass
else
    echo Fail
fi

# Use nproc to set the number of ranks to use for parallel problems
# Set cap of 12 processes
# Defaults to 2 if coreutils is missing
np=2
if [ $(command -v nproc) ]
then
    np=$(nproc)
fi
nr=2
if [ $np -ge 12 ]
then
    nr=12
elif [ $np -ge 6 ]
then
    nr=6
elif [ $np -ge 4 ]
then
    nr=4
fi

echo "Using" $nr "processes for parallel tests"

echo "Test Poisson solve on a circle in parallel"
mpiexec --oversubscribe -n $nr bin/DDCPPoisson.ex -infile test/circle.icpm -cp_pc_ras -ksp_converged_reason -dd_trans_robfo &> test/test2.out
if [ $(grep CONVERGED test/test2.out | wc -l) -eq 1 ]
then
    echo Pass
else
    echo Fail
fi


echo "Test Poisson solve on a sphere in parallel"
mpiexec --oversubscribe -n $nr bin/DDCPPoisson.ex -infile test/sphere.icpm -cp_pc_ras -ksp_converged_reason -dd_trans_robfo &> test/test3.out
if [ $(grep CONVERGED test/test3.out | wc -l) -eq 1 ]
then
    echo Pass
else
    echo Fail
fi


echo "Test Poisson solve on a triangulated surface in parallel"
mpiexec --oversubscribe -n $nr bin/DDCPPoisson.ex -infile test/triang.icpm -cp_pc_ras -ksp_converged_reason -dd_trans_robfo &> test/test4.out
if [ $(grep CONVERGED test/test4.out | wc -l) -eq 1 ]
then
    echo Pass
else
    echo Fail
fi
