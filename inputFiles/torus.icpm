# Solve on a torus

MESH
Surface Torus
Resolution 20             # The desired resolution
Dimension 3               # Embedding dimension
InterpOrder 2             # Interpolation order
BoundingBox 50 50 40      # Upper corner of bounding box
Overlap 3                 # Overlap size
Partitions 24             # Number of partitions, no checking for number of processors
AlignBounds True          # Align subdomain boundaries
Poll 128                  # Number of poll points per dimension (roughly)

DOMAINDECOMPOSITION
RandomInitial False       # Random initialization
MaxIterations 8000        # Maximum DD solver iterations
Tolerance 1.e-6           # Tolerance on relative residual
Transmission DirFO        # Transmission conditions
OSMAlpha 32.0             # Alpha value for Robin conditions

TIME
Initial 0.0               # Initial time
Final 1.0                 # Final Time
Dt 1.e-6                  # Time step size
MaxSteps 5000             # Maximum steps to take

POSTPROCESS
IOLevel All               # Use cloud and/or polling surface mesh
PlotFrequency 10          # Frequency of plotting
FileBase data

OTHER
Verbosity 4               # Control program verbosity
Draw 0                    # Draw operators, none=0, global=1, global and local=2
