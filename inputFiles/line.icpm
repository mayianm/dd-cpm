# Solve on a line
# Used for simple validations I.e. the Robin BC scheme on a global problem

MESH
Surface Line
Resolution 256            # The desired resolution
Dimension 2               # Embedding dimension
InterpOrder 3             # Interpolation order
BoundingBox 520 520       # Upper corner of bounding box
Overlap 4                 # Overlap size
Partitions 12             # Number of partitions, no checking for number of processors
AlignBounds False         # Align subdomain boundaries
Poll 512                  # Number of polling points (roughly per dimension)

DOMAINDECOMPOSITION
RandomInitial False       # Random initialization
MaxIterations 5000        # Maximum DD solver iterations
Tolerance 1.e-6           # Tolerance on relative residual
Transmission RobFO        # Transmission conditions
OSMAlpha 1.0              # Alpha value for Robin conditions

TIME
Initial 0.0               # Initial time
Final 1.0                 # Final Time
Dt 0.001                  # Time step size
MaxSteps 5000             # Maximum steps to take

POSTPROCESS
IOLevel All               # Point cloud and polling surface
PlotFrequency 5           # Frequency of plotting
FileBase data

OTHER
Verbosity 4               # Control program verbosity
Draw 0                    # Draw operators, none=0, global=1, global and local=2
