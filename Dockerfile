FROM ubuntu:20.04
LABEL maintainer="Ian May <imay1@ucsc.edu>"

WORKDIR /tmp

RUN export DEBIAN_FRONTEND=noninteractive && \
    apt-get -qq update && \
    apt-get -yq --with-new-pkgs -o Dpkg::Options::="--force-confold" upgrade && \
    apt-get -y install \
    build-essential \
    git \
    cmake \
    python3-dev && \
    apt-get -y install \
    openmpi-bin \
    openmpi-common

ENV OMPI_ALLOW_RUN_AS_ROOT=1 OMPI_ALLOW_RUN_AS_ROOT_CONFIRM=1

WORKDIR /usr/local
RUN git clone -b release https://gitlab.com/petsc/petsc.git petsc
RUN cd petsc && \
    ./configure PETSC_ARCH=gnu-opt COPTFLAGS=-O3 -march=native -mtune=native CXXOPTFLAGS=-O3 -march=native -mtune=native FOPTFLAGS=-O3 --with-cc=mpicc --with-cxx=mpicxx --with-cxx-dialect=C++11 --with-fc=mpif90 --with-debugging=0 --download-hdf5 --download-fblaslapack=1 --download-metis --download-suitesparse --download-superlu --download-superlu_dist --download-scalapack --download-mumps && \
    make PETSC_DIR=/usr/local/petsc PETSC_ARCH=gnu-opt all

ENV PETSC_DIR=/usr/local/petsc PETSC_ARCH=gnu-opt

WORKDIR /usr/local
RUN git clone -b release https://mayianm@bitbucket.org/mayianm/dd-cpm.git dd-cpm && \
    cd dd-cpm && make examples

RUN rm -rf /tmp/*
    