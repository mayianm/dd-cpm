// Problem Class Implementation
// Author: Ian May
// Purpose: The actual system to be solved is defined here and solved here.
//          In the single domain case this takes in (a pointer to) a CPMesh 
//          object and (a pointer to) a DiffEq object and builds the associated
//          operators. In multidomain problems this also takes in all of the 
//          subdomain meshes and subproblem definitions then builds the needed 
//          MPI communicators between them

#include "Problem.H"
#include "PC_ddcpm.H"
#include "petscpc.h"
#include <cstddef>

namespace DDCPM {

  ProblemGlobal::ProblemGlobal(ProblemDefinition& a_probDef,CPMeshGlobal& a_mesh,
			       DiffEq& a_eq,bool a_transient):
    ProblemBase(a_probDef,a_mesh,a_eq),
    m_mesh(a_mesh),
    m_solComp(false),
    m_transient(a_transient),
    m_nPollLo(0),
    m_nPollUp(0),
    ts(NULL),
    J(NULL),
    Poll(NULL),
    pollSol(NULL),
    pollRhs(NULL)
  {
    PetscLogEvent GLOB_PROB;
    PetscLogEventRegister("Global ProblemGlobal",0,&GLOB_PROB);
    PetscLogEventBegin(GLOB_PROB,0,0,0,0);
  
    MPI_Comm_rank(PETSC_COMM_WORLD,&m_rank);
    MPI_Comm_size(PETSC_COMM_WORLD,&m_size);
    
    if (m_probDef.verbosity>0) {
      PPW_VZERO("\n\nConstruct global problem and operators\n");
    }
    
    try {
      // Build global operators
      buildOps();
      m_eq.flushStencils();
      if (m_probDef.draw>0) {
	drawOps();
      }

      // Register the custom preconditioner so it will be available
      PCRegister("ddcpm",PC_ddcpm::PCCreate_ddcpm);
      
      // Set up petsc solvers
      if (m_transient) {
        formTransientSolver();
      } else {
        formStationarySolver();
      }
      
    } catch(ErrorTrace& e) {
      PUSHERROR(e,"Global problem constructor failed");
      throw e;
    }
    PetscLogEventEnd(GLOB_PROB,0,0,0,0);
  }
  
  ProblemGlobal::~ProblemGlobal()
  {
    KSPDestroy(&ksp);
    VecDestroy(&sol);
    VecDestroy(&rhs);
    MatDestroy(&A);
    if (m_probDef.pp.ioLevel == IOLevel::All ||
	m_probDef.pp.ioLevel == IOLevel::Poll) {
      MatDestroy(&Poll);
      VecDestroy(&pollSol);
      VecDestroy(&pollRhs);
    }
    if (m_transient) {
      MatDestroy(&J);
    }
  }

  PetscErrorCode ProblemGlobal::buildOps()
  {
    PetscLogEvent GLOBAL_OPERATORS;
    PetscLogEventRegister("Global Operators",0,&GLOBAL_OPERATORS);
    PetscLogEventBegin(GLOBAL_OPERATORS,0,0,0,0);
    PetscErrorCode ierr = 0;
    
    if (m_probDef.verbosity > 1) {
      PPW_VONE("Build global operators\n");
    }
    
    // Coordinate row entries between all ranks
    PetscInt actUpIdx = m_mesh.getActUpIdx(),actLoIdx = m_mesh.getActLoIdx();
    PetscInt nLocAct = m_mesh.getActUpIdx() - m_mesh.getActLoIdx();
    std::vector<PetscInt> rnkAct(m_size,0);
    MPI_Allgather(&nLocAct,1,MPI_INT,rnkAct.data(),1,MPI_INT,PETSC_COMM_WORLD);
    for (PetscInt nR=1; nR<m_size; nR++) {
      rnkAct[nR] += rnkAct[nR-1];
    }
    
    if (m_probDef.verbosity > 2) {
      PPS_VTWO("Forming matrix on rank %d from row %d to %d\n",m_rank,actLoIdx,actUpIdx);
    }
    
    // Construct Laplace-Beltrami Operators
    ierr = MatCreate(PETSC_COMM_WORLD,&A); CHKERRQ(ierr);
    ierr = MatSetType(A,MATMPIAIJ); CHKERRQ(ierr);
    ierr = MatSetSizes(A,m_nComps*nLocAct,m_nComps*nLocAct,
		       PETSC_DETERMINE,PETSC_DETERMINE); CHKERRQ(ierr);
    try {
      // Get the preallocation information
      std::vector<PetscInt> d_nnz(nLocAct*m_nComps,0);
      std::vector<PetscInt> o_nnz(nLocAct*m_nComps,0);
      TVec<PetscInt> colIdxs(m_eq.maxStenSize(m_mesh),0);
      TVec<PetscReal> vals(m_eq.maxStenSize(m_mesh),0.);
      for (PetscInt nA=actLoIdx; nA<actUpIdx; nA++) {
        for (PetscInt nC=0; nC<m_nComps; nC++) {
          PetscInt nCols = m_eq.opRow(nA,nC,colIdxs,vals,m_mesh);
          for (PetscInt nI=0; nI<nCols; nI++) {
            if (colIdxs[nI] >= m_nComps*actLoIdx &&
		colIdxs[nI] < m_nComps*actUpIdx) {
              d_nnz[(nA-actLoIdx)*m_nComps+nC]++;
            } else if (colIdxs[nI] >= 0 &&
		       colIdxs[nI] < m_nComps*m_mesh.getNActive()) {
              o_nnz[(nA-actLoIdx)*m_nComps+nC]++;
            }
          }
        }
      }
      ierr = MatMPIAIJSetPreallocation(A,0,d_nnz.data(),0,o_nnz.data()); CHKERRQ(ierr);
      
      // Fill matrix
      for (PetscInt nA=actLoIdx; nA<actUpIdx; nA++) {
        for (PetscInt nC=0; nC<m_nComps; nC++) {
          PetscInt rowNum = nA*m_nComps+nC;
          PetscInt nCols = m_eq.opRow(nA,nC,colIdxs,vals,m_mesh);
          ierr = MatSetValues(A,1,&rowNum,nCols,colIdxs.data(),
			      vals.data(),INSERT_VALUES); CHKERRQ(ierr);
        }
      }
      ierr = MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
      ierr = MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
      
      // Construct polling point extension matrix
      if ((m_probDef.pp.ioLevel == IOLevel::All ||
	   m_probDef.pp.ioLevel == IOLevel::Poll) && m_probDef.mesh.nPoll > 0) {
        // Set locally responsible polling nodes
        PetscInt stride = m_mesh.getNPollPts()/m_size;
        m_nPollLo = m_rank*stride;
        m_nPollUp = (m_rank == m_size - 1) ?
	  m_mesh.getNPollPts() : (m_rank + 1)*stride;
        PetscInt nLocPoll = m_nPollUp - m_nPollLo;
        ierr = MatCreate(PETSC_COMM_WORLD,&Poll); CHKERRQ(ierr);
        ierr = MatSetSizes(Poll,m_nComps*nLocPoll,m_nComps*nLocAct,
			   PETSC_DETERMINE,PETSC_DETERMINE); CHKERRQ(ierr);
        ierr = MatSetType(Poll,MATMPIAIJ); CHKERRQ(ierr);
	
        // Preallocating more than needed
        MatMPIAIJSetPreallocation(Poll,m_mesh.getIntNodes(),PETSC_NULL,
				  m_mesh.getIntNodes(),PETSC_NULL); CHKERRQ(ierr);
        if (m_probDef.verbosity>2) {
          PPS_VTWO("Building polling point extension on rank %d from row %d to %d\n",
		   m_rank,m_nPollLo,m_nPollUp);
        }
        TVec<PetscInt> intIdxs(m_mesh.getIntNodes());
        TVec<PetscReal> intWts(m_mesh.getIntNodes());
        TVec<PetscInt> offset(m_mesh.getIntNodes(),1);
        for (PetscInt nP=m_nPollLo; nP<m_nPollUp; nP++) {
          m_mesh.findPollInterpInfo(nP,intIdxs,intWts);
          intIdxs *= m_nComps;
          for (PetscInt nC=0; nC<m_nComps; nC++) {
            PetscInt rowNum = m_nComps*nP + nC;
            ierr = MatSetValues(Poll,1,&rowNum,intIdxs.getDim(),intIdxs.data(),
				intWts.data(),INSERT_VALUES); CHKERRQ(ierr);
            intIdxs += offset;
          }
        }
        ierr = MatAssemblyBegin(Poll,MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
        ierr = MatAssemblyEnd(Poll,MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
        ierr = MatCreateVecs(Poll,NULL,&pollSol); CHKERRQ(ierr);
        ierr = MatCreateVecs(Poll,NULL,&pollRhs); CHKERRQ(ierr);
      }
    } catch(ErrorTrace& e) {
      PUSHERROR(e,"Global operator construction failed");
      throw e;
    }
  
    // Construct solution and right hand side vectors
    ierr = MatCreateVecs(A,&sol,&rhs); CHKERRQ(ierr);
    TVec<PetscInt> offset(m_nComps);
    for (PetscInt nC=0; nC<m_nComps; nC++) { offset[nC] = nC; }
    for (PetscInt nR=actLoIdx; nR<actUpIdx; nR++) {
      TVec<PetscInt> rows(m_nComps,nR*m_nComps);
      rows += offset;
      std::vector<PetscScalar> iVals = m_eq.icFuncGen(nR,m_mesh);
      ierr = VecSetValues(sol,m_nComps,rows.data(),
			  iVals.data(),INSERT_VALUES); CHKERRQ(ierr);
      std::vector<PetscScalar> rVals = m_transient ?
	m_eq.rhsFuncGen(nR,iVals.data(),0.0,m_mesh) : m_eq.rhsFuncGen(nR,m_mesh);
      ierr = VecSetValues(rhs,m_nComps,rows.data(),
			  rVals.data(),INSERT_VALUES); CHKERRQ(ierr);
    }
    ierr = VecAssemblyBegin(rhs);   CHKERRQ(ierr);
    ierr = VecAssemblyEnd(rhs);     CHKERRQ(ierr);
    ierr = VecAssemblyBegin(sol);   CHKERRQ(ierr);
    ierr = VecAssemblyEnd(sol);     CHKERRQ(ierr);
  
    PetscLogEventEnd(GLOBAL_OPERATORS,0,0,0,0);
    
    return ierr;
  }

  PetscErrorCode ProblemGlobal::solveTransient()
  {
    PetscLogEvent SOLVE_TRANS;
    PetscLogEventRegister("Solve Trans",0,&SOLVE_TRANS);
    PetscLogEventBegin(SOLVE_TRANS,0,0,0,0);
    
    if (m_probDef.verbosity>0) {
      PPW_VZERO("\n\nSolve transient problem\n");
    }
    
    PetscErrorCode ierr = 0;
    PetscInt steps = 0;
    PetscReal time = 1;
    TSSolve(ts,sol); CHKERRQ(ierr);
    ierr = TSGetTime(ts,&time); CHKERRQ(ierr);
    ierr = TSGetStepNumber(ts,&steps); CHKERRQ(ierr);
    
    if (m_probDef.verbosity>1) {
      PPW_VONE("Time stepping completed with %d timesteps, at final time %g\n",
	       steps,time);
    }
    
    PetscLogEventEnd(SOLVE_TRANS,0,0,0,0);
    return ierr;
  }

  PetscErrorCode ProblemGlobal::solveTransient(PetscErrorCode (*monitor)(TS,
									 PetscInt,
									 PetscReal,
									 Vec,void*),
					       void* mctx)
  {
    PetscErrorCode ierr = 0;
    
    ierr = TSMonitorSet(ts,monitor,mctx,NULL); CHKERRQ(ierr);
    ierr = solveTransient(); CHKERRQ(ierr);
    
    return ierr;
  }

  PetscErrorCode ProblemGlobal::formStationarySolver()
  {
    PetscErrorCode ierr = 0;
    if (m_probDef.verbosity>1) {
      PPW_VONE("Create solver (stationary)\n");
    }
    
    // Create solver context
    ierr = KSPCreate(PETSC_COMM_WORLD,&ksp);             CHKERRQ(ierr);  
    ierr = KSPSetOperators(ksp,A,A);                     CHKERRQ(ierr);
    ierr = KSPSetType(ksp,KSPGMRES);                     CHKERRQ(ierr);
    ierr = KSPSetTolerances(ksp,1.e-8,1.e-50,50.0,1000); CHKERRQ(ierr);
    ierr = KSPGetPC(ksp,&pc);                            CHKERRQ(ierr);

    // Set it the PC as our custom one and give it the needed context
    // This is free, and is the simplest way to give it the context in all cases
    ierr = PCSetType(pc,"ddcpm");                         CHKERRQ(ierr);
    ierr = PC_ddcpm::PCSetContext_ddcpm(pc,&m_mesh,this); CHKERRQ(ierr);

    // Set desired options, including setting a different PC type
    ierr = PCSetFromOptions(pc);   CHKERRQ(ierr);
    ierr = KSPSetFromOptions(ksp); CHKERRQ(ierr);
  
    return ierr;
  }

  PetscErrorCode ProblemGlobal::formTransientSolver()
  {
    PetscErrorCode ierr = 0;
    if (m_probDef.verbosity > 1) {
      PPW_VONE("Create solver (transient)\n");
    }
    
    // Create time stepper
    ierr = TSCreate(PETSC_COMM_WORLD,&ts); CHKERRQ(ierr);
    ierr = TSSetProblemType(ts,TS_LINEAR); CHKERRQ(ierr);
    
    // Set the equation to be solved
    ierr = MatDuplicate(A,MAT_DO_NOT_COPY_VALUES,&J); CHKERRQ(ierr);
    ierr = TSSetIFunction(ts,NULL,&(ProblemGlobal::tsIFunction),
			  (void*) this); CHKERRQ(ierr);
    ierr = TSSetIJacobian(ts,J,J,&(ProblemGlobal::tsIJacobian),
			  (void*) this); CHKERRQ(ierr);
    ierr = TSSetRHSFunction(ts,rhs,&(ProblemGlobal::tsRhsFunction),
			    (void*) this); CHKERRQ(ierr);
    
    // Set default behavior
    ierr = TSSetTimeStep(ts,m_probDef.time.dt);                 CHKERRQ(ierr);
    ierr = TSSetSolution(ts,sol);                               CHKERRQ(ierr);
    ierr = TSSetType(ts,TSARKIMEX);                             CHKERRQ(ierr);
    ierr = TSSetMaxTime(ts,m_probDef.time.final);               CHKERRQ(ierr);
    ierr = TSSetMaxSteps(ts,m_probDef.time.maxSteps);           CHKERRQ(ierr);
    ierr = TSSetExactFinalTime(ts,TS_EXACTFINALTIME_MATCHSTEP); CHKERRQ(ierr);
    
    // Pull out the KSP solver and set the preconditioner to be ours if needed
    SNES snes;
    ierr = TSGetSNES(ts,&snes);                         CHKERRQ(ierr);
    if (snes) {
      ierr = SNESSetType(snes,SNESKSPONLY);             CHKERRQ(ierr);
    }
    ierr = TSGetKSP(ts,&ksp);                           CHKERRQ(ierr);
    ierr = KSPSetType(ksp,KSPGMRES);                    CHKERRQ(ierr);
    ierr = KSPSetTolerances(ksp,1.e-8,1.e-50,50.0,100); CHKERRQ(ierr);
    ierr = KSPGetPC(ksp,&pc);                           CHKERRQ(ierr);

    // Set it the PC as our custom one and give it the needed context
    // This is free, and is the simplest way to give it the context in all cases
    ierr = PCSetType(pc,"ddcpm");                         CHKERRQ(ierr);
    ierr = PC_ddcpm::PCSetContext_ddcpm(pc,&m_mesh,this); CHKERRQ(ierr);

    // Set desired options, including setting a different PC type
    ierr = PCSetFromOptions(pc);     CHKERRQ(ierr);
    ierr = KSPSetFromOptions(ksp);   CHKERRQ(ierr);
    ierr = SNESSetFromOptions(snes); CHKERRQ(ierr);
    ierr = TSSetFromOptions(ts);     CHKERRQ(ierr);
    
    return ierr;
  }

// Scalar problems only
  PetscErrorCode ProblemGlobal::solveStationary()
  {
    PetscLogEvent SOLVE_STAT;
    PetscLogEventRegister("Solve stationary problem",0,&SOLVE_STAT);
    PetscLogEventBegin(SOLVE_STAT,0,0,0,0);
    
    if (m_probDef.verbosity>0) {
      PPW_VZERO("\n\nSolve stationary problem\n");
    }
    
    // Solve equation with PETSc KSP
    PetscErrorCode ierr = KSPSolve(ksp,rhs,sol); CHKERRQ(ierr);

    ierr = PCViewFromOptions(pc,NULL,"-pc_view"); CHKERRQ(ierr);

    PetscLogEventEnd(SOLVE_STAT,0,0,0,0);
    return ierr;
  }

  PetscErrorCode
  ProblemGlobal::solveStationary(const std::vector<KSPType>& solveTypes,
				 const std::vector<const char*>& solveNames,
				 std::vector<PetscInt>& convIters)
  {
    PetscLogEvent SOLVE_STAT;
    PetscLogEventRegister("Compare stationary solvers",0,&SOLVE_STAT);
    PetscLogEventBegin(SOLVE_STAT,0,0,0,0);
    
    if (m_probDef.verbosity>0) {
      PPW_VZERO("\n\nCompare stationary solvers\n");
    }
    
    PetscErrorCode ierr = 0;
    std::vector<PetscLogEvent> logE(solveTypes.size(),0);
    
    // Solve equation with PETSc KSP
    for (unsigned int nS=0; nS<solveTypes.size(); nS++) {
      if (m_probDef.verbosity>1) {
        PPW_VONE("Solve Stationary system: %s\n",solveNames[nS]);
      }
      PetscInt iters = 0;
      
      VecSet(sol,0.0);                          CHKERRQ(ierr);
      KSPSetType(ksp,solveTypes[nS]);           CHKERRQ(ierr);
      KSPSetUp(ksp);                            CHKERRQ(ierr);
      PetscLogEventRegister(solveNames[nS],0,&logE[nS]);
      PetscLogEventBegin(logE[nS],0,0,0,0);
      ierr = KSPSolve(ksp,rhs,sol);             CHKERRQ(ierr);
      ierr = KSPGetTotalIterations(ksp,&iters); CHKERRQ(ierr);
      convIters.push_back(iters - (nS == 1 ? convIters[0] : 0));
      PetscLogEventEnd(logE[nS],0,0,0,0);
    }

    PetscLogEventEnd(SOLVE_STAT,0,0,0,0);
    return ierr;
  }
  
  // Hard code to heat equation form for now: F(uDot,u,t) = uDot + Au
  PetscErrorCode ProblemGlobal::tsIFunction(TS ts,PetscReal t,Vec u,Vec uDot,
					    Vec f,void* ctx)
  {
    (void) t; (void) ts;
    PetscErrorCode ierr = 0;
    ProblemGlobal *pctx = (ProblemGlobal*) ctx;
    
    ierr = MatMultAdd(pctx->A,u,uDot,f); CHKERRQ(ierr);
    
    return ierr;
  }

  PetscErrorCode ProblemGlobal::tsIJacobian(TS ts,PetscReal t,Vec u,Vec uDot,
					    PetscReal shift,Mat J,Mat PJ,void* ctx)
  {
    (void) uDot; (void) ts; (void) u; (void) t;
    PetscErrorCode ierr = 0;
    ProblemGlobal *pctx = (ProblemGlobal*) ctx;

    if (pctx->m_shift == shift) {
      return 0;
    }
    
    pctx->m_shift = shift;
    ierr = MatCopy(pctx->A,J,DIFFERENT_NONZERO_PATTERN); CHKERRQ(ierr);
    ierr = MatShift(J,shift);                            CHKERRQ(ierr);    
    
    return 0;
  }

  PetscErrorCode ProblemGlobal::tsRhsFunction(TS ts,PetscReal t,Vec u,
					      Vec f,void* ctx)
  {
    (void) ts;
    ProblemGlobal     *pctx = (ProblemGlobal*) ctx;
    PetscErrorCode     ierr = 0; 
    PetscInt           ldim,nComps,first;
    PetscScalar       *fArr;
    const PetscScalar *uArr;
    
    ierr = MatGetOwnershipRange(pctx->A,&first,NULL);  CHKERRQ(ierr);
    ierr = VecGetArrayRead(u,&uArr);                   CHKERRQ(ierr);
    ierr = VecGetArray(f,&fArr);                       CHKERRQ(ierr);
    ierr = VecGetLocalSize(u,&ldim);                   CHKERRQ(ierr);
    nComps = pctx->m_eq.getNComps();
    
    for (PetscInt n=0; n<ldim/nComps; n++) {
      const std::vector<PetscScalar>& forcing =
        pctx->m_eq.rhsFuncGen(n + first/nComps,&uArr[n*nComps],t,pctx->m_mesh);
      for (PetscInt nC=0; nC<nComps; nC++) {
        fArr[n*nComps+nC] = forcing[nC];
      }
    }
    
    ierr = VecRestoreArrayRead(u,&uArr); CHKERRQ(ierr);
    ierr = VecRestoreArray(f,&fArr);     CHKERRQ(ierr);
  
    return ierr;
  }
  
  PetscInt ProblemGlobal::getNComps() const
  {
    return m_eq.getNComps();
  }
  
  PetscInt ProblemGlobal::getNPollLo() const
  {
    return m_nPollLo;
  }
  
  PetscInt ProblemGlobal::getNPollUp() const
  {
    return m_nPollUp;
  }
  
  PetscErrorCode ProblemGlobal::takeSol(double const** outSol) const
  {
    PetscErrorCode ierr = 0;
    ierr = VecGetArrayRead(sol,outSol); CHKERRQ(ierr);
    return ierr;
  }

  PetscErrorCode ProblemGlobal::returnSol(double const** outSol) const
  {
    PetscErrorCode ierr = 0;
    ierr = VecRestoreArrayRead(sol,outSol); CHKERRQ(ierr);
    return ierr;
  }

  PetscErrorCode ProblemGlobal::takeRhs(double const** outRhs) const
  {
    PetscErrorCode ierr = 0;
    ierr = VecGetArrayRead(rhs,outRhs); CHKERRQ(ierr);
    return ierr;
  }

  PetscErrorCode ProblemGlobal::returnRhs(double const** outRhs) const
  {
    PetscErrorCode ierr = 0;
    ierr = VecRestoreArrayRead(rhs,outRhs); CHKERRQ(ierr);
    return ierr;
  }

  PetscErrorCode ProblemGlobal::takeSolPoll(double const** outSol) const
  {
    PetscErrorCode ierr = 0;
    ierr = MatMult(Poll,sol,pollSol); CHKERRQ(ierr);
    ierr = VecGetArrayRead(pollSol,outSol); CHKERRQ(ierr);
    return ierr;
  }

  PetscErrorCode ProblemGlobal::returnSolPoll(double const** outSol) const
  {
    PetscErrorCode ierr = 0;
    ierr = VecRestoreArrayRead(pollSol,outSol); CHKERRQ(ierr);
    return ierr;
  }

  PetscErrorCode ProblemGlobal::takeRhsPoll(double const** outRhs) const
  {
    PetscErrorCode ierr = 0;
    ierr = MatMult(Poll,rhs,pollRhs); CHKERRQ(ierr);
    ierr = VecGetArrayRead(pollRhs,outRhs); CHKERRQ(ierr);
    return ierr;
  }

  PetscErrorCode ProblemGlobal::returnRhsPoll(double const** outRhs) const
  {
    PetscErrorCode ierr = 0;
    ierr = VecRestoreArrayRead(pollRhs,outRhs); CHKERRQ(ierr);
    return ierr;
  }

  PetscErrorCode ProblemGlobal::drawOps() const
  {
    PetscErrorCode ierr = 0;
    ierr = MatView(A,PETSC_VIEWER_DRAW_WORLD);      CHKERRQ(ierr);
    if (m_probDef.pp.ioLevel == IOLevel::All ||
	m_probDef.pp.ioLevel == IOLevel::Poll) {
      ierr = MatView(Poll,PETSC_VIEWER_DRAW_WORLD); CHKERRQ(ierr);
    }  
    return ierr;
  }

} // Close namespace
