#include <map>

#include "CPMesh.H"
#include "GridFunc.H"

namespace DDCPM {
  
  // ---------------- GridFunc methods ------------------
  std::ostream& operator<<(std::ostream& os,const GridFunc& gf)
  {
    os << "Position |    Weight     | Extension flag" << std::endl;
    for (size_t nP=0; nP<gf.m_gFuncPos.size(); nP++) {
      os << gf.m_gFuncPos[nP] << " | " << gf.m_gFuncWts[nP]
	 << " | " << gf.m_gFuncExt[nP] << " | " << std::endl;
    }
    return os;
  }

  // Addition of GridFuncs
  GridFunc& GridFunc::operator+=(const GridFunc& other)
  {
    return (*this).addScaled(other,1.0);
  }

  const GridFunc GridFunc::operator+(const GridFunc& other) const
  {
    return GridFunc(*this) += other;
  }

  // Subtraction/negation of GridFuncs
  GridFunc& GridFunc::operator-=(const GridFunc& other)
  {
    return (*this).addScaled(other,-1.0);
  }

  const GridFunc GridFunc::operator-(const GridFunc& other) const
  {
    return GridFunc(*this) -= other;
  }

  const GridFunc GridFunc::operator-() const
  {
    return GridFunc(*this).scale(-1.0);
  }

  // Scaling of GridFuncs
  GridFunc& GridFunc::operator*=(PetscScalar scl)
  {
    return (*this).scale(scl);
  }

  const GridFunc GridFunc::operator*(PetscScalar scl) const
  {
    return GridFunc(*this).scale(scl);
  }

  const GridFunc operator*(PetscScalar scl,const GridFunc& other)
  {
    return GridFunc(other).scale(scl);
  }

  // Composition of GridFuncs
  GridFunc& GridFunc::operator*=(const GridFunc& other)
  {
    // Move positions and weights to temporaries, clear extension flags
    std::vector<TVec<PetscInt> > pos;
    pos.swap(m_gFuncPos);
    std::vector<PetscScalar> wts;
    wts.swap(m_gFuncWts);
    std::vector<bool> ().swap(m_gFuncExt);
    // Loop over pos and create new ops over each member
    for (size_t nP=0; nP<pos.size(); nP++) {
      // offset the position vectors
      std::vector<TVec<PetscInt> > otherPos(other.getGFuncPos());
      for (size_t nO=0; nO<otherPos.size(); nO++) {
	otherPos[nO] += pos[nP];
      }
      // Add sub-operator in
      this->addScaled(GridFunc(otherPos,other.getGFuncWts(),
			       other.getGFuncExt()),wts[nP]);
    }
    return *this;
  }

  const GridFunc GridFunc::operator*(const GridFunc& other) const
  {
    return GridFunc(*this) *= other;
  }

  GridFunc& GridFunc::scale(PetscScalar scl)
  {
    for (size_t nP=0; nP<m_gFuncWts.size(); nP++) {
      m_gFuncWts[nP] *= scl;
    }
    return *this;
  }

  GridFunc& GridFunc::addScaled(const GridFunc& other,PetscScalar scl)
  {
    const std::vector<TVec<PetscInt> >& pos = other.getGFuncPos();
    const std::vector<PetscScalar>& wts = other.getGFuncWts();
    const std::vector<bool>& ext = other.getGFuncExt();

    for (size_t nP=0; nP<pos.size(); nP++) {
      auto it = std::find(m_gFuncPos.begin(),m_gFuncPos.end(),pos[nP]);
      if (it != m_gFuncPos.end()) {
        size_t idx = std::distance(m_gFuncPos.begin(),it);
        // Same extension type
        if (ext[nP] == m_gFuncExt[idx]) {
          m_gFuncWts[idx] += scl*wts[nP];
        } else {
          // Check if other extension type is already stored later
          it = std::find(++it,m_gFuncPos.end(),pos[nP]);
          if (it!=m_gFuncPos.end()) {
            size_t idx = std::distance(m_gFuncPos.begin(),it);
            if (ext[nP] == m_gFuncExt[idx]) {
              m_gFuncWts[idx] += scl*wts[nP];
            }
          } else { // Other extension type not yet present
            m_gFuncPos.push_back(pos[nP]);
            m_gFuncWts.push_back(scl*wts[nP]);
            m_gFuncExt.push_back(ext[nP]);
          }
        }
      } else { // Not yet present in total gfunc
        m_gFuncPos.push_back(pos[nP]);
        m_gFuncWts.push_back(scl*wts[nP]);
        m_gFuncExt.push_back(ext[nP]);
      }
    }
    return *this;
  }

  std::vector<TVec<PetscInt> > GridFunc::getGFuncUnique() const
  {
    std::vector<TVec<PetscInt> > unq(m_gFuncPos);
    std::sort(unq.begin(),unq.end());
    unq.erase(std::unique(unq.begin(),unq.end()),unq.end());
    return unq;
  }
  
  PetscInt GridFunc::maxStenSize(CPMeshBase& a_mesh) const
  {
    return m_gFuncPos.size()*a_mesh.getIntNodes();
  }

  PetscInt GridFunc::opRow(PetscInt rowNum,TVec<PetscInt>& colNums,
			   TVec<PetscReal>& vals,CPMeshBase& a_mesh)
  {
    try {
      // Generate stencil if not cached
      if (m_stencils.find(rowNum) == m_stencils.end()) {
        PetscInt nUniq = 0;
        PetscInt nActive = a_mesh.getNActive() + a_mesh.getNActiveBC();
        std::map<PetscInt,PetscScalar> idxwts;
	
        // Traverse grid function,pulling indices and weights
        for (size_t nS=0; nS<m_gFuncPos.size(); nS++) {
          PetscInt idx = a_mesh.getRelIDX(rowNum,m_gFuncPos[nS]);
          // If extension needed then union in the interpolation points/weights
          if ((m_gFuncExt[nS] || idx>=nActive) && idx>=0) {
            const TVec<PetscInt>& intNodes(a_mesh.getInterpNodes(idx));
            const TVec<PetscScalar>& intWeights(a_mesh.getInterpWeights(idx));
            for (size_t nC=0; nC<intNodes.getDim(); nC++) {
              auto it = idxwts.find(intNodes[nC]);
              if (it!=idxwts.end()) {
                idxwts[intNodes[nC]] += m_gFuncWts[nS]*intWeights[nC];
              } else {
                idxwts[intNodes[nC]] = m_gFuncWts[nS]*intWeights[nC];
                nUniq++;
              }
            }
          } else if (idx>=0) {
            auto it = idxwts.find(idx);
	    if (it!=idxwts.end()) {
	      idxwts[idx] += m_gFuncWts[nS];
	    } else {
	      idxwts[idx] = m_gFuncWts[nS];
	      nUniq++;
	    }
          }
        }
	
        // Insert new stencil and weights
        m_stencils.insert(std::make_pair(rowNum,TVec<PetscInt>(nUniq)));
        m_stenWts.insert(std::make_pair(rowNum,TVec<PetscReal>(nUniq)));
        PetscInt ctr = 0;
        for (auto it=idxwts.begin(); it!=idxwts.end(); ++it) {
          m_stencils[rowNum][ctr] = it->first;
          m_stenWts[rowNum][ctr] = it->second;
          colNums[ctr] = it->first;
          vals[ctr] = it->second;
          ctr++;
        }
        return nUniq;
      }
      // Copy out stencil and weights
      for (size_t nI=0; nI<m_stencils[rowNum].getDim(); nI++) {
        colNums[nI] = m_stencils[rowNum][nI];
        vals[nI] = m_stenWts[rowNum][nI];
      }
      return m_stencils[rowNum].getDim();
    } catch(ErrorTrace& e) {
      PUSHERROR(e,"Row construction failed");
      throw e;
    }
  }

  // Clear out cached stencils
  void GridFunc::flushStencils()
  {
    std::unordered_map<PetscInt,TVec<PetscInt>>().swap(m_stencils);
    std::unordered_map<PetscInt,TVec<PetscScalar>>().swap(m_stenWts);
  }

  // Specific grid functions for named construction
  std::vector<TVec<PetscInt> > GridFunc::gfpNamed(const char* name,PetscInt dim)
  {
    if (strcmp(name,"LaplacianFourthOrder") == 0) {
      return gfp_Lap4(dim);
    } else if (strcmp(name,"LaplacianSecondOrder") == 0) {
      return gfp_Lap2(dim);
    }
    return gfp_id(dim); // Default to identity
  }

  std::vector<PetscScalar> GridFunc::gfwNamed(const char* name,PetscInt dim,
					      PetscReal delta,PetscScalar scale)
  {
    if (strcmp(name,"LaplacianFourthOrder") == 0) {
      return gfw_Lap4(dim,delta,scale);
    } else if (strcmp(name,"LaplacianSecondOrder") == 0) {
      return gfw_Lap2(dim,delta,scale);
    }
    return gfw_id(scale); // Default to identity
  }

  std::vector<bool> GridFunc::gfeNamed(const char* name,PetscInt dim)
  {
    if (strcmp(name,"LaplacianFourthOrder") == 0) {
      return gfe_Lap4(dim);
    } else if (strcmp(name,"LaplacianSecondOrder") == 0) {
      return gfe_Lap2(dim);
    }
    return gfe_id(); // Default to identity
  }

  std::vector<TVec<PetscInt> > GridFunc::gfp_id(PetscInt dim)
  {
    if (dim == 2) {
      return {TVec<PetscInt>(std::array<PetscInt,2>{0,0})};
    } else if (dim == 3) {
      return {TVec<PetscInt>(std::array<PetscInt,3>{0,0,0})};
    }
    return {TVec<PetscInt>()};
  }

  std::vector<TVec<PetscInt> > GridFunc::gfp_Lap2(PetscInt dim)
  {
    if (dim == 2) {
      return {
	TVec<PetscInt>(std::array<PetscInt,2>{ 0, 0}),
        TVec<PetscInt>(std::array<PetscInt,2>{ 1, 0}),
        TVec<PetscInt>(std::array<PetscInt,2>{-1, 0}),
        TVec<PetscInt>(std::array<PetscInt,2>{ 0, 1}),
        TVec<PetscInt>(std::array<PetscInt,2>{ 0,-1})
      };
    } else if (dim == 3) {
      return {
	TVec<PetscInt>(std::array<PetscInt,3>{ 0, 0, 0}),
        TVec<PetscInt>(std::array<PetscInt,3>{ 1, 0, 0}),
        TVec<PetscInt>(std::array<PetscInt,3>{-1, 0, 0}),
        TVec<PetscInt>(std::array<PetscInt,3>{ 0, 1, 0}),
        TVec<PetscInt>(std::array<PetscInt,3>{ 0,-1, 0}),
        TVec<PetscInt>(std::array<PetscInt,3>{ 0, 0, 1}),
        TVec<PetscInt>(std::array<PetscInt,3>{ 0, 0,-1})
      };
    }
    return std::vector<TVec<PetscInt> >();
  }

  std::vector<TVec<PetscInt> > GridFunc::gfp_Lap4(PetscInt dim)
  {
    if (dim == 2) {
      return {
	TVec<PetscInt>(std::array<PetscInt,2>{ 0, 0}),
        TVec<PetscInt>(std::array<PetscInt,2>{ 1, 0}),
        TVec<PetscInt>(std::array<PetscInt,2>{-1, 0}),
        TVec<PetscInt>(std::array<PetscInt,2>{ 0, 1}),
        TVec<PetscInt>(std::array<PetscInt,2>{ 0,-1}),
        TVec<PetscInt>(std::array<PetscInt,2>{ 1, 1}),
        TVec<PetscInt>(std::array<PetscInt,2>{ 1,-1}),
        TVec<PetscInt>(std::array<PetscInt,2>{-1, 1}),
        TVec<PetscInt>(std::array<PetscInt,2>{-1,-1})
      };
    } else if (dim == 3) {
      return {
	TVec<PetscInt>(std::array<PetscInt,3>{ 0, 0, 0}),
        TVec<PetscInt>(std::array<PetscInt,3>{ 1, 0, 0}),
        TVec<PetscInt>(std::array<PetscInt,3>{-1, 0, 0}),
        TVec<PetscInt>(std::array<PetscInt,3>{ 0, 1, 0}),
        TVec<PetscInt>(std::array<PetscInt,3>{ 0,-1, 0}),
        TVec<PetscInt>(std::array<PetscInt,3>{ 0, 0, 1}),
        TVec<PetscInt>(std::array<PetscInt,3>{ 0, 0,-1}),
        TVec<PetscInt>(std::array<PetscInt,3>{ 1, 1, 0}),
        TVec<PetscInt>(std::array<PetscInt,3>{ 1,-1, 0}),
        TVec<PetscInt>(std::array<PetscInt,3>{-1, 1, 0}),
        TVec<PetscInt>(std::array<PetscInt,3>{-1,-1, 0}),
        TVec<PetscInt>(std::array<PetscInt,3>{ 1, 0, 1}),
        TVec<PetscInt>(std::array<PetscInt,3>{ 1, 0,-1}),
        TVec<PetscInt>(std::array<PetscInt,3>{-1, 0, 1}),
        TVec<PetscInt>(std::array<PetscInt,3>{-1, 0,-1}),
        TVec<PetscInt>(std::array<PetscInt,3>{ 0, 1, 1}),
        TVec<PetscInt>(std::array<PetscInt,3>{ 0, 1,-1}),
        TVec<PetscInt>(std::array<PetscInt,3>{ 0,-1, 1}),
        TVec<PetscInt>(std::array<PetscInt,3>{ 0,-1,-1})
      };
    }
    return std::vector<TVec<PetscInt> >();
  }


  std::vector<PetscScalar> GridFunc::gfw_id(PetscScalar scale)
  {
    return {scale};
  }

  std::vector<PetscScalar> GridFunc::gfw_Lap2(PetscInt dim,PetscReal delta,
					      PetscScalar scale)
  {
    PetscScalar hM2 = scale/(delta*delta);
    if (dim == 2) {
      return {-4.0*hM2,hM2,hM2,hM2,hM2};
    } else if (dim == 3) {
      return {-6.0*hM2,hM2,hM2,hM2,hM2,hM2,hM2};
    }
    return std::vector<PetscScalar> (0);
  }

  std::vector<PetscScalar> GridFunc::gfw_Lap4(PetscInt dim,PetscReal delta,
					      PetscScalar scale)
  {
    PetscScalar sixhM2 = scale/(6*delta*delta);
    if (dim == 2) {
      return {-20.0*sixhM2,
	      4.0*sixhM2,4.0*sixhM2,4.0*sixhM2,4.0*sixhM2,
	      sixhM2,sixhM2,sixhM2,sixhM2};
    } else if (dim == 3) {
      return {-60.0*sixhM2,
	      8.0*sixhM2,8.0*sixhM2,8.0*sixhM2,8.0*sixhM2,8.0*sixhM2,8.0*sixhM2,
	      sixhM2,sixhM2,sixhM2,sixhM2,sixhM2,sixhM2,
	      sixhM2,sixhM2,sixhM2,sixhM2,sixhM2,sixhM2};
    }
    return std::vector<PetscScalar> (0);
  }

  std::vector<bool> GridFunc::gfe_id()
  {
    return {false};
  }

  std::vector<bool> GridFunc::gfe_Lap2(PetscInt dim)
  {
    if (dim == 2) {
      return {false,true,true,true,true};
    } else if (dim == 3) {
      return {false,true,true,true,true,true,true};
    }
    return std::vector<bool> (0);
  }

  std::vector<bool> GridFunc::gfe_Lap4(PetscInt dim)
  {
    if (dim == 2) {
      return {false,true,true,true,true,true,true,true,true};
    } else if (dim == 3) {
      return {false,
	      true,true,true,true,true,true,true,true,true,
	      true,true,true,true,true,true,true,true,true};
    }
    return std::vector<bool> (0);
  }

} // Close namespace
