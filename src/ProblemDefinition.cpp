// ProblemDefinition Class Definition
// Author: Ian May
// Purpose: Define the problem by reading an input file and/or command line
//          arguments.

#include "ProblemDefinition.H"

namespace DDCPM {

  void ProblemDefinition::setFromFile(char* fname)
  {
    std::ifstream inFile;
    inFile.open(fname);
    bool opened = inFile.is_open();
    if (opened && m_rank == 0) {
      printf("Setting options from input file: %s\n",fname);
    } else if (m_rank == 0) {
      ERRORTRACE("Failed to open input file, tried: " + std::string(fname));
    }
  
    if (opened) {
      std::string line;
      while(getline(inFile,line)) {
	if (strComp(line,"MESH") || strComp(line,"TIME") ||
	    strComp(line,"POSTPROCESS") || strComp(line,"OTHER")) {
	  break;
	}
      }
    
      redirectParse(&inFile,line);
      inFile.close();
      conflictCheck();
    }
  }

  void ProblemDefinition::redirectParse(std::ifstream* inFile,std::string label)
  {
    if (strComp(label,"MESH")) {
      pdParseMesh(inFile);
    } else if (strComp(label,"TIME")) {
      pdParseTime(inFile);
    } else if (strComp(label,"POSTPROCESS")) {
      pdParsePostProc(inFile);
    } else if (strComp(label,"OTHER")) {
      pdParseOther(inFile);
    }
  }

  void ProblemDefinition::conflictCheck()
  {
    if (mesh.nPoll == 0 && pp.ioLevel == IOLevel::All) {
      PP_WARN("No poll points specified, reverting to IOLevel::Cloud");
      pp.ioLevel = IOLevel::Cloud;
    } else if (mesh.nPoll == 0 && pp.ioLevel == IOLevel::Poll) {
      PP_WARN("No poll points specified, reverting to IOLevel::None");
      pp.ioLevel = IOLevel::None;
    }
  }

  void ProblemDefinition::pdParseMesh(std::ifstream* inFile)
  {
    std::string line;
    while(getline(*inFile,line)) {
      if (strComp(line,"MESH") || strComp(line,"TIME") ||
	  strComp(line,"POSTPROCESS") || strComp(line,"OTHER")) {
	break;
      }
    
      std::istringstream lStrm(line);
      std::string token;
      getline(lStrm,token,' ');
      if (strComp(token,"Surface")) {
	mesh.surfBase[0] = '\0';
	getline(lStrm,token,' ');
	if (strComp(token,"Circle")) {
	  strcat(mesh.surfBase,"Circle");
	  mesh.surfType = Surfs::Circle;
	} else if (strComp(token,"Line")) {
	  strcat(mesh.surfBase,"Line");
	  mesh.surfType = Surfs::Line;
	} else if (strComp(token,"Arc")) {
	  strcat(mesh.surfBase,"Arc");
	  mesh.surfType = Surfs::Arc;
	} else if (strComp(token,"Sphere")) {
	  strcat(mesh.surfBase,"Sphere");
	  mesh.surfType = Surfs::Sphere;
	} else if (strComp(token,"Disc")) {
	  strcat(mesh.surfBase,"Disc");
	  mesh.surfType = Surfs::Disc;
	} else if (strComp(token,"Torus")) {
	  strcat(mesh.surfBase,"Torus");
	  mesh.surfType = Surfs::Torus;
	} else if (strComp(token,"Plane")) {
	  strcat(mesh.surfBase,"Plane");
	  mesh.surfType = Surfs::Plane;
	} else if (strComp(token,"User")) {
	  strcat(mesh.surfBase,"User");
	  mesh.surfType = Surfs::User;
	} else if (strComp(token,"Triangulated")) {
	  strcat(mesh.surfBase,"Triangulated");
	  mesh.surfType = Surfs::Triangulated;
	  getline(lStrm,token,' ');
	  mesh.triPlyFile[0] = '\0';
	  strcat(mesh.triPlyFile,token.c_str());
	}
      } else if (strComp(token,"Resolution")) {
	getline(lStrm,token,' ');
	std::stringstream ss(token);
	ss >> mesh.nRes;
	mesh.delta = 1./((double)mesh.nRes);
      } else if (strComp(token,"Dimension")) {
	getline(lStrm,token,' ');
	std::stringstream ss(token);
	ss >> mesh.dim;
      } else if (strComp(token,"BoundingBox")) {
	for (int nB=0; nB<2*mesh.dim; nB++) {
	  getline(lStrm,token,' ');
	  std::stringstream ss(token);
	  ss >> mesh.bbSize[nB];
	}
      } else if (strComp(token,"Overlap")) {
	getline(lStrm,token,' ');
	std::stringstream ss(token);
	ss >> mesh.nOver;
      } else if (strComp(token,"Poll")) {
	getline(lStrm,token,' ');
	std::stringstream ss(token);
	ss >> mesh.nPoll;
      } else if (strComp(token,"InterpOrder")) {
	getline(lStrm,token,' ');
	std::stringstream ss(token);
	ss >> mesh.ord;
      } else if (strComp(token,"Partitions")) {
	getline(lStrm,token,' ');
	std::stringstream ss(token);
	ss >> mesh.nParts;
      } else if (strComp(token,"AlignBounds")) {
	getline(lStrm,token,' ');
	mesh.alignBounds = strComp(token,"True") ? PETSC_TRUE : PETSC_FALSE;
      }
    }
  
    redirectParse(inFile,line);
  }

  void ProblemDefinition::pdParseTime(std::ifstream* inFile)
  {
    std::string line;
    while(getline(*inFile,line)) {
      if (strComp(line,"MESH") || strComp(line,"TIME") ||
	  strComp(line,"POSTPROCESS") || strComp(line,"OTHER")) {
	break;
      }
    
      std::istringstream lStrm(line);
      std::string token;
      getline(lStrm,token,' ');
      if (strComp(token,"Initial")) {
	getline(lStrm,token,' ');
	time.initial = stod(token);
      } else if (strComp(token,"Final")) {
	getline(lStrm,token,' ');
	time.final = stod(token);
      } else if (strComp(token,"Dt")) {
	getline(lStrm,token,' ');
	time.dt = stod(token);
      } else if (strComp(token,"MaxSteps")) {
	getline(lStrm,token,' ');
	std::stringstream ss(token);
	ss >> time.maxSteps;
      }
    }
  
    redirectParse(inFile,line);
  }

  void ProblemDefinition::pdParsePostProc(std::ifstream* inFile)
  {
    std::string line;
    while(getline(*inFile,line)) {
      if (strComp(line,"MESH") || strComp(line,"TIME") ||
	  strComp(line,"POSTPROCESS") || strComp(line,"OTHER")) {
	break;
      }
    
      std::istringstream lStrm(line);
      std::string token;
      getline(lStrm,token,' ');
      if (strComp(token,"PlotFrequency")) {
	getline(lStrm,token,' ');
	std::stringstream ss(token);
	ss >> pp.plotFreq;
      } else if (strComp(token,"FileBase")) {
	getline(lStrm,token,' ');
	pp.fnameBase[0] = '\0';
	strcat(pp.fnameBase,token.c_str());
      } else if (strComp(token,"IOLevel")) {
	getline(lStrm,token,' ');
	if (strComp(token,"All")) {
	  pp.ioLevel = IOLevel::All;
	} else if (strComp(token,"Cloud")) {
	  pp.ioLevel = IOLevel::Cloud;
	} else if (strComp(token,"Poll")) {
	  pp.ioLevel = IOLevel::Poll;
	} else {
	  pp.ioLevel = IOLevel::None;
	}
      }
    }
  
    redirectParse(inFile,line);
  }

  void ProblemDefinition::pdParseOther(std::ifstream* inFile)
  {
    std::string line;
    while(getline(*inFile,line)) {
      if (strComp(line,"MESH") || strComp(line,"TIME") ||
	  strComp(line,"POSTPROCESS") || strComp(line,"OTHER")) {
	break;
      }
    
      std::istringstream lStrm(line);
      std::string token;
      getline(lStrm,token,' ');
      if (strComp(token,"Verbosity")) {
	getline(lStrm,token,' ');
	std::stringstream ss(token);
	ss >> verbosity;
      } else if (strComp(token,"Draw")) {
	getline(lStrm,token,' ');
	std::stringstream ss(token);
	ss >> draw;
      }
    }
  
    redirectParse(inFile,line);
  }

  bool ProblemDefinition::strComp(std::string line,std::string comp)
  {
    return line.compare(comp) == 0;
  }

  PetscErrorCode ProblemDefinition::setFromCLI()
  {
    // Set from command line
    PetscOptionsBegin(PETSC_COMM_WORLD,NULL,"Closest Point options","");
    {
      PPW_VZERO("Setting options from command line\n");
      // Mesh Options
      PetscOptionsInt("-mesh_dim","Embedding dimension","",mesh.dim,&mesh.dim,NULL);
      PetscOptionsInt("-mesh_ord","Interpolation order","",mesh.ord,&mesh.ord,NULL);
      PetscOptionsInt("-mesh_nparts","Number of partitions","",
		      mesh.nParts,&mesh.nParts,NULL);
      PetscOptionsInt("-mesh_nover","(O)RAS Overlap","",mesh.nOver,&mesh.nOver,NULL);
      PetscOptionsInt("-mesh_npoll","Number of polling points","",
		      mesh.nPoll,&mesh.nPoll,NULL);
      PetscBool setRes = PETSC_FALSE;
      PetscOptionsInt("-mesh_res","Resolution","",mesh.nRes,&mesh.nRes,&setRes);
      if (setRes) {
	mesh.delta = 1.0/((double)mesh.nRes);
      }
      PetscBool setBox;
      PetscInt nmax = mesh.dim;
      mesh.bbSize.resize(nmax,0);
      PetscOptionsGetIntArray(NULL,NULL,"-mesh_bnd_box",mesh.bbSize.data(),
			      &nmax,&setBox);
      if (!setBox && setRes) {
	for (int nD=0; nD<mesh.dim; nD++) {
	  mesh.bbSize[nD] = 2*(mesh.nRes+mesh.ord+2);
	}
      }
      PetscOptionsBool("-mesh_align_bounds","Align subdomain boundaries","",
		       mesh.alignBounds,&mesh.alignBounds,NULL);
      // Surface setting is non-optimal... PetscOptionsString was not friendly though
      // This works 
	   PetscBool surfType = PETSC_FALSE;
      PetscOptionsBool("-mesh_surf_circle","Solve on circle","",
		       surfType,&surfType,NULL);
      if (surfType) {
	mesh.surfBase[0] = '\0';
	strcat(mesh.surfBase,"Circle");
	mesh.surfType = Surfs::Circle;
      }
      surfType = PETSC_FALSE;
      PetscOptionsBool("-mesh_surf_line","Solve on line","",surfType,&surfType,NULL);
      if (surfType) {
	mesh.surfBase[0] = '\0';
	strcat(mesh.surfBase,"Line");
	mesh.surfType = Surfs::Line;
      }
      surfType = PETSC_FALSE;
      PetscOptionsBool("-mesh_surf_arc","Solve on arc","",surfType,&surfType,NULL);
      if (surfType) {
	mesh.surfBase[0] = '\0';
	strcat(mesh.surfBase,"Arc");
	mesh.surfType = Surfs::Arc;
      }
      surfType = PETSC_FALSE;
      PetscOptionsBool("-mesh_surf_sphere","Solve on sphere","",
		       surfType,&surfType,NULL);
      if (surfType) {
	mesh.surfBase[0] = '\0';
	strcat(mesh.surfBase,"Sphere");
	mesh.surfType = Surfs::Sphere;
      }
      surfType = PETSC_FALSE;
      PetscOptionsBool("-mesh_surf_disc","Solve on disc","",surfType,&surfType,NULL);
      if (surfType) {
	mesh.surfBase[0] = '\0';
	strcat(mesh.surfBase,"Disc");
	mesh.surfType = Surfs::Disc;
      }
      surfType = PETSC_FALSE;
      PetscOptionsBool("-mesh_surf_torus","Solve on torus","",
		       surfType,&surfType,NULL);
      if (surfType) {
	mesh.surfBase[0] = '\0';
	strcat(mesh.surfBase,"Torus");
	mesh.surfType = Surfs::Torus;
      }
      surfType = PETSC_FALSE;
      PetscOptionsBool("-mesh_surf_plane","Solve on plane","",
		       surfType,&surfType,NULL);
      if (surfType) {
	mesh.surfBase[0] = '\0';
	strcat(mesh.surfBase,"Plane");
	mesh.surfType = Surfs::Plane;
      }
      surfType = PETSC_FALSE;
      PetscOptionsBool("-mesh_surf_user","Solve on user defined surface","",
		       surfType,&surfType,NULL);
      if (surfType) {
	mesh.surfBase[0] = '\0';
	strcat(mesh.surfBase,"User");
	mesh.surfType = Surfs::User;
      }
      surfType = PETSC_FALSE;
      PetscOptionsBool("-mesh_surf_triangulated","Solve on triangulation","",
		       surfType,&surfType,NULL);
      if (surfType) {
	mesh.surfBase[0] = '\0';
	strcat(mesh.surfBase,"Triangulated");
	mesh.surfType = Surfs::Triangulated;
      }
      surfType = PETSC_FALSE;
      // Read surface name and triangulated PLY file location directly
      PetscOptionsGetString(NULL,NULL,"-mesh_surface_name",mesh.surfBase,
			    sizeof(mesh.surfBase),NULL);
      PetscOptionsGetString(NULL,NULL,"-mesh_tri_plyfile",mesh.triPlyFile,
			    sizeof(mesh.triPlyFile),NULL);
    
      // Time stepping control
      PetscOptionsReal("-time_initial","Starting time","",
		       time.initial,&time.initial,NULL);
      PetscOptionsReal("-time_final","Final time","",
		       time.final,&time.final,NULL);
      PetscOptionsReal("-time_dt","Time step size","",
		       time.dt,&time.dt,NULL);
      PetscOptionsInt("-time_max_steps","Maximum time steps","",
		      time.maxSteps,&time.maxSteps,NULL);
    
      // Post processing settings
      PetscOptionsInt("-pp_plotfreq","Plot frequency","",
		      pp.plotFreq,&pp.plotFreq,NULL);
      PetscBool ioLevel = PETSC_FALSE;
      PetscOptionsBool("-pp_out_all","Write all meshes, all fields, to plot files","",
		       ioLevel,&ioLevel,NULL);
      if (ioLevel) {
	pp.ioLevel = IOLevel::All;
      }
      ioLevel = PETSC_FALSE;
      PetscOptionsBool("-pp_out_cloud","Write cloud mesh, all fields, to plot files",
		       "",ioLevel,&ioLevel,NULL);
      if (ioLevel) {
	pp.ioLevel = IOLevel::Cloud;
      }
      ioLevel = PETSC_FALSE;
      PetscOptionsBool("-pp_out_poll","Write poll mesh, all fields, to plot files","",
		       ioLevel,&ioLevel,NULL);
      if (ioLevel) {
	pp.ioLevel = IOLevel::Poll;
      }
      ioLevel = PETSC_FALSE;
      PetscOptionsBool("-pp_out_none","Write nothing to plot files","",
		       ioLevel,&ioLevel,NULL);
      if (ioLevel) {
	pp.ioLevel = IOLevel::None;
      }
    
      // Override output directory
      PetscOptionsGetString(NULL,NULL,"-pp_out_dir",pp.fnameBase,
			    sizeof(pp.fnameBase),NULL);
    
      // Errata
      PetscOptionsInt("-verb","Verbosity","",verbosity,&verbosity,NULL);
      PetscOptionsInt("-draw","Draw","",draw,&draw,NULL);
    }
    PetscOptionsEnd();
  
    return 0;
  }

  void ProblemDefinition::printConfig()
  {
    if (m_rank == 0 && verbosity>0) {
      std::cout << "---------- Problem Definition ----------" << std::endl;
      std::cout << "    Surface: ";
      if (mesh.surfType == Surfs::Circle) {
	std::cout << "Circle" << std::endl;
      } else if (mesh.surfType == Surfs::Line) {
	std::cout << "Line" << std::endl;
      } else if (mesh.surfType == Surfs::Arc) {
	std::cout << "Arc" << std::endl;
      } else if (mesh.surfType == Surfs::Sphere) {
	std::cout << "Sphere" << std::endl;
      } else if (mesh.surfType == Surfs::Disc) {
	std::cout << "Disc" << std::endl;
      } else if (mesh.surfType == Surfs::Torus) {
	std::cout << "Torus" << std::endl;
      } else if (mesh.surfType == Surfs::Plane) {
	std::cout << "Plane" << std::endl;
      } else if (mesh.surfType == Surfs::User) {
	std::cout << "User" << std::endl;
      } else if (mesh.surfType == Surfs::Triangulated) {
	std::cout << "Triangulated" << std::endl;
      } else {
	std::cout << "Undefined" << std::endl;
      }   
      std::cout << "    Mesh" << std::endl;
      std::cout << "      Dim: " << mesh.dim << std::endl
		<< "      Interp Order: " << mesh.ord << std::endl
		<< "      Overlap: " << mesh.nOver << std::endl
		<< "      Partitions: " << mesh.nParts << std::endl;
      std::cout << "      Align Interfaces: " << mesh.alignBounds << std::endl; 
      std::cout << "      Poll Points: " << mesh.nPoll << std::endl
		<< "      Resolution: " << mesh.nRes << std::endl
		<< "      Grid spacing: " << mesh.delta << std::endl
		<< "      Bounding Box: ";
      for (int n=0; n<mesh.dim; n++) {
	std::cout << mesh.bbSize[n] << " ";
      }
      std::cout << std::endl;
      std::cout << "    Time stepping" << std::endl
		<< "      Initial time: " << time.initial << std::endl
		<< "      Final time: " << time.final << std::endl
		<< "      Step size: " << time.dt << std::endl
		<< "      Maximum steps: " << time.maxSteps << std::endl;
      std::cout << "    Post Processing" << std::endl
		<< "      Plot Frequency: " << pp.plotFreq << std::endl
		<< "      IO Level: ";
      if (pp.ioLevel == IOLevel::All) {
	std::cout << "All" << std::endl;
      } else if (pp.ioLevel == IOLevel::Cloud) {
	std::cout << "Cloud" << std::endl;
      } else if (pp.ioLevel == IOLevel::Poll) {
	std::cout << "Poll" << std::endl;
      } else {
	std::cout << "None" << std::endl;
      } 
      std::cout << "    Errata" << std::endl
		<< "      Verbosity: " << verbosity << std::endl
		<< "      Draw: " << draw << std::endl;
      std::cout << "-------- End Problem Definition --------" << std::endl;
    }
  }

} // Close DDCPM::namespace
