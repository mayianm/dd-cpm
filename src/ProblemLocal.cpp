// Problem Class Implementation
// Author: Ian May
// Purpose: The actual system to be solved is defined here and solved here.
//          In the single domain case this takes in (a pointer to) a CPMesh 
//          object and (a pointer to) a DiffEq object and builds the associated
//          operators. In multidomain problems this also takes in all of the 
//          subdomain meshes and subproblem definitions then builds the needed 
//          MPI communicators between them

#include "Problem.H"

namespace DDCPM {

  ProblemLocal::ProblemLocal(ProblemDefinition& a_probDef,CPMeshLocal& a_mesh,
			     DiffEq& a_eq,PetscReal a_shift):
    ProblemBase(a_probDef,a_mesh,a_eq),
    m_mesh(a_mesh)
  {
    PetscLogEvent PROBLEM;
    PetscLogEventRegister("ProblemLocal Cons.",0,&PROBLEM);
    PetscLogEventBegin(PROBLEM,0,0,0,0);
    
    if (m_probDef.verbosity>2) {
      PPS_VTWO("Construct local problem\n");
    }
    
    try {
      buildOps();
      m_eq.flushStencils();
      shift(a_shift);
    } catch(ErrorTrace& e) {
      PUSHERROR(e,"Local problem construction failed");
      throw e;
    }
    
    if (m_probDef.draw > 1) {
      drawOps();
    }

    PetscLogEventEnd(PROBLEM,0,0,0,0);
  }
  
  ProblemLocal::~ProblemLocal()
  {
    KSPDestroy(&ksp);
    VecDestroy(&sol);
    VecDestroy(&rhs);
    MatDestroy(&A);
  }

  PetscErrorCode ProblemLocal::buildOps()
  {
    PetscLogEvent LOCAL_OPERATORS;
    PetscLogEventRegister("Local Operators",0,&LOCAL_OPERATORS);
    PetscLogEventBegin(LOCAL_OPERATORS,0,0,0,0);
    PetscErrorCode ierr;
    
    if (m_probDef.verbosity > 3) {
      PPS_VTHREE("Build local operators\n");
    }
    
    try {
      PetscInt actTot = m_mesh.getNActive() + m_mesh.getNActiveBC();
      ierr = MatCreate(PETSC_COMM_SELF,&A); CHKERRQ(ierr);
      ierr = MatSetType(A,MATSEQAIJ);       CHKERRQ(ierr);
      ierr = MatSetSizes(A,PETSC_DECIDE,PETSC_DECIDE,
			 m_nComps*actTot,m_nComps*actTot); CHKERRQ(ierr);
      
      // Get the preallocation information;
      TVec<PetscInt> colIdxs(m_eq.maxStenSize(m_mesh),0);
      TVec<PetscReal> vals(m_eq.maxStenSize(m_mesh),0.0);
      std::vector<PetscInt> nnz(m_nComps*actTot,0);
      for (PetscInt nA=0; nA<actTot; nA++) {
        for (PetscInt nC=0; nC<m_nComps; nC++) {
          nnz[nA*m_nComps+nC] = m_eq.opRow(nA,nC,colIdxs,vals,m_mesh);
        }
      }
      ierr = MatSeqAIJSetPreallocation(A,0,nnz.data()); CHKERRQ(ierr);

      // Fill matrix
      for (PetscInt nA=0; nA<actTot; nA++) {
        for (PetscInt nC=0; nC<m_nComps; nC++) {
          PetscInt rowNum = nA*m_nComps+nC;
          PetscInt nCols = m_eq.opRow(nA,nC,colIdxs,vals,m_mesh);
          ierr = MatSetValues(A,1,&rowNum,nCols,colIdxs.data(),
			      vals.data(),INSERT_VALUES); CHKERRQ(ierr);
        }
      }
      ierr = MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
      ierr = MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);   CHKERRQ(ierr);
    } catch(ErrorTrace& e) {
      PUSHERROR(e,"Local operator construction failed");
      throw e;
    }
    // Construct solution and right hand side vectors
    TVec<PetscInt> offset(m_nComps);
    for (PetscInt nC=0; nC<m_nComps; nC++) {
      offset[nC] = nC;
    }
    ierr = MatCreateVecs(A,&sol,&rhs); CHKERRQ(ierr);
    ierr = VecSet(sol,0.0);             CHKERRQ(ierr);
    ierr = VecSet(rhs,0.0);             CHKERRQ(ierr);
    // Construct direct solver
    ierr = KSPCreate(PETSC_COMM_SELF,&ksp); CHKERRQ(ierr);
    ierr = KSPSetOptionsPrefix(ksp,"sub_"); CHKERRQ(ierr);
    ierr = KSPSetOperators(ksp,A,A);        CHKERRQ(ierr);
    ierr = KSPSetType(ksp,KSPPREONLY);      CHKERRQ(ierr);
    ierr = KSPSetFromOptions(ksp);          CHKERRQ(ierr);
    ierr = KSPGetPC(ksp,&pc);               CHKERRQ(ierr);
    ierr = PCSetOptionsPrefix(pc,"sub_");   CHKERRQ(ierr);

    // iLU is a good default choice
    ierr = PCSetType(pc,PCILU);                           CHKERRQ(ierr);
    ierr = PCFactorSetMatOrderingType(pc,MATORDERINGRCM); CHKERRQ(ierr);
    ierr = PCFactorSetReuseOrdering(pc,PETSC_TRUE);       CHKERRQ(ierr);
    ierr = PCFactorSetLevels(pc,2);                       CHKERRQ(ierr);

    // Then allow overriding this from CLI
    ierr = PCSetFromOptions(pc); CHKERRQ(ierr);
    ierr = KSPSetUp(ksp);        CHKERRQ(ierr);
    
    PetscLogEventEnd(LOCAL_OPERATORS,0,0,0,0);
    return ierr;
  }

  PetscErrorCode ProblemLocal::drawOps() const
  {
    PetscErrorCode ierr = MatView(A,PETSC_VIEWER_DRAW_SELF); CHKERRQ(ierr);
    return ierr;
  }

} // Close namespace
