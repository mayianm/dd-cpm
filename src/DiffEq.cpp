#include "CPMesh.H"
#include "DiffEq.H"

namespace DDCPM {
  
  PetscInt DiffEq::getNComps() const
  {
    return m_lhsOp.size();
  }

// --------------- DiffEq methods ---------------------------
  std::vector<TVec<PetscInt> > DiffEq::getGFunc() const
  {
    return m_lhsOp[0]->getGFuncUnique();
  }
  
  PetscInt DiffEq::maxStenSize(CPMeshBase& a_mesh) const
  {
    return m_lhsOp[0]->maxStenSize(a_mesh);
  }
  
  PetscInt DiffEq::opRow(PetscInt rowNum,PetscInt comp,TVec<PetscInt>& colNums,
			 TVec<PetscReal>& vals,CPMeshBase& a_mesh)
  {
    PetscLogEvent OPROW;
    PetscLogEventRegister("DiffEq::opRow",0,&OPROW);
    PetscLogEventBegin(OPROW,0,0,0,0);
    try {
      // Get operator row w.r.t. raw indices
      PetscInt nCols = m_lhsOp[comp]->opRow(rowNum,colNums,vals,a_mesh);
      // Shift to respect number of components
      colNums.scale(m_lhsOp.size());
      colNums.add(comp);
      PetscLogEventEnd(OPROW,0,0,0,0);
      return nCols;
    } catch(ErrorTrace& e) {
      PUSHERROR(e,"Operator row construction failed");
      throw e;
    }
  }

  void DiffEq::flushStencils()
  {
    for (std::size_t nO=0; nO<m_lhsOp.size(); nO++) {
      m_lhsOp[nO]->flushStencils();
    }
  }

  // Evaluate the right hand side function at the node at rowNum
  std::vector<PetscScalar> DiffEq::rhsFuncGen(PetscInt rowNum,
					      const CPMeshGlobal& a_mesh) const
  {
    return m_rhsFuncStat(a_mesh.getCP(rowNum));
  }

  std::vector<PetscScalar> DiffEq::rhsFuncGen(PetscInt rowNum,
					      const PetscScalar* sols,PetscReal t,
					      const CPMeshGlobal& a_mesh)
  {
    return m_rhsFuncTrans(a_mesh.getCP(rowNum),t,sols);
  }

  std::vector<PetscScalar> DiffEq::icFuncGen(PetscInt rowNum,
					     const CPMeshGlobal& a_mesh) const
  {
    return m_icFunction(a_mesh.getCP(rowNum));
  }

} // Close namespace
