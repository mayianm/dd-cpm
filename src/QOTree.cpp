// QOTree class implementation
// Author: Ian May
// Notes: This is achieved through the magic bit masks from Libmorton
//        library (https://github.com/Forceflow/libmorton), though only the relevant 
//        methods are included here. See <dd-cpm-root>/licenses/LICENSE

#include "ErrorTrace.H"
#include "QOTree.H"

namespace DDCPM {
  
  QOTree::QOTree(unsigned int a_dim,bool a_bimap)
  {
    m_dim = a_dim;
    m_bimap = a_bimap;
  }

  // Morton encoding
  uint64_t QOTree::vecToMorton(const TVec<uint32_t>& a_lat)
  {
    uint64_t key = 0;
    if (a_lat.getDim() == 2) {
      key |= splitBy3(a_lat[0]) | splitBy3(a_lat[1]) << 1;
    } else if (a_lat.getDim() == 3) {
      key |= splitBy3(a_lat[0]) | splitBy3(a_lat[1]) << 1 | splitBy3(a_lat[2]) << 2;
    } else {
      PP_ERR("Unsupported dimension in QOTree::vecToMorton(), given: %d\n",
	     a_lat.getDim());
      ERRORTRACE("Unsupported dimension in QOTree::vecToMorton");
    }
    return key;
  }
  
  uint64_t QOTree::vecToMorton(const std::array<uint32_t,3>& a_lat)
  {
    uint64_t key = 0;
    key |= splitBy3(a_lat[0]) | splitBy3(a_lat[1]) << 1 | splitBy3(a_lat[2]) << 2;
    return key;
  }

  uint64_t QOTree::splitBy3(uint32_t a)
  {
    uint64_t x = a & 0x1fffff;
    x = (x | x << 32) & 0x1f00000000ffff;
    x = (x | x << 16) & 0x1f0000ff0000ff;
    x = (x | x << 8) & 0x100f00f00f00f00f;
    x = (x | x << 4) & 0x10c30c30c30c30c3;
    x = (x | x << 2) & 0x1249249249249249;
    return x;
  }

  TVec<uint32_t> QOTree::mortonToVec(uint64_t a_key,unsigned int a_dim)
  {
    if (a_dim == 2) {
      return TVec<uint32_t>(std::array<uint32_t,2> {packBy3(a_key),
						    packBy3(a_key >> 1)});
    } else if (a_dim == 3) {
      return TVec<uint32_t>(std::array<uint32_t,3> {packBy3(a_key),
						    packBy3(a_key >> 1),
						    packBy3(a_key >> 2)});
    }
    return TVec<uint32_t>();
  }

  uint32_t QOTree::packBy3(uint64_t a)
  {
    uint64_t x = a & 0x1249249249249249;
    x = (x ^ (x >> 2)) & 0x10c30c30c30c30c3;
    x = (x ^ (x >> 4)) & 0x100f00f00f00f00f;
    x = (x ^ (x >> 8)) & 0x1f0000ff0000ff;
    x = (x ^ (x >> 16)) & 0x1f00000000ffff;
    x = (x ^ (x >> 32)) & 0x1fffff;
    return (uint32_t) x;
  }

// Tree building
  void QOTree::addLeaf(const TVec<uint32_t>& a_lat,int a_dLoc)
  {
    addLeaf(vecToMorton(a_lat),a_dLoc);
  }
  
  void QOTree::addLeaf(const std::array<uint32_t,3>& a_lat,int a_dLoc)
  {
    addLeaf(vecToMorton(a_lat),a_dLoc);
  }

  void QOTree::addLeaf(uint64_t a_key,int a_dLoc)
  {
    m_fTree[a_key] = a_dLoc;
    if (m_bimap && a_dLoc >= 0) {
      m_bTree[a_dLoc] = a_key;
    }
  }

  bool QOTree::addLeafUnique(const TVec<uint32_t>& a_lat,int a_dLoc)
  {
    return addLeafUnique(vecToMorton(a_lat),a_dLoc);
  }

  bool QOTree::addLeafUnique(const std::array<uint32_t,3>& a_lat,int a_dLoc)
  {
    return addLeafUnique(vecToMorton(a_lat),a_dLoc);
  }

  bool QOTree::addLeafUnique(uint64_t a_key,int a_dLoc)
  {
    if (getDLoc(a_key)<0) {
      m_fTree[a_key] = a_dLoc;
      if (m_bimap && a_dLoc>=0) {
        m_bTree[a_dLoc] = a_key;
      }
      return true;
    }
    return false;
  }

  void QOTree::offsetValues(PetscInt a_off)
  {
    if (m_bimap) {
      std::unordered_map<PetscInt,uint64_t> ().swap(m_bTree);
    }
    for (auto it=m_fTree.begin(); it!=m_fTree.end(); it++) {
      it->second += a_off;
      if (m_bimap) { m_bTree[it->second] = it->first; }
    }
  }
  
  // Getters
  bool QOTree::hasLeaf(const TVec<uint32_t>& a_lat) const
  {
    auto it = m_fTree.find(vecToMorton(a_lat));
    return it != m_fTree.end();
  }
  
  bool QOTree::hasLeaf(const std::array<uint32_t,3>& a_lat) const
  {
    auto it = m_fTree.find(vecToMorton(a_lat));
    return it != m_fTree.end();
  }

  bool QOTree::hasLeaf(uint64_t a_key) const
  {
    auto it = m_fTree.find(a_key);
    return it != m_fTree.end();
  }

  int QOTree::getDLoc(const TVec<uint32_t>& a_lat) const
  {
    return getDLoc(vecToMorton(a_lat));
  }

  int QOTree::getDLoc(const std::array<uint32_t,3>& a_lat) const
  {
    return getDLoc(vecToMorton(a_lat));
  }

  int QOTree::getDLoc(uint64_t a_key) const
  {
    auto it = m_fTree.find(a_key);
    if (it != m_fTree.end()) {
      return it->second;
    }
    return DLOC_DEFAULT;
  }

  void QOTree::deleteTree()
  {
    std::unordered_map<uint64_t,PetscInt>().swap(m_fTree);
    std::unordered_map<PetscInt,uint64_t>().swap(m_bTree);
  }

  void QOTree::trimTree()
  {
    for (auto it=m_fTree.begin(); it!=m_fTree.end(); ) {
      if (it->second < 0) {
        it = m_fTree.erase(it); 
      } else {
        it++;
      }
    }
    for (auto it=m_bTree.begin(); it!=m_bTree.end(); ) {
      if (it->first < 0) {
        it = m_bTree.erase(it);
      } else {
        it++;
      }
    }
  }

  unsigned int QOTree::size() const
  {
    return m_fTree.size();
  }

  TVec<uint32_t> QOTree::getVecByVal(PetscInt a_val) const
  {
    if (m_bimap) {
      auto it = m_bTree.find(a_val);
      if (it != m_bTree.end()) {
        return mortonToVec(it->second,m_dim);
      }
      PP_ERR("QOTree::getVecByVal failed on value: %d on tree of size: %ld\n",
	     a_val,m_bTree.size());
      ERRORTRACE("QOTree::getVecByVal failed");
      return TVec<uint32_t> (m_dim);
    }
    for (auto it=m_fTree.begin(); it!=m_fTree.end(); it++) {
      if (it->second == a_val) {
        return mortonToVec(it->first,m_dim);
      }
    }
    PP_ERR("QOTree::getVecByVal failed on value: %d\n",a_val);
    ERRORTRACE("QOTree::getVecByVal failed");
    return TVec<uint32_t> (m_dim,std::numeric_limits<uint32_t>::max());
  }

  std::unordered_map<uint64_t,PetscInt>& QOTree::getMap()
  {
    return m_fTree;
  }

// Iterators
  std::unordered_map<uint64_t,PetscInt>::const_iterator QOTree::begin() const
  {
    return m_fTree.begin();
  }

  std::unordered_map<uint64_t,PetscInt>::const_iterator QOTree::end() const
  {
    return m_fTree.end();
  }

} // Close namespace
