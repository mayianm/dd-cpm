// CPPostProc Class Implementation
// Author: Ian May
// Purpose: Performs all output for the Closest Point solvers.
//          Also defines any post-processing routines needed.

#include "CPPostProc.H"

namespace DDCPM {
  
  CPPostProc::CPPostProc(ProblemDefinition& a_probDef):
    m_probDef(a_probDef),
    cloudMeshAllocd(false),
    pollMeshAllocd(false)
  {
    if (m_probDef.verbosity > 0) {
      PPW_VZERO("\n\nConstruct CPPostProc object\n");
    }
    MPI_Comm_rank(PETSC_COMM_WORLD,&m_rank);
    MPI_Comm_size(PETSC_COMM_WORLD,&m_size);
    
    // Create descriptive filename
    std::ostringstream pstr;
    pstr << m_probDef.mesh.nRes << "_" << m_probDef.mesh.ord
	 << "_" << m_probDef.mesh.nParts << "_" << m_probDef.mesh.nOver;
    parStr = pstr.str();
    std::ostringstream fnstrm;
    fnstrm << m_probDef.pp.fnameBase << "/" << m_probDef.mesh.surfBase << parStr;
    std::string dir = fnstrm.str();
    
    // Create directory if needed
    std::size_t pos = 0;
    while(pos<dir.size() && pos!=std::string::npos) {
      pos = dir.find("/",pos+1);
      mkdir(dir.substr(0,pos).c_str(),0777);
    }
    
    // Finish root of filenames
    fnstrm << "/" << m_probDef.mesh.surfBase;
    filename = fnstrm.str();
    if (m_probDef.verbosity>1 && m_probDef.pp.ioLevel!=IOLevel::None) {
      PPW_VONE("Root filename for output: %s\n",filename.c_str());
    }
  }

  PetscErrorCode CPPostProc::tsMonitor(TS ts,PetscInt step,PetscReal time,
				       Vec u,void* mctx)
  {
    (void) ts; // Suppress unused param warnings
    (void) u;
    TsCtx *ctx = (TsCtx*) mctx;
    // Write solution file if plot frequency matches or at/past final time
    if (step % ctx->m_probDef.pp.plotFreq == 0 ||
       PetscAbsReal(time - ctx->m_probDef.time.final) < 1.e-15 ||
       time >= ctx->m_probDef.time.final) {
      ctx->m_postProc.writeData(step,time,ctx->m_mesh,ctx->m_problem);
    }
    return 0;
  }

  void CPPostProc::writeData(int a_step,double a_time,const CPMeshGlobal& a_mesh,
			     const ProblemGlobal& a_prob)
  {
    // Prepare filename and open file
    char numStr[7];
    sprintf(numStr,"%06d",a_step);
    std::string stepName = filename + std::string(numStr) + ".h5";
    hid_t plist_id = H5Pcreate(H5P_FILE_ACCESS);
    H5Pset_fapl_mpio(plist_id,PETSC_COMM_WORLD,MPI_INFO_NULL);
    hid_t file_id = H5Fcreate(stepName.c_str(),H5F_ACC_TRUNC,H5P_DEFAULT,plist_id);
    H5Pclose(plist_id);
    // Write out timestamp
    hsize_t one = 1;
    hid_t dataspace_t = H5Screate_simple(1,&one,&one);
    hid_t dataset_t = H5Dcreate(file_id,"time",H5T_NATIVE_DOUBLE,
				dataspace_t,H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT);
    if (m_rank == 0) {
      H5Dwrite(dataset_t,H5T_NATIVE_DOUBLE,H5S_ALL,H5S_ALL,H5P_DEFAULT,&a_time);
    }
    H5Dclose(dataset_t);
    H5Sclose(dataspace_t);
    if (m_probDef.verbosity > 1) {
      PPW_VONE("\nWriting to file: %s\n",stepName.c_str());
    }
    
    // Write polling surface info if desired
    if (m_probDef.pp.ioLevel == IOLevel::All ||
	m_probDef.pp.ioLevel == IOLevel::Poll) {
      writePollData(file_id,a_mesh,a_prob);
    }
    
    // Write cloud info if desired
    if (m_probDef.pp.ioLevel == IOLevel::All ||
	m_probDef.pp.ioLevel == IOLevel::Cloud) {
      writeCloudData(file_id,a_mesh,a_prob);
    }
    
    // Close file
    H5Fclose(file_id);
  }

  void CPPostProc::allocCloudMeshData(const CPMeshGlobal& a_mesh,
				      const ProblemGlobal& a_globProb)
  {
    int nPtGbl = a_mesh.getNActive();
    int nPtLoc = a_mesh.getActUpIdx() - a_mesh.getActLoIdx();
    int nComp = a_globProb.getNComps();
    
    // Global dataset dimensions
    clGblVec.push_back(nPtGbl);
    clGblVec.push_back(m_probDef.mesh.dim);
    clGblScl.push_back(nPtGbl);
    clGblPrb.push_back(nPtGbl);
    if (nComp>1) {
      clGblPrb.push_back(nComp);
    }
    
    // Local dataset dimensions
    clLocVec.push_back(nPtLoc);
    clLocVec.push_back(m_probDef.mesh.dim);
    clLocScl.push_back(nPtLoc);
    clLocPrb.push_back(nPtLoc);
    if (nComp>1) {
      clLocPrb.push_back(nComp);
    }
    
    // Offset information
    clOffVec.push_back(a_mesh.getActLoIdx());
    clOffVec.push_back(0);
    clOffScl.push_back(a_mesh.getActLoIdx());
    clOffPrb.push_back(a_mesh.getActLoIdx());
    if (nComp>1) {
      clOffPrb.push_back(0);
    }
    
    // Allocate space for all mesh data and fill in
    coordsCloud.resize(m_probDef.mesh.dim*nPtLoc);
    a_mesh.getActivePos(coordsCloud.data());
    varDist.resize(nPtLoc);
    a_mesh.getDistance(varDist.data());
    varSNormCloud.resize(m_probDef.mesh.dim*nPtLoc);
    a_mesh.getSurfNorm(varSNormCloud.data());
    if (m_probDef.mesh.nParts > 1) {
      varPart.resize(nPtLoc);
      a_mesh.getPartition(varPart.data());
    }
    
    cloudMeshAllocd = true;
  }

  void CPPostProc::allocPollMeshData(const CPMeshGlobal& a_mesh,
				     const ProblemGlobal& a_globProb)
  {
    // Form all data arrays
    int nPtGbl = a_mesh.getNPollPts();
    int nPtLoc = a_globProb.getNPollUp() - a_globProb.getNPollLo();
    int nFaces = a_mesh.getNPollFaces();
    int nComp = a_globProb.getNComps();
    plGblVec.push_back(nPtGbl);
    plGblVec.push_back(m_probDef.mesh.dim);
    plGblConn.push_back(nFaces);
    plGblConn.push_back(3);
    plGblPrb.push_back(nPtGbl);
    plLocPrb.push_back(nPtLoc);
    plOffPrb.push_back(a_globProb.getNPollLo());
    if (nComp > 1) {
      plGblPrb.push_back(nComp);
      plLocPrb.push_back(nComp);
      plOffPrb.push_back(0);
    }
    
    // Allocate space for and fill out mesh data,rank 0 only
    if (m_rank == 0) {
      coordsPoll.resize(m_probDef.mesh.dim*nPtGbl);
      connPoll.resize(3*nFaces);
      varSNormPoll.resize(m_probDef.mesh.dim*nPtGbl);
      // Fill data in
      a_mesh.getPollPos(coordsPoll.data());
      a_mesh.getPollFaces(connPoll.data());
      a_mesh.getPollSurfNorm(varSNormPoll.data());
    }
    
    // Allocate problem data to match rank-local dimension
    pollMeshAllocd = true;
  }

  void CPPostProc::writeCloudData(hid_t file_id,const CPMeshGlobal& a_mesh,
				  const ProblemGlobal& a_globProb)
  {
    // Allocate mesh if needed
    if (!cloudMeshAllocd) {
      allocCloudMeshData(a_mesh,a_globProb);
    }
    
    // Create global dataspaces
    hid_t dsp_gbl_vec = H5Screate_simple(2,clGblVec.data(),NULL);
    hid_t dsp_gbl_scl = H5Screate_simple(1,clGblScl.data(),NULL);
    hid_t dsp_gbl_prb = H5Screate_simple(clGblPrb.size(),clGblPrb.data(),NULL);
    
    // Create datasets and close global dataspaces
    hid_t dataset_pos = H5Dcreate(file_id,"cloud_pos",H5T_NATIVE_DOUBLE,
				  dsp_gbl_vec,H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT);
    hid_t dataset_snorm = H5Dcreate(file_id,"cloud_snorm",H5T_NATIVE_DOUBLE,
				    dsp_gbl_vec,H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT);
    hid_t dataset_dist = H5Dcreate(file_id,"cloud_dist",H5T_NATIVE_DOUBLE,
				   dsp_gbl_scl,H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT);
    hid_t dataset_sol = H5Dcreate(file_id,"cloud_sol",H5T_NATIVE_DOUBLE,
				  dsp_gbl_prb,H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT);
    hid_t dataset_rhs = H5Dcreate(file_id,"cloud_rhs",H5T_NATIVE_DOUBLE,
				  dsp_gbl_prb,H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT);
    hid_t dataset_part = 0;
    if (m_probDef.mesh.nParts > 1) {
      dataset_part = H5Dcreate(file_id,"cloud_part",H5T_NATIVE_INT,
			       dsp_gbl_scl,H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT);
    }
    
    // Close global dataspaces
    H5Sclose(dsp_gbl_vec);
    H5Sclose(dsp_gbl_prb);
    H5Sclose(dsp_gbl_scl);
    
    // Create local dataspaces
    hid_t dsp_loc_vec = H5Screate_simple(2,clLocVec.data(),NULL);
    hid_t dsp_loc_scl = H5Screate_simple(1,clLocScl.data(),NULL);
    hid_t dsp_loc_prb = H5Screate_simple(clLocPrb.size(),clLocPrb.data(),NULL);
    
    // Data output property list
    hid_t plist_id = H5Pcreate(H5P_DATASET_XFER);
    H5Pset_dxpl_mpio(plist_id,H5FD_MPIO_COLLECTIVE);
    
    // Write vector data out
    dsp_gbl_vec = H5Dget_space(dataset_pos);
    H5Sselect_hyperslab(dsp_gbl_vec,H5S_SELECT_SET,clOffVec.data(),NULL,
			clLocVec.data(),NULL);
    H5Dwrite(dataset_pos,H5T_NATIVE_DOUBLE,dsp_loc_vec,dsp_gbl_vec,plist_id,
	     coordsCloud.data());
    H5Dclose(dataset_pos);
    H5Sclose(dsp_gbl_vec);
    dsp_gbl_vec = H5Dget_space(dataset_snorm);
    H5Sselect_hyperslab(dsp_gbl_vec,H5S_SELECT_SET,clOffVec.data(),NULL,
			clLocVec.data(),NULL);
    H5Dwrite(dataset_snorm,H5T_NATIVE_DOUBLE,dsp_loc_vec,dsp_gbl_vec,plist_id,
	     varSNormCloud.data());
    H5Dclose(dataset_snorm);
    H5Sclose(dsp_gbl_vec);
    
    // Write scalar data out
    dsp_gbl_scl = H5Dget_space(dataset_dist);
    H5Sselect_hyperslab(dsp_gbl_scl,H5S_SELECT_SET,clOffScl.data(),NULL,
			clLocScl.data(),NULL);
    H5Dwrite(dataset_dist,H5T_NATIVE_DOUBLE,dsp_loc_scl,dsp_gbl_scl,plist_id,
	     varDist.data());
    H5Dclose(dataset_dist);
    H5Sclose(dsp_gbl_scl);
    if (m_probDef.mesh.nParts>1) {
      dsp_gbl_scl = H5Dget_space(dataset_part);
      H5Sselect_hyperslab(dsp_gbl_scl,H5S_SELECT_SET,clOffScl.data(),NULL,
			  clLocScl.data(),NULL);
      H5Dwrite(dataset_part,H5T_NATIVE_INT,dsp_loc_scl,dsp_gbl_scl,plist_id,
	       varPart.data());
      H5Dclose(dataset_part);
      H5Sclose(dsp_gbl_scl);
    }
    
    // Write problem data out
    dsp_gbl_prb = H5Dget_space(dataset_sol);
    H5Sselect_hyperslab(dsp_gbl_prb,H5S_SELECT_SET,clOffPrb.data(),NULL,
			clLocPrb.data(),NULL);
    double const* sol = NULL;
    a_globProb.takeSol(&sol);
    H5Dwrite(dataset_sol,H5T_NATIVE_DOUBLE,dsp_loc_prb,dsp_gbl_prb,plist_id,sol);
    a_globProb.returnSol(&sol);
    H5Dclose(dataset_sol);
    H5Sclose(dsp_gbl_prb);
    
    dsp_gbl_prb = H5Dget_space(dataset_rhs);
    H5Sselect_hyperslab(dsp_gbl_prb,H5S_SELECT_SET,clOffPrb.data(),NULL,
			clLocPrb.data(),NULL);
    double const* rhs = NULL;
    a_globProb.takeRhs(&rhs);
    H5Dwrite(dataset_rhs,H5T_NATIVE_DOUBLE,dsp_loc_prb,dsp_gbl_prb,plist_id,rhs);
    a_globProb.returnRhs(&rhs);
    H5Dclose(dataset_rhs);
    H5Sclose(dsp_gbl_prb);
    
    // Close datasets and dataspaces
    H5Sclose(dsp_loc_vec);
    H5Sclose(dsp_loc_prb);
    H5Sclose(dsp_loc_scl);
    H5Pclose(plist_id);
  }

  void CPPostProc::writePollData(hid_t file_id,const CPMeshGlobal& a_mesh,
				 const ProblemGlobal& a_globProb)
  {
    // Allocate if needed
    if (!pollMeshAllocd) {
      allocPollMeshData(a_mesh,a_globProb);
    }
    
    // Create dataspaces for mesh data
    hid_t dataspace_vec = H5Screate_simple(2,plGblVec.data(),NULL);
    hid_t dataspace_conn = H5Screate_simple(2,plGblConn.data(),NULL);
    
    // Create datasets for the needed fields
    hid_t dataset_pos = H5Dcreate(file_id,"poll_pos",H5T_NATIVE_DOUBLE,
				  dataspace_vec,H5P_DEFAULT,
				  H5P_DEFAULT,H5P_DEFAULT);
    hid_t dataset_conn = H5Dcreate(file_id,"poll_conn",H5T_NATIVE_INT,
				   dataspace_conn,H5P_DEFAULT,
				   H5P_DEFAULT,H5P_DEFAULT);
    hid_t dataset_snorm = H5Dcreate(file_id,"poll_snorm",H5T_NATIVE_DOUBLE,
				    dataspace_vec,H5P_DEFAULT,
				    H5P_DEFAULT,H5P_DEFAULT);
    
    // Write all data out,rank 0 only
    if (m_rank == 0) {
      H5Dwrite(dataset_pos,H5T_NATIVE_DOUBLE,H5S_ALL,H5S_ALL,
	       H5P_DEFAULT,coordsPoll.data());
      H5Dwrite(dataset_conn,H5T_NATIVE_INT,H5S_ALL,H5S_ALL,
	       H5P_DEFAULT,connPoll.data());
      H5Dwrite(dataset_snorm,H5T_NATIVE_DOUBLE,H5S_ALL,H5S_ALL,
	       H5P_DEFAULT,varSNormPoll.data());
    }
    
    // Close datasets and dataspaces
    H5Dclose(dataset_pos);
    H5Dclose(dataset_conn);
    H5Dclose(dataset_snorm);
    H5Sclose(dataspace_vec);
    H5Sclose(dataspace_conn);
    
    // Create global spaces/sets
    hid_t dsp_gbl_prb = H5Screate_simple(plGblPrb.size(),plGblPrb.data(),NULL);
    hid_t dataset_sol = H5Dcreate(file_id,"poll_sol",H5T_NATIVE_DOUBLE,
				  dsp_gbl_prb,H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT);
    hid_t dataset_rhs = H5Dcreate(file_id,"poll_rhs",H5T_NATIVE_DOUBLE,
				  dsp_gbl_prb,H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT);
    H5Sclose(dsp_gbl_prb);
    
    // Create local space and property list
    hid_t dsp_loc_prb = H5Screate_simple(plLocPrb.size(),plLocPrb.data(),NULL);
    hid_t plist_id = H5Pcreate(H5P_DATASET_XFER);
    H5Pset_dxpl_mpio(plist_id,H5FD_MPIO_COLLECTIVE);
    
    // Write solution
    dsp_gbl_prb = H5Dget_space(dataset_sol);
    H5Sselect_hyperslab(dsp_gbl_prb,H5S_SELECT_SET,plOffPrb.data(),NULL,
			plLocPrb.data(),NULL);
    double const* solPoll = NULL;
    a_globProb.takeSolPoll(&solPoll);
    H5Dwrite(dataset_sol,H5T_NATIVE_DOUBLE,dsp_loc_prb,dsp_gbl_prb,plist_id,solPoll);
    a_globProb.returnSolPoll(&solPoll);
    H5Dclose(dataset_sol);
    H5Sclose(dsp_gbl_prb);
    
    // Write RHS
    dsp_gbl_prb = H5Dget_space(dataset_rhs);
    H5Sselect_hyperslab(dsp_gbl_prb,H5S_SELECT_SET,plOffPrb.data(),NULL,
			plLocPrb.data(),NULL);
    double const* rhsPoll = NULL;
    a_globProb.takeRhsPoll(&rhsPoll);
    H5Dwrite(dataset_rhs,H5T_NATIVE_DOUBLE,dsp_loc_prb,dsp_gbl_prb,plist_id,rhsPoll);
    a_globProb.returnRhsPoll(&rhsPoll);
    H5Dclose(dataset_rhs);
    H5Sclose(dsp_gbl_prb);
    
    // Clean up
    H5Sclose(dsp_loc_prb);
    H5Pclose(plist_id);
  }

} // Close namespace
