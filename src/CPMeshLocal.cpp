// CPMesh Class Implementation
// Author: Ian May
/*! \file CPMesh.H
    \brief Provides a mesh data structure for Closest Point surface pde solvers using 
    domain decomposition. This interfaces with METIS to perform partitions and build local
    meshes satisfying all overlap and stencil requirements.
*/

#include "CPMesh.H"

namespace DDCPM {

  CPMeshLocal::CPMeshLocal(ProblemDefinition& a_probDef,CPMeshGlobal& a_globMesh,
			   DiffEq& a_eq,PetscInt a_actLoIdx,PetscInt a_actUpIdx,
			   PetscBool a_isRobFO,
			   PetscReal a_osmAlpha,PetscReal a_osmAlphaCross):
    CPMeshBase(a_probDef,a_eq,a_actLoIdx,a_actUpIdx,a_globMesh.getCenter()),
    m_globMesh(a_globMesh),
    m_isRobFO(a_isRobFO),
    m_osmAlpha(a_osmAlpha),
    m_osmAlphaCross(a_osmAlphaCross),
    m_nActiveBC(0),
    m_overFrac(0.0),
    m_activeBC(a_probDef.mesh.dim,true)
  {
    PetscLogEvent LOCAL_MESHING;
    PetscLogEventRegister("Local Meshing",0,&LOCAL_MESHING);
    PetscLogEventBegin(LOCAL_MESHING,0,0,0,0);
    if (m_probDef.verbosity>2) {
      PPS_VTWO("Constructing local mesh\n");
    }
    try {
      // Number of points in interpolation stencil
      for (PetscInt nC=0; nC<m_probDef.mesh.dim; nC++) {
        m_nIntNodes *= (m_probDef.mesh.ord + 1);
      }
      
      // Populate node lists and map active nodes from the disjoint partition
      for (PetscInt nE=m_actLoIdx; nE<m_actUpIdx; nE++) {
        m_gblToLoc.insert(std::make_pair(nE,nE - m_actLoIdx));
        m_locToGbl.insert(std::make_pair(nE - m_actLoIdx,nE));
        m_active.addLeaf(m_globMesh.getLattice(nE),nE - m_actLoIdx);
      }

      // Mark cross points as they may need different BCs
      flagCrossPoints();
      
      // Active overlap nodes
      std::vector<PetscInt> itfIdxs; // Interface idxs stored by global numbering
      for (PetscInt nO=0; nO<m_probDef.mesh.nOver; nO++) {
        QOTree lapBuff(m_probDef.mesh.dim,false);
        for (auto it=m_active.begin(); it!=m_active.end(); it++) {
          if (nO == 0 && isInterfacial(QOTree::mortonToVec(it->first,
							   m_probDef.mesh.dim)))
            m_overFrac += 1.0;
          TVec<PetscInt> neumNbrs =
            m_globMesh.getNeumannNbrs(QOTree::mortonToVec(it->first,
							  m_probDef.mesh.dim));
          for (unsigned int nN=0; nN<neumNbrs.getDim(); nN++) {
            auto it = m_gblToLoc.find(neumNbrs[nN]);
            // Check that neumNbrs[nN] is globally active and
	    // missing from the local problem so far
            if (-1 < neumNbrs[nN] &&
		neumNbrs[nN] < m_globMesh.getNActive() &&
		it == m_gblToLoc.end()) {
              insertOverNode(neumNbrs[nN],lapBuff);
              // Add last layer of overlap nodes to itfIdxs for
	      // effective boundary locations later
              if (nO == m_probDef.mesh.nOver-1 && m_isRobFO) {
                itfIdxs.emplace_back(neumNbrs[nN]);
              }
            }
          }
        }
        // add everything from this layer to the active nodes
        flushOverNodes(lapBuff);
      }
      m_nActive = m_active.size();
      
      // Find all BC nodes
      // Nodes to complete partial stencils
      // These have to be separated since they hold the BCs on the local problem
      for (auto it=m_active.begin(); it!=m_active.end(); it++) {
        std::vector<TVec<uint32_t> > stencil(getMissingStencil(it->first));
        for (unsigned int nS=0; nS<stencil.size(); nS++) {
	  // Index of missing node in global numbering
          PetscInt g_idx = m_globMesh.getIDX(stencil[nS]);
          if (g_idx < m_globMesh.getNActive()) { // add it as BC node
            if (m_gblToLoc.find(g_idx) == m_gblToLoc.end()) {
              insertBCNode(g_idx);
            }
          } else {
            if (!m_ghost.hasLeaf(stencil[nS])) { // ghost in the global problem
              m_ghost.addLeaf(stencil[nS],m_ghost.size());
            }
          }
        }
      }
      
      // Form the active interpolation stencils and reindex as needed 
      for (auto it=m_active.begin(); it!=m_active.end(); it++) {
        TVec<PetscInt> intNodes =
          m_globMesh.getInterpNodes(QOTree::mortonToVec(it->first,
							m_probDef.mesh.dim));
        for (unsigned int nN=0; nN<intNodes.getDim(); nN++) {
          auto it = m_gblToLoc.find(intNodes[nN]);
          if (-1 < intNodes[nN] && intNodes[nN] < m_globMesh.getNActive()) {
            if (it == m_gblToLoc.end()) {
              insertBCNode(intNodes[nN]);
            }
          } else {
            ERRORTRACE("Active node in CPMeshLocal has non-active interpolation node\n");
          }
        }
      }
      
      // Any BC nodes from global ghost extension stencils
      for (auto it=m_ghost.begin(); it!=m_ghost.end(); it++) {
        TVec<PetscInt> intNodes =
          m_globMesh.getInterpNodes(QOTree::mortonToVec(it->first,
							m_probDef.mesh.dim));
        for (unsigned int nN=0; nN<intNodes.getDim(); nN++) {
          auto it = m_gblToLoc.find(intNodes[nN]);
          if (-1 < intNodes[nN] && intNodes[nN] < m_globMesh.getNActive()) {
            if (it == m_gblToLoc.end()) {
              insertBCNode(intNodes[nN]);
            }
          } else {
            ERRORTRACE("Ghost node in CPMeshLocal has non-active interpolation node\n");
          }
        }
      }
      m_nActiveBC = m_activeBC.size();
      m_overFrac = m_nActive + m_nActiveBC - (m_actUpIdx - m_actLoIdx);
      m_overFrac /= ((PetscReal) m_nActive + m_nActiveBC);
      
      // Put the global ghosts into the local node map,reindex their local storage
      m_ghost.offsetValues(m_nActiveBC + m_nActive);
      PetscInt ghostCut = m_nActiveBC + m_nActive + m_ghost.size();
      for (auto it=m_ghost.begin(); it!=m_ghost.end(); it++) {
        PetscInt g_idx = m_globMesh.getIDX(QOTree::mortonToVec(it->first,
							       m_probDef.mesh.dim));
        m_gblToLoc.insert(std::make_pair(g_idx,it->second));
        m_locToGbl.insert(std::make_pair(it->second,g_idx));
      }
      m_nGhost = m_ghost.size();
      
      // Finally, enforce robin conditions if ORAS is being used
      if (m_isRobFO) {
        // Robin conditions need ghosts around the BC nodes as well
        PetscInt gCtr = 0;
        for (auto it=m_activeBC.begin(); it!=m_activeBC.end(); it++) {
          std::vector<TVec<uint32_t> > stencil(getMissingStencil(it->first));
          for (unsigned int nS=0; nS<stencil.size(); nS++) {
	    // Index of missing node in global numbering
            PetscInt g_idx = m_globMesh.getIDX(stencil[nS]); 
            auto it = m_gblToLoc.find(g_idx);
            if (it == m_gblToLoc.end()) { // its a new node
              m_gblToLoc.insert(std::make_pair(g_idx,ghostCut+gCtr));
              m_locToGbl.insert(std::make_pair(ghostCut + gCtr,g_idx));
              m_ghost.addLeaf(stencil[nS],ghostCut + gCtr);
              gCtr++;
            }
          }
        }
        m_nGhost = m_ghost.size();
	
        // Propagate the eff. bnd. locations out to the boundary and ghost nodes
        PetscInt nGstBnd = m_nGhost - ghostCut + m_nActive + m_nActiveBC;
        std::vector<PetscInt> locActBndIdx(m_nActiveBC,0);
        std::vector<PetscInt> locGstBndIdx(nGstBnd,0);
        m_locActBoundary.resize(m_nActiveBC,TVec<PetscReal>(m_probDef.mesh.dim));
        std::vector<TVec<PetscReal> > locGstConorms(nGstBnd,
						    TVec<PetscReal>(m_probDef.mesh.dim));
        std::vector<PetscReal> aBndDist(m_nActiveBC,
					std::numeric_limits<PetscReal>::max());
        std::vector<PetscReal> gBndDist(nGstBnd,
					std::numeric_limits<PetscReal>::max());
        TVec<uint32_t> latRad(m_probDef.mesh.dim,m_probDef.mesh.ord+10);
        for (unsigned int nE=0; nE<itfIdxs.size(); nE++) {
          TVec<PetscReal> center = m_globMesh.getCP(itfIdxs[nE]);
          TVec<uint32_t> basePoint = realToIntPos(center);
          TVec<uint32_t> bndBox = basePoint + latRad; // Inclusive
	  
          // Shift and ensure that the lower bound doesn't wrap
          basePoint -= latRad;
          for (PetscInt nD=0; nD<m_probDef.mesh.dim; nD++) {
            if (basePoint[nD] > bndBox[nD]) {
              basePoint[nD] = 0;
            }
          }
	  
          // Check if this node is BC or BC ghost
          TVec<uint32_t> p = basePoint;
          do {
            PetscInt abcIdx = m_activeBC.getDLoc(p);
            PetscInt gstIdx = m_ghost.getDLoc(p);
            TVec<PetscReal> sep = intToRealPos(p) - center;
            PetscReal sepD = sep.norm();
            if (abcIdx >= 0) { //boundary node
              if (sepD < aBndDist[abcIdx - m_nActive]) { //better match
                locActBndIdx[abcIdx - m_nActive] = itfIdxs[nE];
                m_locActBoundary[abcIdx - m_nActive] = center;
                aBndDist[abcIdx - m_nActive] = sepD;
              }
            } else if (gstIdx >= ghostCut) {
	      // ghost node not induced by global problem
              if (sepD < gBndDist[gstIdx - ghostCut]) { //better match
                locGstBndIdx[gstIdx - ghostCut] = itfIdxs[nE];
                gBndDist[gstIdx - ghostCut] = sepD;
              }
            }
          } while(p.incMod(basePoint,bndBox));
        }
	
        // Flag all nodes near the cross points to use larger alpha values
        if (2*m_probDef.mesh.nOver >= m_probDef.mesh.ord - 2) {
          latRad += TVec<uint32_t>(m_probDef.mesh.dim,
				   2*m_probDef.mesh.nOver - m_probDef.mesh.ord - 2);
        }
	
        // Flag all nodes effected by the crosspoint modification
        std::set<PetscInt> robinModified;
        PetscReal modRad = 2.0*m_probDef.mesh.delta*m_probDef.mesh.nOver;
        for (unsigned int nC=0; nC<m_crossPoints.size(); nC++) {
          TVec<uint32_t> basePoint(QOTree::mortonToVec(m_crossPoints[nC],
						       m_probDef.mesh.dim));
          TVec<uint32_t> bndBox(basePoint); // Inclusive
          TVec<PetscReal> center(intToRealPos(basePoint));
          // Shift
          basePoint.subtract(latRad);
          bndBox.add(latRad);
          for (PetscInt nD=0; nD<m_probDef.mesh.dim; nD++) {
            if (basePoint[nD]>bndBox[nD]) { basePoint[nD] = 0; }
          }
	  
          // Check if this node is BC or BC ghost
          TVec<uint32_t> p(basePoint);
          do {
            PetscInt abcIdx = m_activeBC.getDLoc(p);
            PetscInt gstIdx = m_ghost.getDLoc(p);
            TVec<PetscReal> sep(intToRealPos(p));
            sep.subtract(center);
            PetscReal sepD = sep.norm();
            if (sepD<modRad) {
              if (-1<abcIdx) {
                robinModified.emplace(abcIdx);
              } else if (ghostCut<=gstIdx) {
                robinModified.emplace(gstIdx);
              }
            }
          } while(p.incMod(basePoint,bndBox));
        }
	
        // Find boundary conormals and build transmission conditions
        tentativeConorms(ghostCut,locActBndIdx,locGstBndIdx,locGstConorms);
        for (auto it=m_activeBC.begin(); it!=m_activeBC.end(); it++) {
          findRobinBC(it->second,locActBndIdx[it->second-m_nActive],
		      m_locActConorms[it->second-m_nActive],
		      robinModified.find(it->second) != robinModified.end());
        }
	
        for (auto it=m_ghost.begin(); it!=m_ghost.end(); it++) {
          if (it->second>=ghostCut) {
            findRobinGBC(it->second,locGstBndIdx[it->second-ghostCut],
			 locGstConorms[it->second-ghostCut],
			 robinModified.find(it->second) != robinModified.end());
          }
        }
      } else {
        // Dirichlet conditions need identity map over BC nodes
        for (auto it=m_activeBC.begin(); it!=m_activeBC.end(); it++) {
          m_cpInterpNodes.insert(std::make_pair(it->second,
						TVec<PetscInt>(1,it->second)));
          m_cpInterpWeights.insert(std::make_pair(it->second,
						  TVec<PetscReal>(1,1.0)));
        }
      }
    } catch(ErrorTrace& e) {
      PUSHERROR(e,"Local mesh construction failed");
      throw e;
    }
    
    if (m_probDef.verbosity>3) {
        PPS_VTHREE("Local mesh has %d active nodes, %d ghost nodes, and %d active BCs\n",m_nActive,m_nGhost,m_nActiveBC);
    }
    
    PetscLogEventEnd(LOCAL_MESHING,0,0,0,0);
  }

  void CPMeshLocal::insertOverNode(PetscInt g_idx,QOTree& buffTree)
  {
    PetscInt locIdx = m_active.size() + buffTree.size();
    TVec<uint32_t> lat(m_globMesh.getLattice(g_idx));
    m_gblToLoc.insert(std::make_pair(g_idx,locIdx));
    m_locToGbl.insert(std::make_pair(locIdx,g_idx));
    buffTree.addLeaf(lat,locIdx);
    m_activeOverOwn.emplace_back(g_idx);
  }

  void CPMeshLocal::flushOverNodes(const QOTree& buffTree)
  {
    for (auto it=buffTree.begin(); it!=buffTree.end(); it++) {
      m_active.addLeaf(it->first,it->second);
    }
  }

  void CPMeshLocal::insertBCNode(PetscInt g_idx)
  {
    PetscInt locIdx = m_nActive + m_activeBC.size();
    TVec<uint32_t> lat(m_globMesh.getLattice(g_idx));
    m_gblToLoc.insert(std::make_pair(g_idx,locIdx));
    m_locToGbl.insert(std::make_pair(locIdx,g_idx));
    m_activeBC.addLeaf(lat,locIdx);
    m_activeBCOwn.emplace_back(g_idx);
  }

  void CPMeshLocal::tentativeConorms(PetscInt a_ghostCut,
				     const std::vector<PetscInt>& locActBndIdx,
				     const std::vector<PetscInt>& locGstBndIdx,
				     std::vector<TVec<PetscReal> >& locGstConorms)
  {
    PetscInt embDim = m_probDef.mesh.dim;
    m_locActConorms.resize(m_nActiveBC,embDim);
    for (auto it=m_activeBC.begin(); it!=m_activeBC.end(); it++) {
      PetscInt idx = it->second-m_nActive;
      m_locActConorms[idx] =
        m_globMesh.closestPoint(intToRealPos(QOTree::mortonToVec(it->first,
								 embDim)));
      m_locActConorms[idx] -= m_locActBoundary[idx];
      m_locActConorms[idx].orthogonalize(m_globMesh.getSN(locActBndIdx[idx]));
      m_locActConorms[idx].normalize();
    }
    
    for (auto it=m_ghost.begin(); it!=m_ghost.end(); it++) {
      if (it->second >= a_ghostCut) {
        PetscInt idx = it->second - a_ghostCut;
        locGstConorms[idx] =
          m_globMesh.closestPoint(intToRealPos(QOTree::mortonToVec(it->first,
								   embDim)));
        locGstConorms[idx] -= m_globMesh.getCP(locGstBndIdx[idx]);
        locGstConorms[idx].orthogonalize(m_globMesh.getSN(locGstBndIdx[idx]));
        locGstConorms[idx].normalize();
      }
    }
  }
  
  // index exchanges,meant to be called from within local problems
  PetscInt CPMeshLocal::localToGlobalIdx(PetscInt a_idx)
  {
    PetscLogEvent L2G_IDX;
    PetscLogEventRegister("L2G idx",0,&L2G_IDX);
    PetscLogEventBegin(L2G_IDX,0,0,0,0);

    auto it = m_locToGbl.find(a_idx);
    if (it != m_locToGbl.end()) {
      PetscLogEventEnd(L2G_IDX,0,0,0,0);
      return it->second;
    }

    PetscLogEventEnd(L2G_IDX,0,0,0,0);
    return DLOC_DEFAULT;
  }

  TVec<PetscInt> CPMeshLocal::globalToLocalIdx(const TVec<PetscInt>& g_idxs)
  {
    PetscLogEvent G2L_IDX;
    PetscLogEventRegister("G2L idx",0,&G2L_IDX);
    PetscLogEventBegin(G2L_IDX,0,0,0,0);

    TVec<PetscInt> locIdxs(g_idxs.getDim());
    for (unsigned int nC=0; nC<g_idxs.getDim(); nC++) {
      auto it = m_gblToLoc.find(g_idxs[nC]);
      if (it != m_gblToLoc.end()) {
        locIdxs[nC] = it->second;
      } else {
        locIdxs[nC] = DLOC_DEFAULT;
      }
    }
    
    PetscLogEventEnd(G2L_IDX,0,0,0,0);
    return locIdxs;
  }

  TVec<PetscInt> CPMeshLocal::localToGlobalIdx(const TVec<PetscInt>& a_idxs)
  {
    TVec<PetscInt> gblIdxs(a_idxs.getDim());
    for (unsigned int nC=0; nC<a_idxs.getDim(); nC++) {
      gblIdxs[nC] = localToGlobalIdx(a_idxs[nC]);
    }
    
    return gblIdxs;
  }

  bool CPMeshLocal::isInterfacial(const TVec<uint32_t>& a_lat)
  {
    for (PetscInt nD=0; nD<m_probDef.mesh.dim; nD++) {
      for (PetscInt nS=0; nS<2; nS++) {
        TVec<uint32_t> nbr(a_lat);
        nbr[nD] = nbr[nD] + 1 - 2*nS;
        if (m_active.getDLoc(nbr) < 0) { // nbr is not active locally
          PetscInt g_idx = m_globMesh.getIDX(nbr);
          if (-1 < g_idx && g_idx < m_globMesh.getNActive()) {
	    // but is active globally
            return true;
	  }
        } else if (-1 < m_activeBC.getDLoc(nbr)) { // or is a BC node
          return true;
        }
      }
    }
    
    return false;
  }

// Handle to the resident mesh structures from the outside
  PetscInt CPMeshLocal::getIDX(const TVec<uint32_t>& a_lat) const
  {
    PetscLogEvent GET_IDX;
    PetscLogEventRegister("Get idx",0,&GET_IDX);
    PetscLogEventBegin(GET_IDX,0,0,0,0);
  
    PetscInt aIdx = m_active.getDLoc(a_lat);
    if (aIdx >= 0) {
      PetscLogEventEnd(GET_IDX,0,0,0,0);
      return aIdx;
    }
    
    PetscInt gIdx = m_ghost.getDLoc(a_lat);
    if (gIdx >=0 ) {
      PetscLogEventEnd(GET_IDX,0,0,0,0);
      return gIdx;
    }
    
    PetscLogEventEnd(GET_IDX,0,0,0,0);
    return m_activeBC.getDLoc(a_lat);
  }

  void CPMeshLocal::flagCrossPoints()
  {
    for (auto it=m_active.begin(); it!=m_active.end(); it++) {
      TVec<uint32_t> lat = QOTree::mortonToVec(it->first,m_probDef.mesh.dim);
      TVec<PetscInt> vNbrParts(m_globMesh.getVoronoiNbrs(lat));
      for (unsigned int nV=0; nV<vNbrParts.getDim(); nV++) {
        vNbrParts[nV] = m_globMesh.getPartition(vNbrParts[nV]);
      }
      
      PetscInt minPart=vNbrParts.min(),maxPart=vNbrParts.max();
      for (unsigned int nV=0; nV<vNbrParts.getDim(); nV++) {
        if (vNbrParts[nV] != minPart && vNbrParts[nV] != maxPart) {
          // Node neighbors more than two subdomains
          m_crossPoints.push_back(it->first);
          break;
        }
      }
    }
  }

  TVec<uint32_t> CPMeshLocal::getLattice(PetscInt a_idx) const
  {
    if (a_idx < m_nActive) {
      return m_active.getVecByVal(a_idx);
    } else if (a_idx < m_nActive+m_nActiveBC) {
      return m_activeBC.getVecByVal(a_idx);
    } else {
      return m_ghost.getVecByVal(a_idx);
    }
    ERRORTRACE("Local lattice lookup failed. Sought a_idx: %d\n");
    return TVec<uint32_t>(m_probDef.mesh.dim);
  }

  TVec<PetscReal> CPMeshLocal::getActiveBCPos(PetscInt a_idx) const
  {
    return intToRealPos(m_activeBC.getVecByVal(a_idx));
  }

  TVec<PetscReal> CPMeshLocal::getActivePos(PetscInt a_idx) const
  {
    return intToRealPos(m_active.getVecByVal(a_idx));
  }

  TVec<PetscReal> CPMeshLocal::getGhostPos(PetscInt a_idx) const
  {
    return intToRealPos(m_ghost.getVecByVal(a_idx));
  }

  void CPMeshLocal::getBCOwn(std::vector<PetscInt>& idxs) const
  {
    for (PetscInt nBC=0; nBC<m_nActiveBC; nBC++) {
      idxs.push_back(m_activeBCOwn[nBC]);
    }
  }

  void CPMeshLocal::getOverOwn(std::vector<PetscInt>& idxs) const
  {
    for (unsigned int nO=0; nO<m_activeOverOwn.size(); nO++) {
      idxs.push_back(m_activeOverOwn[nO]);
    }
  }

  TVec<PetscInt> CPMeshLocal::getInterpNodes(PetscInt a_idx)
  {
    auto it = m_cpInterpNodes.find(a_idx);
    if (it == m_cpInterpNodes.end()) {
      try {
        return globalToLocalIdx(m_globMesh.getInterpNodes(localToGlobalIdx(a_idx)));
      } catch(ErrorTrace& e) {
        PUSHERROR(e,"Interpolation failed on index " + std::to_string(a_idx));
        throw e;
      }
    }
    return it->second;
  }

  TVec<PetscInt> CPMeshLocal::getInterpNodes(const TVec<uint32_t>& a_lat)
  {
    try {
      return getInterpNodes(getIDX(a_lat));
    } catch(ErrorTrace& e) {
      throw e;
    }
  }

  TVec<PetscReal> CPMeshLocal::getInterpWeights(PetscInt a_idx)
  {
    auto it = m_cpInterpWeights.find(a_idx);
    if (it == m_cpInterpWeights.end()) {
      try {
        return m_globMesh.getInterpWeights(localToGlobalIdx(a_idx));
      } catch(ErrorTrace& e) {
        PUSHERROR(e,"Interpolation failed on index " + std::to_string(a_idx));
        throw e;
      }
    }
    return it->second;
  }

  TVec<PetscReal> CPMeshLocal::getInterpWeights(const TVec<uint32_t>& a_lat)
  {
    return getInterpWeights(getIDX(a_lat));
  }

  // Robin boundary conditions
  void CPMeshLocal::findRobinBC(PetscInt a_bcIdx,PetscInt a_itfIdx,
                                const TVec<PetscReal>& conorm,bool a_modRob)
  {
    try {
      // Get the stencil from the global problem
      TVec<PetscInt> intNodes = m_globMesh.getInterpNodes(a_itfIdx);
      TVec<PetscReal> intWeights = m_globMesh.getInterpWeights(a_itfIdx);
      for (unsigned int nI=0; nI<intNodes.getDim(); nI++) {
        auto it = m_gblToLoc.find(intNodes[nI]);
        if (it != m_gblToLoc.end()) {
          intNodes[nI] = it->second;
        } else {
          ERRORTRACE("Interp node missing in CPMeshLocal::findRobinBC\n");
          intNodes[nI] = -1;
        }
      }
      
      // Scale weights to enforce Robin condition
      TVec<PetscReal> dir = intToRealPos(m_activeBC.getVecByVal(a_bcIdx)) -
	m_globMesh.getCP(a_itfIdx);
      PetscReal rFac = dir.innerProduct(conorm);
      rFac = std::isnan(rFac) ? 0.0 : rFac;
      PetscReal alpha = a_modRob ? m_osmAlphaCross : m_osmAlpha;
      intWeights.scale(1.0/(1.0 + alpha*rFac));
      
      // Insert
      m_cpInterpNodes.insert(std::make_pair(a_bcIdx,intNodes));
      m_cpInterpWeights.insert(std::make_pair(a_bcIdx,intWeights));
    } catch(ErrorTrace& e) {
      PUSHERROR(e,"Robin boundary condition failed");
      throw e;
    }
  }
  
  void CPMeshLocal::findRobinGBC(PetscInt a_bcIdx,PetscInt a_itfIdx,
                                 const TVec<PetscReal>& conorm,bool a_modRob)
  {
    try {
      // Get the stencil from the global problem
      TVec<PetscInt> intNodes = m_globMesh.getInterpNodes(a_itfIdx);
      TVec<PetscReal> intWeights = m_globMesh.getInterpWeights(a_itfIdx);
      for (unsigned int nI=0; nI<intNodes.getDim(); nI++) {
        auto it = m_gblToLoc.find(intNodes[nI]);
        if (it != m_gblToLoc.end()) {
          intNodes[nI] = it->second;
        } else {
          ERRORTRACE("Interp node missing in CPMeshLocal::findRobinGBC\n");
          intNodes[nI] = -1;
        }
      }
      
      // Scale weights to enforce Robin condition
      TVec<PetscReal> dir = intToRealPos(m_ghost.getVecByVal(a_bcIdx)) -
	m_globMesh.getCP(a_itfIdx);
      PetscReal rFac = dir.innerProduct(conorm);
      rFac = std::isnan(rFac) ? 0.0 : rFac;
      PetscReal alpha = a_modRob ? m_osmAlphaCross : m_osmAlpha;
      intWeights.scale(1.0/(1.0 + alpha*rFac));
      
      // Insert
      m_cpInterpNodes.insert(std::make_pair(a_bcIdx,intNodes));
      m_cpInterpWeights.insert(std::make_pair(a_bcIdx,intWeights));
    } catch(ErrorTrace& e) {
      PUSHERROR(e,"Robin boundary condition failed");
      throw e;
    }
  }

  TVec<PetscReal> CPMeshLocal::getBoundaryConorm(PetscInt a_idx) const
  {
    if (m_isRobFO) {
      return m_locActConorms[a_idx];
    } else {
      return TVec<PetscReal>(m_probDef.mesh.dim);
    }
  }

} // Close namespace
