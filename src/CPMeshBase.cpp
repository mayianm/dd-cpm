// CPMeshBase Class Implementation
// Author: Ian May
// Purpose: 
/*! \file CPMeshGlobal.H
    \brief Provides a mesh data structure for Closest Point surface pde solvers using domain
    decomposition. This interfaces with METIS to perform partitions and build local meshes
    satisfying all overlap and stencil requirements.
*/

#include "CPMesh.H"

namespace DDCPM {

  CPMeshBase::CPMeshBase(ProblemDefinition& a_probDef,DiffEq& a_eq):
    m_probDef(a_probDef),
    m_nIntNodes(1),
    m_nActive(0),
    m_nGhost(0),
    g_pi(4.0*PetscAtanReal(1.0)),
    m_center(a_probDef.mesh.dim),
    m_eq(a_eq),
    m_active(a_probDef.mesh.dim,true),
    m_ghost(a_probDef.mesh.dim,true)
  {
    for (int nD=0; nD<m_probDef.mesh.dim; nD++) {
      m_nIntNodes *= (m_probDef.mesh.ord + 1);
    }
    
    PetscReal d_dim = m_probDef.mesh.dim;
    PetscReal d_ord = m_probDef.mesh.ord + 1.01;
    m_gamma = (d_ord*PetscSqrtReal(d_dim)/2.0)*m_probDef.mesh.delta;
  }

  CPMeshBase::CPMeshBase(ProblemDefinition& a_probDef,DiffEq& a_eq,
			 PetscInt a_actLoIdx,PetscInt a_actUpIdx,
			 const TVec<uint32_t>& a_center):
    m_probDef(a_probDef),
    m_nIntNodes(1),
    m_nActive(0),
    m_nGhost(0),
    m_actLoIdx(a_actLoIdx),
    m_actUpIdx(a_actUpIdx),
    m_gamma(0.0),
    g_pi(4.0*PetscAtanReal(1.0)),
    m_center(a_center),
    m_eq(a_eq),
    m_active(a_probDef.mesh.dim,true),
    m_ghost(a_probDef.mesh.dim,true)
  {  
    PetscReal d_dim = m_probDef.mesh.dim;
    PetscReal d_ord = m_probDef.mesh.ord + 1.01;
    m_gamma = (d_ord*PetscSqrtReal(d_dim)/2.0)*m_probDef.mesh.delta;
  }

  TVec<PetscReal> CPMeshBase::intToRealPos(const TVec<uint32_t>& p) const
  {
    TVec<PetscReal> x(m_probDef.mesh.dim);
    for (PetscInt nC=0; nC<m_probDef.mesh.dim; nC++) {
      PetscInt pC = p[nC];
      PetscInt cC = m_center[nC];
      x[nC] = ((PetscReal)(pC - cC))*m_probDef.mesh.delta;
    }
    return x;
  }

  TVec<uint32_t> CPMeshBase::realToIntPosRound(const TVec<PetscReal>& x) const
  {
    TVec<uint32_t> p(m_center);
    for (PetscInt nC=0; nC<m_probDef.mesh.dim; nC++) {
      PetscReal pc = round(x[nC]/m_probDef.mesh.delta);
      p[nC] += ((uint32_t) pc);
    }
    return p;
  }

  TVec<uint32_t> CPMeshBase::realToIntPosFloor(const TVec<PetscReal>& x) const
  {
    TVec<uint32_t> p(m_center);
    for (PetscInt nC=0; nC<m_probDef.mesh.dim; nC++) {
      PetscReal pc = floor(x[nC]/m_probDef.mesh.delta + 0.01);
      p[nC] += ((uint32_t) pc);
    }
    return p;
  }
  
  TVec<uint32_t> CPMeshBase::realToIntPos(const TVec<PetscReal>& x) const
  {
    return m_probDef.mesh.ord%2 == 0 ? realToIntPosRound(x) : realToIntPosFloor(x);
  }

  std::array<PetscReal,3> CPMeshBase::intToRealPos(const std::array<uint32_t,3>& p) const
  {
    std::array<PetscReal,3> x;
    for (PetscInt nC=0; nC<3; nC++) {
      PetscInt pC = p[nC];
      PetscInt cC = m_center[nC];
      x[nC] = ((PetscReal)(pC - cC))*m_probDef.mesh.delta;
    }
    return x;
  }

  PetscInt CPMeshBase::getRelIDX(PetscInt a_idx,const TVec<PetscInt>& a_off) const
  {
    PetscLogEvent GET_RELIDX;
    PetscLogEventRegister("Get rel idx",0,&GET_RELIDX);
    PetscLogEventBegin(GET_RELIDX,0,0,0,0);

    TVec<uint32_t> lat = getLattice(a_idx);
    lat.addInt(a_off);

    PetscLogEventEnd(GET_RELIDX,0,0,0,0);
    return getIDX(lat);
  }

  std::vector<TVec<uint32_t> > CPMeshBase::getMissingStencil(uint64_t a_lat)
  {
    std::vector<TVec<uint32_t> > missing;
    for (const TVec<PetscInt>& gPos : m_eq.getGFunc()) {
      TVec<uint32_t> tmp(QOTree::mortonToVec(a_lat,m_probDef.mesh.dim));
      tmp.addInt(gPos);
      if (getIDX(tmp) < 0) {
        missing.emplace_back(tmp);
      }
    }
    return missing;
  }

  TVec<PetscReal> CPMeshBase::findTotalWeight(const TVec<uint32_t>& baseIntCP,
                                              const TVec<PetscReal>& a_cp) const
  {
    PetscLogEvent INTERP_WEIGHT;
    PetscLogEventRegister("Interp tot wts",0,&INTERP_WEIGHT);
    PetscLogEventBegin(INTERP_WEIGHT,0,0,0,0);

    TVec<PetscReal> totWt(m_probDef.mesh.dim);
    TVec<PetscReal> baseCoord = intToRealPos(baseIntCP);
    for (PetscInt nD=0; nD<m_probDef.mesh.dim; nD++) {
      PetscReal wd = 0.0;
      PetscReal x = a_cp[nD];
      for (PetscInt nP=0; nP<m_probDef.mesh.ord + 1; nP++) {
        PetscReal xj = baseCoord[nD] + nP*m_probDef.mesh.delta;
        PetscReal wj = PetscPowReal(-1.0,nP)*nChoosek(m_probDef.mesh.ord,nP);
        wd += wj/(x - xj);
      }
      totWt[nD] = wd;
    }
  
    PetscLogEventEnd(INTERP_WEIGHT,0,0,0,0);
    return totWt;
  }

  TVec<PetscReal> CPMeshBase::findPointWeights(const TVec<uint32_t>& baseIntCP,
                                               const TVec<PetscReal>& cp,
                                               const TVec<PetscReal>& totWt,
                                               PetscInt a_dim) const
  {
    PetscLogEvent INTERP_WEIGHTS;
    PetscLogEventRegister("Interp pt wts",0,&INTERP_WEIGHTS);
    PetscLogEventBegin(INTERP_WEIGHTS,0,0,0,0);

    TVec<PetscReal> weights(m_probDef.mesh.ord + 1);
    TVec<PetscReal> baseCoord = intToRealPos(baseIntCP);
    for (PetscInt nO=0; nO<m_probDef.mesh.ord + 1; nO++) {
      PetscReal xj = baseCoord[a_dim] + nO*m_probDef.mesh.delta;
      PetscReal wj = PetscPowReal(-1.0,nO)*nChoosek(m_probDef.mesh.ord,nO);
      PetscReal x = cp[a_dim];
      PetscReal w = wj/(x - xj);
      PetscReal tw = totWt[a_dim];
      if (!std::isinf(w) && !std::isinf(tw)) {
        weights[nO] = w/tw;
      } else if (!std::isinf(w) && std::isinf(tw)) {
        weights[nO] = 0.0;
      } else if (std::isinf(w) && std::isinf(tw)) {
        weights[nO] = 1.0;
      } else {
        PP_ERR("Something else happened in findPointWeights()\n");
      }
    }
  
    PetscLogEventEnd(INTERP_WEIGHTS,0,0,0,0);
    return weights;
  }

  PetscReal CPMeshBase::nChoosek(PetscInt n,PetscInt k)
  {
    if (k == 0) {
      return 1.0;
    }
    return (n*nChoosek(n - 1,k - 1))/((PetscReal) k);
  }

} // Close namespace
