#include <limits>
#include <mpi.h>
#include <vector>

#include <petsc.h>
#include <petsc/private/pcimpl.h>
#include <petscoptions.h>

#include "PC_ddcpm.H"
#include "petscsystypes.h"

namespace DDCPM {

  PetscErrorCode PC_ddcpm::PCCreate_ddcpm(PC pc)
  {
    PC_ddcpm *ddcpm;

    // Set the data portion of the PC
    PetscNewLog(pc,&ddcpm);
    pc->data = (void*)ddcpm;

    // Set defaults
    ddcpm->isSetup          = PETSC_FALSE;
    ddcpm->isRobFO          = PETSC_FALSE;
    ddcpm->osmAlpha         = 4.0;
    ddcpm->osmAlphaCross    = 20.0;
    ddcpm->isMultiplicative = PETSC_FALSE;
    ddcpm->nColorsGbl       = 0;
    ddcpm->nColorsLoc       = 0;
    ddcpm->globMesh         = NULL;
    ddcpm->globProb         = NULL;
    ddcpm->nSubProb         = 0;
    ddcpm->colCorr          = NULL;
    ddcpm->colRes           = NULL;
    ddcpm->colPartSol       = NULL;
    ddcpm->bcs              = NULL;
    ddcpm->solFrom          = NULL;
    ddcpm->rhsTo            = NULL;

    // Bind the needed functions for PC registration
    pc->ops->apply               = PC_ddcpm::PCApply_ddcpm;
    pc->ops->applytranspose      = NULL;
    pc->ops->setup               = PC_ddcpm::PCSetup_ddcpm;
    pc->ops->destroy             = PC_ddcpm::PCDestroy_ddcpm;
    pc->ops->setfromoptions      = PC_ddcpm::PCSetFromOptions_ddcpm;
    pc->ops->view                = PC_ddcpm::PCView_ddcpm;
    pc->ops->applyrichardson     = NULL;
    pc->ops->applysymmetricleft  = NULL;
    pc->ops->applysymmetricright = NULL;

    // Bind functions to set options petsc style
    PetscObjectComposeFunction((PetscObject)pc,
			       "PCDDCPMSetUseRobfo_C",
			       PCDDCPMSetUseRobfo_ddcpm);
    PetscObjectComposeFunction((PetscObject)pc,
			       "PCDDCPMSetOSMAlpha_C",
			       PCDDCPMSetOSMAlpha_ddcpm);
    PetscObjectComposeFunction((PetscObject)pc,
			       "PCDDCPMSetOSMAlphaCross_C",
			       PCDDCPMSetOSMAlphaCross_ddcpm);
    PetscObjectComposeFunction((PetscObject)pc,
			       "PCDDCPMSetUseMultiplicative_C",
			       PCDDCPMSetUseMultiplicative_ddcpm);

    return 0;
  }

  PetscErrorCode PC_ddcpm::PCDDCPMSetUseRobfo_ddcpm(PC pc,PetscBool flg)
  {
    PC_ddcpm *ddcpm = (PC_ddcpm*)pc->data;

    ddcpm->isRobFO = flg;

    return 0;
  }

  PetscErrorCode PC_ddcpm::PCDDCPMSetOSMAlpha_ddcpm(PC pc,PetscReal alpha)
  {
    PC_ddcpm *ddcpm = (PC_ddcpm*)pc->data;

    ddcpm->osmAlpha = alpha;

    return 0;
  }

  PetscErrorCode PC_ddcpm::PCDDCPMSetOSMAlphaCross_ddcpm(PC pc,PetscReal alpha)
  {
    PC_ddcpm *ddcpm = (PC_ddcpm*)pc->data;

    ddcpm->osmAlphaCross = alpha;

    return 0;
  }

  PetscErrorCode PC_ddcpm::PCDDCPMSetUseMultiplicative_ddcpm(PC pc,PetscBool flg)
  {
    PC_ddcpm *ddcpm = (PC_ddcpm*)pc->data;

    ddcpm->isMultiplicative = flg;

    return 0;
  }

  PetscErrorCode PC_ddcpm::PCSetFromOptions_ddcpm(PC pc,PetscOptionItems *PetscOptionsObject)
  {
    PC_ddcpm *ddcpm = (PC_ddcpm*)pc->data;
    PetscErrorCode ierr = 0;

    PetscOptionsHeadBegin(PetscOptionsObject,"ddcpm options");
    ierr = PetscOptionsBool("-pc_ddcpm_robfo","Use Robin transmission conditions",
			    "PCDDCPMSetUseRobfo",ddcpm->isRobFO,
			    &ddcpm->isRobFO,NULL); CHKERRQ(ierr);
    ierr = PetscOptionsScalar("-pc_ddcpm_osm_alpha",
			      "Robin transmission coefficient away from cross points","",
			      ddcpm->osmAlpha,&ddcpm->osmAlpha,NULL); CHKERRQ(ierr);
    ierr = PetscOptionsScalar("-pc_ddcpm_osm_alpha_cross",
			      "Robin transmission coefficient near cross points","",
			      ddcpm->osmAlphaCross,&ddcpm->osmAlphaCross,NULL); CHKERRQ(ierr);
    ierr = PetscOptionsBool("-pc_ddcpm_multiplicative","Use multiplicative updates",
			    "PCDDCPMSetUseMultiplicative",ddcpm->isMultiplicative,
			    &ddcpm->isMultiplicative,NULL); CHKERRQ(ierr);
    PetscOptionsHeadEnd();
    
    return ierr;
  }

  PetscErrorCode PC_ddcpm::PCSetContext_ddcpm(PC pc,
					      CPMeshGlobal *mesh,
					      ProblemGlobal *prob)
  {
    PC_ddcpm *ddcpm = (PC_ddcpm*)pc->data;

    ddcpm->globMesh = mesh;
    ddcpm->globProb = prob;
    
    return 0;
  }

  PetscErrorCode PC_ddcpm::PCSetup_ddcpm(PC pc)
  {
    PC_ddcpm *ddcpm = (PC_ddcpm*)pc->data;
    PetscErrorCode ierr = 0;
    PetscInt rank,size;

    if (!ddcpm->isSetup) { // Set up for the first time
      MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
      MPI_Comm_size(PETSC_COMM_WORLD,&size);

      // Color the subdomains of the global mesh for multiplicative solves
      if (ddcpm->isMultiplicative) {
	ddcpm->globMesh->colorSubdomains();
	ddcpm->nColorsGbl = ddcpm->globMesh->getNColors();
      }
    
      // Set the number of local subproblems and their labels/colors
      ddcpm->nSubProb = ddcpm->globProb->m_probDef.mesh.nParts/size;
      ddcpm->subGlobNums = ddcpm->globMesh->getPartsOfRank(rank);
      ddcpm->subColors = ddcpm->globMesh->getPartColors(ddcpm->subGlobNums);
    
      // count number of unique colors on this rank
      ddcpm->nColorsLoc = 0;
      for (PetscInt nS=0; nS<ddcpm->nSubProb; nS++) {
	bool uniq = true;
	for (PetscInt nP=nS + 1; nP<ddcpm->nSubProb; nP++) {
	  if (ddcpm->subColors[nS] == ddcpm->subColors[nP]) {
	    uniq = false;
	    break;
	  }
	}
	if (uniq) {
	  ddcpm->nColorsLoc++;
	}
      }

      // Bounding global indices on each partition
      TVec<PetscInt> partLoIdxs(ddcpm->nSubProb,0);
      TVec<PetscInt> partUpIdxs(ddcpm->nSubProb,0);
    
      // Construct local meshes and problems
      for (PetscInt nP=0; nP<ddcpm->nSubProb; nP++) {
	ddcpm->globMesh->getPartExtents(ddcpm->subGlobNums[nP],
					partLoIdxs[nP],partUpIdxs[nP]);
	ddcpm->subMesh.emplace_back(new CPMeshLocal(ddcpm->globProb->m_probDef,
						    *ddcpm->globMesh,
						    ddcpm->globProb->m_eq,
						    partLoIdxs[nP],partUpIdxs[nP],
						    ddcpm->isRobFO,ddcpm->osmAlpha,
						    ddcpm->osmAlphaCross));
	ddcpm->subProb.emplace_back(new ProblemLocal(ddcpm->globProb->m_probDef,
						     *ddcpm->subMesh[nP],
						     ddcpm->globProb->m_eq,
						     ddcpm->globProb->getShift()));
      }
    
      // Find mappings of local to global indices respecting overlaps and BC nodes
      ierr = ddcpm->findIdxMaps(partLoIdxs,partUpIdxs);

      // Allocate storage for intermediate quantities in multiplicative method
      if (ddcpm->isMultiplicative) {
	ierr = MatCreateVecs(pc->pmat,&ddcpm->colCorr,&ddcpm->colRes); CHKERRQ(ierr);
	ierr = VecDuplicate(ddcpm->colRes,&ddcpm->colPartSol); CHKERRQ(ierr);
      }
    } else { // Set up called again, probably from a time stepper
      // Re-shift all of the local problems
      for (PetscInt nP=0; nP<ddcpm->nSubProb; nP++) {
	PetscReal s = ddcpm->globProb->getShift();
	ddcpm->subProb[nP]->unshift();
	ddcpm->subProb[nP]->shift(s);
      }
    }

    // Need all ranks to finish setting up
    ddcpm->isSetup = PETSC_TRUE;

    return ierr;
  }

  PetscErrorCode PC_ddcpm::PCView_ddcpm(PC pc,PetscViewer viewer)
  {
    PC_ddcpm *ddcpm = (PC_ddcpm*)pc->data;
    PetscErrorCode ierr;
    PetscBool iascii;
    PetscViewerFormat format;

    ierr = PetscObjectTypeCompare((PetscObject)viewer,
				  PETSCVIEWERASCII,&iascii); CHKERRQ(ierr);
    if (iascii) {
      PetscInt rank,size;
      MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
      MPI_Comm_size(PETSC_COMM_WORLD,&size);
      
      // Get info about subdomains
      PetscInt minDjt = std::numeric_limits<PetscInt>::max();
      PetscInt meanDjt = 0,maxDjt = 0,meanOvr = 0;
      
      for (PetscInt nS=0; nS<ddcpm->nSubProb; nS++) {
	PetscInt djn = ddcpm->subMesh[nS]->getNDisjoint();
	minDjt = djn < minDjt ? djn : minDjt;
	maxDjt = djn > maxDjt ? djn : maxDjt;
	meanDjt += djn;
	meanOvr += ddcpm->subMesh[nS]->getNActive();
      }

      meanDjt /= ddcpm->nSubProb;
      meanOvr /= ddcpm->nSubProb;

      // Gather subdomain information to rank 0 and overwrite locals
      std::vector<PetscInt> minDjt_g(size,0),maxDjt_g(size,0),meanDjt_g(size,0),meanOvr_g(size,0);
      std::vector<PetscInt> nColorsLoc_g(size,0);
      PetscReal meanColors = 0.0;
	
      MPI_Gather(&minDjt,1,MPI_INT,minDjt_g.data(),1,MPI_INT,0,PETSC_COMM_WORLD);
      MPI_Gather(&maxDjt,1,MPI_INT,maxDjt_g.data(),1,MPI_INT,0,PETSC_COMM_WORLD);
      MPI_Gather(&meanDjt,1,MPI_INT,meanDjt_g.data(),1,MPI_INT,0,PETSC_COMM_WORLD);
      MPI_Gather(&meanOvr,1,MPI_INT,meanOvr_g.data(),1,MPI_INT,0,PETSC_COMM_WORLD);
      MPI_Gather(&ddcpm->nColorsLoc,1,MPI_INT,nColorsLoc_g.data(),1,MPI_INT,0,PETSC_COMM_WORLD);

      if (rank == 0) {
	meanDjt = 0;
	meanOvr = 0;
	for (PetscInt nR=0; nR<size; nR++) {
	  minDjt = minDjt_g[nR] < minDjt ? minDjt_g[nR] : minDjt;
	  maxDjt = maxDjt_g[nR] > maxDjt ? maxDjt_g[nR] : maxDjt;
	  meanDjt += meanDjt_g[nR];
	  meanOvr += meanOvr_g[nR];
	  meanColors += nColorsLoc_g[nR];
	}
	meanDjt /= size;
	meanOvr /= size;
	meanColors /= size;
      }

      // Print information about the PC
      PetscViewerASCIIPrintf(viewer,"  update type: %s\n",
			     ddcpm->isMultiplicative ? "restricted multiplicative" : "restricted additive");
      if (ddcpm->isMultiplicative) {
	PetscViewerASCIIPrintf(viewer,"    global number of colors: %d\n",ddcpm->nColorsGbl);
	PetscViewerASCIIPrintf(viewer,"    average colors per rank: %f\n",meanColors);
      }
      PetscViewerASCIIPrintf(viewer,"  transmission conditions: %s\n",ddcpm->isRobFO ? "Robin" : "Dirichlet");
      if (ddcpm->isRobFO) {
	PetscViewerASCIIPrintf(viewer,"    alpha: %f\n",ddcpm->osmAlpha);
	PetscViewerASCIIPrintf(viewer,"    alpha cross: %f\n",ddcpm->osmAlphaCross);
      }
      PetscViewerASCIIPrintf(viewer,"  subdomain stats: %d subdomains per rank\n",ddcpm->nSubProb);
      PetscViewerASCIIPrintf(viewer,"    Rank | min disjoint | max disjoint | avg disjoint | avg overlapped\n");
      PetscViewerASCIIPrintf(viewer,"     all | %12d | %12d | %12d | %14d\n",minDjt,maxDjt,meanDjt,meanOvr);
      
      PetscViewerGetFormat(viewer, &format);
      if (format == PETSC_VIEWER_ASCII_INFO_DETAIL) {
	for (PetscInt nR=0; nR<size; nR++) {
	  PetscViewerASCIIPrintf(viewer,"    %4d | %12d | %12d | %12d | %14d\n",
				 nR,minDjt_g[nR],maxDjt_g[nR],meanDjt_g[nR],meanOvr_g[nR]);
	}
      }
    }

    return ierr;
  }

  PetscErrorCode PC_ddcpm::PCApply_ddcpm(PC pc,Vec xin,Vec xout)
  {
    PC_ddcpm *ddcpm = (PC_ddcpm*)pc->data;
    PetscErrorCode ierr;

    // Setup the preconditioner if not already done
    if (!ddcpm->isSetup) {
      ierr = PCSetup_ddcpm(pc); CHKERRQ(ierr);
    }
    
    // Call desired application method
    if (true) {
      ierr = ddcpm->rasShell(xin,xout); CHKERRQ(ierr);
    } else {
      ierr = ddcpm->rmsShell(xin,xout); CHKERRQ(ierr);
    }

    return ierr;
  }

  PetscErrorCode PC_ddcpm::PCDestroy_ddcpm(PC pc)
  {
    PC_ddcpm *ddcpm = (PC_ddcpm*)pc->data;
    PetscErrorCode ierr;

    ddcpm->subProb.clear();
    ddcpm->subProb.shrink_to_fit();
    ddcpm->subMesh.clear();
    ddcpm->subMesh.shrink_to_fit();
    
    if (!ddcpm->colCorr) {
      ierr = VecDestroy(&ddcpm->colCorr);    CHKERRQ(ierr);
    }
    if (!ddcpm->colRes) {
      ierr = VecDestroy(&ddcpm->colRes);     CHKERRQ(ierr);
    }
    
    if (!ddcpm->colPartSol) {
      ierr = VecDestroy(&ddcpm->colPartSol); CHKERRQ(ierr);
    }

    // Unbind additional functions
    PetscObjectComposeFunction((PetscObject)pc,
			       "PCDDCPMSetUseRobfo_C",
			       NULL);
    PetscObjectComposeFunction((PetscObject)pc,
			       "PCDDCPMSetOSMAlpha_C",
			       NULL);
    PetscObjectComposeFunction((PetscObject)pc,
			       "PCDDCPMSetOSMAlphaCross_C",
			       NULL);
    PetscObjectComposeFunction((PetscObject)pc,
			       "PCDDCPMSetUseMultiplicative_C",
			       NULL);
    
    // Free the ddcpm data structure inside of pc
    ierr = PetscFree(pc->data); CHKERRQ(ierr);
    
    return ierr;
  }
  
  PetscErrorCode PC_ddcpm::findIdxMaps(const TVec<PetscInt>& a_partLoIdxs,
				       const TVec<PetscInt>& a_partUpIdxs)
  {
    PetscErrorCode ierr = 0;
    PetscInt nComps = globProb->m_nComps;
    
    // Disjoint node ranges
    for (PetscInt nS=0; nS<nSubProb; nS++) {
      // Number of disjoint dofs in partition nS
      nSet.push_back(nComps*(a_partUpIdxs[nS]-a_partLoIdxs[nS]));
      
      // Global indices of these dofs
      idxSolTo.emplace_back(std::vector<PetscInt>(nSet[nS],0));
      
      // Local indices of these dofs
      idxSolFrom.emplace_back(std::vector<PetscInt>(nSet[nS],0));
      for (PetscInt nI=0; nI<nSet[nS]; nI++) {
        idxSolFrom[nS][nI] = nI;
        idxSolTo[nS][nI] = nComps*a_partLoIdxs[nS] + nI;
      }
    }
    
    // set up BC communicator indices
    std::vector<std::vector<PetscInt> > idxBCFrom;
    for (PetscInt nS=0; nS<nSubProb; nS++) {
      PetscInt nSubActBC = subMesh[nS]->getNActiveBC();
      
      // Number of dofs in nS
      nBC.push_back(nComps*nSubActBC);
      
      // Global dofs to pull BCs from
      idxBCFrom.emplace_back(std::vector<PetscInt>(nBC[nS],0));
      
      std::vector<PetscInt> meshBCFrom;
      subMesh[nS]->getBCOwn(meshBCFrom);
      for (PetscInt nB=0; nB<nSubActBC; nB++) {
        for (PetscInt nC=0; nC<nComps; nC++) {
          idxBCFrom[nS][nComps*nB+nC] = nComps*meshBCFrom[nB] + nC;
        }
      }
    }
    
    // set up Rhs communicator indices
    for (PetscInt nS=0; nS<nSubProb; nS++) {
      // Size of the right hand side for subproblem nS
      nRhs.push_back(nComps*(subMesh[nS]->getNActive() +
			     subMesh[nS]->getNActiveBC()));
      // Local indices to write into
      idxRhsTo.emplace_back(std::vector<PetscInt>(nRhs[nS],0));
      
      // Global indices to populate RHS [nS] from 
      idxRhsFrom.emplace_back(std::vector<PetscInt>(nRhs[nS],0));
      PetscInt subAct = subMesh[nS]->getNActive();
      
      // Disjoint nodes
      for (PetscInt nI=0; nI<nSet[nS]; nI++) {
        idxRhsTo[nS][nI] = nI;
        idxRhsFrom[nS][nI] = idxSolTo[nS][nI];
      }
      
      // Overlap nodes into
      for (PetscInt nI=nSet[nS]; nI<nComps*subAct; nI++) {
        idxRhsTo[nS][nI] = nI;
      }
      
      // Indices of overlap nodes in global numbering
      std::vector<PetscInt> meshRhsFrom;
      subMesh[nS]->getOverOwn(meshRhsFrom);
      // Modify for multicomponent problems
      for (unsigned int nR=0; nR<meshRhsFrom.size(); nR++) {
        for (PetscInt nC=0; nC<nComps; nC++) {
          PetscInt idx = nSet[nS] + nComps*nR + nC;
          idxRhsFrom[nS][idx] = nComps*meshRhsFrom[nR] + nC;
        }
      }
      
      // BC Nodes
      for (PetscInt nI=0; nI<nBC[nS]; nI++) {
        idxRhsTo[nS][nComps*subAct+nI] = nComps*subAct+nI;
        idxRhsFrom[nS][nComps*subAct+nI] = idxBCFrom[nS][nI];
      }
    }

    return ierr;
  }

  PetscErrorCode PC_ddcpm::rebuildSol(Vec xout,PetscInt col)
  {
    PetscErrorCode ierr = 0;
    
    for (PetscInt nS=0; nS<nSubProb; nS++) {
      if (subColors[nS] == col) {
        std::vector<PetscScalar> vals(nSet[nS],0);
        ierr = VecGetValues(subProb[nS]->sol,nSet[nS],
			    idxSolFrom[nS].data(),vals.data()); CHKERRQ(ierr);
        ierr = VecSetValues(xout,nSet[nS],idxSolTo[nS].data(),
			    vals.data(),INSERT_VALUES);  CHKERRQ(ierr);
      }
      ierr = VecAssemblyBegin(xout); CHKERRQ(ierr);
      ierr = VecAssemblyEnd(xout);   CHKERRQ(ierr);
    }
    
    return ierr;
  }

  PetscErrorCode PC_ddcpm::updateBCs(Vec xin,PetscInt col)
  {
    PetscErrorCode ierr = 0;
    
    for (PetscInt nS=0; nS<nSubProb; nS++) {
      PetscInt  isSize = col == subColors[nS] ? nRhs[nS] : 0;
      PetscInt *idxFrom = col == subColors[nS] ? idxRhsFrom[nS].data() : NULL;
      PetscInt *idxTo = col == subColors[nS] ? idxRhsTo[nS].data() : NULL;
      ierr = ISCreateGeneral(PETSC_COMM_WORLD,isSize,idxFrom,
			     PETSC_COPY_VALUES,&solFrom); CHKERRQ(ierr);
      ierr = ISCreateGeneral(PETSC_COMM_WORLD,isSize,idxTo,
			     PETSC_COPY_VALUES,&rhsTo); CHKERRQ(ierr);
      ierr = VecScatterCreate(xin,solFrom,
			      subProb[nS]->rhs,rhsTo,&bcs); CHKERRQ(ierr);
      ierr = VecScatterBegin(bcs,xin,subProb[nS]->rhs,
			     INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
      ierr = VecScatterEnd(bcs,xin,subProb[nS]->rhs,
			   INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
      ierr = ISDestroy(&solFrom); CHKERRQ(ierr);
      ierr = ISDestroy(&rhsTo); CHKERRQ(ierr);
      ierr = VecScatterDestroy(&bcs); CHKERRQ(ierr);
      
      // zero bcs,needed for the correction scheme
      if (col == subColors[nS]) {
        PetscInt offset = subMesh[nS]->getNActive();
        for (PetscInt nB=0; nB<nBC[nS]; nB++) {
          ierr = VecSetValue(subProb[nS]->rhs,globProb->m_nComps*offset + nB,
			     0.0,INSERT_VALUES); CHKERRQ(ierr);
        }
        ierr = VecAssemblyBegin(subProb[nS]->rhs); CHKERRQ(ierr);
        ierr = VecAssemblyEnd(subProb[nS]->rhs); CHKERRQ(ierr);
      }
    }
    
    return ierr;
  }

  PetscErrorCode PC_ddcpm::rasShell(Vec xin,Vec xout)
  {
    PetscErrorCode ierr = 0;
    
    // Set BCs on all owned subdomains doing any needed communication
    ierr = updateBCs(xin,0); CHKERRQ(ierr);

    // Solve all local problems
    for (PetscInt nS=0; nS<nSubProb; nS++) {
      ierr = KSPSolve(subProb[nS]->ksp,subProb[nS]->rhs,
		      subProb[nS]->sol); CHKERRQ(ierr);
    }
    
    // Build up total correction from all subdomains
    ierr = rebuildSol(xout,0); CHKERRQ(ierr);
  
    return ierr;
  }

  PetscErrorCode PC_ddcpm::rmsShell(Vec xin,Vec xout)
  {
    PetscErrorCode ierr = 0;
    
    // Set up intermediate vectors
    ierr = VecCopy(xin,colPartSol); CHKERRQ(ierr);
    ierr = VecSet(xout,0.0);          CHKERRQ(ierr);

    // Setup and solve local problems one color at a time
    for (PetscInt nC=0; nC<globMesh->getNColors(); nC++) {
      // Set BCs on all owned subdomains of color nC
      ierr = updateBCs(colPartSol,nC); CHKERRQ(ierr);

      // Solve local problems of color nC
      for (PetscInt nS=0; nS<nSubProb; nS++) {
        if (subColors[nS] == nC) {
          ierr = KSPSolve(subProb[nS]->ksp,subProb[nS]->rhs,
			  subProb[nS]->sol); CHKERRQ(ierr);
        }
      }
      
      // Build up correction from subdomains of this color
      ierr = VecSet(colCorr,0.0); CHKERRQ(ierr);
      ierr = rebuildSol(colCorr,nC); CHKERRQ(ierr);
      
      // Update xin, xout, and the residual
      ierr = MatMult(globProb->A,colCorr,colRes); CHKERRQ(ierr);
      ierr = VecAXPY(colPartSol,-1.0,colRes);     CHKERRQ(ierr);
      ierr = VecAXPY(xout,1.0,colCorr);           CHKERRQ(ierr);
    }
  
    return ierr;
  }

} // end namespace DDCPM
