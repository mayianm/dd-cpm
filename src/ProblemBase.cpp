// Problem Class Implementation
// Author: Ian May
// Purpose: The actual system to be solved is defined here and solved here.
//          In the single domain case this takes in (a pointer to) a CPMesh 
//          object and (a pointer to) a DiffEq object and builds the associated
//          operators. In multidomain problems this also takes in all of the 
//          subdomain meshes and subproblem definitions then builds the needed 
//          MPI communicators between them

#include "Problem.H"

namespace DDCPM {

  ProblemBase::ProblemBase(ProblemDefinition& a_probDef, CPMeshBase& a_mesh, DiffEq& a_eq):
    m_probDef(a_probDef),
    m_mesh(a_mesh),
    m_eq(a_eq),
    m_nComps(m_eq.getNComps()),
    m_shift(0.0),
    ksp(NULL),
    pc(NULL),
    A(NULL),
    sol(NULL),
    rhs(NULL)
  {}
  
  PetscErrorCode ProblemBase::unshift()
  {
    PetscErrorCode ierr = MatShift(A,-m_shift); CHKERRQ(ierr);
    return ierr;
  }

  PetscErrorCode ProblemBase::shift(PetscScalar s)
  {
    PetscErrorCode ierr = MatShift(A,s); CHKERRQ(ierr);
    m_shift = s;
    return ierr;
  }

} // Close namespace
