// CPMeshGlobal Class Implementation
// Author: Ian May 
/*! \file CPMesh.H
    \brief Provides a mesh data structure for Closest Point surface pde solvers
           using domain decomposition. This interfaces with METIS to perform
           partitions and build local meshes satisfying all overlap and stencil
           requirements.
*/

#include <array>
#include <cstddef>
#include <cstdint>
#include <functional>
#include <limits>
#include <unordered_map>
#include <utility>
#include <vector>

#include <petsc.h>

#include "ErrorTrace.H"
#include "CPMesh.H"

namespace DDCPM {

  CPMeshGlobal::CPMeshGlobal(ProblemDefinition& a_probDef,DiffEq& a_eq,
                             TVec<PetscReal>(*a_userCP)(const TVec<PetscReal>&),
                             PetscReal(*a_userDist)(const TVec<PetscReal>&),
                             TVec<PetscReal>(*a_userSN)(const TVec<PetscReal>&),
                             PetscReal(*a_userBCs)(const TVec<PetscReal>&)) :
    CPMeshBase(a_probDef,a_eq),
    m_nColors(1),
    m_nPollPts(a_probDef.mesh.nPoll),
    m_nActiveGlobal(0),
    m_nGhostGlobal(0),
    m_ghostLoIdx(0),
    m_ghostUpIdx(0),
    m_triCPTrimmed(false),
    m_bndBoxUp(a_probDef.mesh.dim),
    m_bndBoxLo(a_probDef.mesh.dim),
    m_bndTgtCUp(a_probDef.mesh.dim),
    m_bndTgtCLo(a_probDef.mesh.dim),
    m_bndTgtXUp(a_probDef.mesh.dim),
    m_bndTgtXLo(a_probDef.mesh.dim),
    m_nActPart(a_probDef.mesh.nParts),
    m_triMap(a_probDef.mesh.dim,false),
    m_partColors(a_probDef.mesh.nParts,0)
  {
    PetscLogEvent GLOBAL_MESHING;
    PetscLogEventRegister("Global Meshing",0,&GLOBAL_MESHING);
    PetscLogEventBegin(GLOBAL_MESHING,0,0,0,0);
    
    if (m_probDef.verbosity>0) {
      PPW_VZERO("\n\nConstruct global mesh\n");
    }
    
    MPI_Comm_rank(PETSC_COMM_WORLD,&m_rank);
    MPI_Comm_size(PETSC_COMM_WORLD,&m_size);
    
    try {
      // Set the closest point and related functions
      setClosestPoint(a_userCP);
      setSignedDist(a_userDist);
      setSurfaceNormal(a_userSN);
      if (a_userBCs != NULL) {
        globalBCWeight = a_userBCs;
      } else {
        globalBCWeight = [](const TVec<PetscReal>&)->PetscReal{ return 1.0; };
      }
      
      // Form the distance and CP functions for triangulations if needed
      if (m_probDef.mesh.surfType == Surfs::Triangulated) {
        parseTriangulation();
      } else { // Default center
        for (PetscInt nC=0; nC<m_probDef.mesh.dim; nC++) {
          m_center[nC] =  m_probDef.mesh.bbSize[nC]/2;
        }
      }
      
      // Populate active node lists
      findActiveSliced();
      
      if (m_probDef.mesh.nParts > 1) {
        partition();
        if (m_probDef.mesh.alignBounds) {
          alignDisjoint(m_probDef.mesh.ord + 1);
        }
      }
      
      // Layer the locally stored ghost nodes
      findGhostNodes();
      
      // Populate polling nodes if needed
      if (m_probDef.pp.ioLevel == IOLevel::All ||
	 m_probDef.pp.ioLevel == IOLevel::Poll) {
        findPollPoints();
      }
      
      if (m_probDef.verbosity > 1) {
        PPW_VONE("Global mesh has %d active nodes, %d ghost nodes, and %d polling nodes\n",m_nActiveGlobal,m_nGhostGlobal,m_nPollPts);
      }
      
      if (m_probDef.verbosity > 2) {
        PPS_VTWO("Rank %d: m_actLoIdx=%d m_actUpIdx=%d m_ghostLoIdx=%d m_ghostUpIdx=%d Tree sizes: %d, %d\n",
		 m_rank,m_actLoIdx,m_actUpIdx,m_ghostLoIdx,m_ghostUpIdx,
		 m_active.size(),m_ghost.size());
      }
      
      // Find which partitions live on which ranks
      formPartsOfRank();
      
    } catch(ErrorTrace& e) {
      PUSHERROR(e,"CPMeshGlobal Construction failed");
      throw e;
    }

    PetscLogEventEnd(GLOBAL_MESHING,0,0,0,0);
  }

// --------- Geometry ---------
  void CPMeshGlobal::setSignedDist(PetscReal(*userDist)(const TVec<PetscReal>&))
  {
    if (m_probDef.mesh.surfType == Surfs::Circle) {
      signedDist = circleDist;
    } else if (m_probDef.mesh.surfType == Surfs::Sphere) {
      signedDist = sphereDist;
    } else if (m_probDef.mesh.surfType == Surfs::Line) {
      signedDist = lineDist;
    } else if (m_probDef.mesh.surfType == Surfs::Torus) {
      signedDist = torusDist;
    } else if (m_probDef.mesh.surfType == Surfs::Plane) {
      signedDist = planeDist;
    } else if (m_probDef.mesh.surfType == Surfs::Disc) {
      signedDist = discDist;
    } else if (m_probDef.mesh.surfType == Surfs::Arc) {
      signedDist =
	std::function<PetscReal(const TVec<PetscReal>&)>(std::bind(derivedDist,this,std::placeholders::_1));
    } else if (m_probDef.mesh.surfType == Surfs::Triangulated) {
      signedDist =
	std::function<PetscReal(const TVec<PetscReal>&)>(std::bind(triangulatedDist,this,std::placeholders::_1));
    } else if (m_probDef.mesh.surfType == Surfs::User) {
      signedDist = userDist == NULL ?
	std::function<PetscReal(const TVec<PetscReal>&)>(std::bind(derivedDist,this,std::placeholders::_1)) : userDist;
    } else {
      ERRORTRACE("Surface type did not match any that is predefined\n");
    }
  }

  PetscReal CPMeshGlobal::circleDist(const TVec<PetscReal>& x)
  {
    if (x.getDim() == 2) {
      return x.norm() - 1.0;
    }
    
    TVec<PetscReal> y(x.getDim());
    y[0] = x[0];
    y[1] = x[1];
    if (y.norm() > 0) {
      y.normalize();
      TVec<PetscReal> dn = x - y;
      return dn.norm();
    }
    
    return std::numeric_limits<PetscReal>::max();
  }

  PetscReal CPMeshGlobal::sphereDist(const TVec<PetscReal>& x)
  {
    return x.norm() - 1.0;
  }

  PetscReal CPMeshGlobal::torusDist(const TVec<PetscReal>& x)
  {
    PetscReal R = 0.67;
    PetscReal r = 0.33;
    PetscReal rxy = PetscSqrtReal(x[0]*x[0] + x[1]*x[1]);
    return PetscSqrtReal(x[2]*x[2] + PetscPowReal(R - rxy,2.0)) - r;
  }

  PetscReal CPMeshGlobal::discDist(const TVec<PetscReal>& x)
  {
    if (x.norm() > 1.0) {
      return x.norm() - 1.0;
    }
    
    return 0.0;
  }

  PetscReal CPMeshGlobal::planeDist(const TVec<PetscReal>& x)
  {
    if (x[0] > 1.0) { // X Right
      if (x[1] > 1.0) { // Y above
        return PetscSqrtReal(x[2]*x[2] +
			     (x[0] - 1.0)*(x[0] - 1.0) + (x[1] - 1.0)*(x[1] - 1.0));
      } else if (x[1] < -1.0) { // Y below
        return PetscSqrtReal(x[2]*x[2] +
			     (x[0] - 1.0)*(x[0] - 1.0)+(x[1] + 1.0)*(x[1] + 1.0));
      } else { // Y interior
        return PetscSqrtReal(x[2]*x[2] + (x[0] - 1.0)*(x[0] - 1.0));
      }
    } else if (x[0] < -1.0) { // X Left
      if (x[1] > 1.0) { // Y above
        return PetscSqrtReal(x[2]*x[2] +
			     (x[0] + 1.0)*(x[0] + 1.0) + (x[1] - 1.0)*(x[1] - 1.0));
      } else if (x[1] < -1.0) { // Y below
        return PetscSqrtReal(x[2]*x[2] +
			     (x[0] + 1.0)*(x[0] + 1.0)+(x[1] + 1.0)*(x[1] + 1.0));
      } else { // Y interior
        return PetscSqrtReal(x[2]*x[2] + (x[0] + 1.0)*(x[0] + 1.0));
      }
    } else { // X interior
      if (x[1] > 1.0) { // Y above
        return PetscSqrtReal(x[2]*x[2] + (x[1] - 1.0)*(x[1] - 1.0));
      } else if (x[1] < -1.0) { // Y below
        return PetscSqrtReal(x[2]*x[2] + (x[1] + 1.0)*(x[1] + 1.0));
      } else { // Y interior
        return PetscAbsReal(x[2]);
      }
    }
  }

  PetscReal CPMeshGlobal::lineDist(const TVec<PetscReal>& x)
  {
    if (x[0] > 1.0) { // X Right
      return PetscSqrtReal(x[1]*x[1] + (x[0] - 1.0)*(x[0] - 1.0));
    } else if (x[0] < -1.0) { // X Left
      return PetscSqrtReal(x[1]*x[1] + (x[0]+1.0)*(x[0] + 1.0));
    } else { // X interior
      return PetscAbsReal(x[1]);
    }
  }

  PetscReal CPMeshGlobal::triangulatedDist(CPMeshGlobal* mesh,
					   const TVec<PetscReal>& x)
  {
    PetscLogEvent TRI_DIST;
    PetscLogEventRegister("Tri. Distance",0,&TRI_DIST);
    PetscLogEventBegin(TRI_DIST,0,0,0,0);
    
    TVec<uint32_t> lat = mesh->realToIntPos(x);
    if (mesh->m_triCPTrimmed) {
      PetscInt idx = mesh->getIDX(lat);
      if (idx >= 0 && idx < mesh->m_nActiveGlobal) {
        idx = mesh->m_permToLex[idx];
      }
      return mesh->m_triangleDist[idx];
    } else {
      PetscInt idx = mesh->m_triMap.getDLoc(lat);
      if (idx >= 0) {
        return mesh->m_triangleDist[idx];
      }
    }
    
    return std::numeric_limits<PetscReal>::max();
  
    PetscLogEventEnd(TRI_DIST,0,0,0,0);
  }

  PetscReal CPMeshGlobal::derivedDist(CPMeshGlobal* mesh,const TVec<PetscReal>& x)
  {
    TVec<PetscReal> sep = x - mesh->closestPoint(x);
    return sep.norm();
  }

  void CPMeshGlobal::setClosestPoint(TVec<PetscReal>(*userCP)(const TVec<PetscReal>&))
  {
    if (m_probDef.mesh.surfType == Surfs::Circle) {
      closestPoint = circleCP;
    } else if (m_probDef.mesh.surfType == Surfs::Sphere) {
      closestPoint = sphereCP;
    } else if (m_probDef.mesh.surfType == Surfs::Arc) {
      closestPoint = arcCP;
    } else if (m_probDef.mesh.surfType == Surfs::Line) {
      closestPoint = lineCP;
    } else if (m_probDef.mesh.surfType == Surfs::Torus) {
      closestPoint = torusCP;
    } else if (m_probDef.mesh.surfType == Surfs::Plane) {
      closestPoint = planeCP;
    } else if (m_probDef.mesh.surfType == Surfs::Disc) {
      closestPoint = discCP;
    } else if (m_probDef.mesh.surfType == Surfs::Triangulated) {
      closestPoint =
	std::function<TVec<PetscReal>(const TVec<PetscReal>&)>(std::bind(triangulatedCP,this,std::placeholders::_1));
    } else if (m_probDef.mesh.surfType == Surfs::User) {
      closestPoint = userCP;
      if (closestPoint == NULL) {
        ERRORTRACE("Must supply user defined CP function for surface type User");
      }
    } else {
      ERRORTRACE("Surface type did not match any that is predefined.\n");
    }
  }

  TVec<PetscReal> CPMeshGlobal::sphereCP(const TVec<PetscReal>& x)
  {
    TVec<PetscReal> cp(x);
    cp.normalize();
    return cp;
  }

  TVec<PetscReal> CPMeshGlobal::circleCP(const TVec<PetscReal>& x)
  {
    if (x.getDim() == 2) {
      TVec<PetscReal> cp(x);
      cp.normalize();
      return cp;
    }
    
    TVec<PetscReal> cp(x.getDim());
    cp[0] = x[0];
    cp[1] = x[1];
    cp.normalize();
    return cp;
  }

  TVec<PetscReal> CPMeshGlobal::discCP(const TVec<PetscReal>& x)
  {
    if (x.norm() < 1.0) {
      return x;
    }
    
    TVec<PetscReal> cp(x);
    cp.normalize();
    return cp;
  }

  TVec<PetscReal> CPMeshGlobal::arcCP(const TVec<PetscReal>& x)
  {
    PetscReal th = PetscAtan2Real(x[1],x[0]);
    if (th > 0. && th < 2.0) {
      TVec<PetscReal> cp(x);
      cp.normalize();
      return cp;
    } else if (th > 2.0) {
      return TVec<PetscReal>(std::array<PetscReal,2>{
	  PetscCosReal(2.0),
	  PetscSinReal(2.0)
	});
    }
    return TVec<PetscReal>(std::array<PetscReal,2>{1.0,0.0});
  }

  TVec<PetscReal> CPMeshGlobal::torusCP(TVec<PetscReal> x)
  {
    PetscReal dist = torusDist(x);
    TVec<PetscReal> normal(torusSNorm(x));
    x.addScaled(normal,-dist);
    return x;
  }

  TVec<PetscReal> CPMeshGlobal::planeCP(const TVec<PetscReal>& x)
  {
    if (x[0] > 1.0) {// X Right
      if (x[1] > 1.0) { // Y above
        return TVec<PetscReal>(std::array<PetscReal,3>{1.0,1.0,0.0});
      } else if (x[1] < -1.0) { // x[1] below
        return TVec<PetscReal>(std::array<PetscReal,3>{1.0,-1.0,0.0});
      } else { // Y interior
        return TVec<PetscReal>(std::array<PetscReal,3>{1.0,x[1],0.0});
      }
    } else if (x[0] < -1.0) { // X Left
      if (x[1] > 1.0) { // Y above
        return TVec<PetscReal>(std::array<PetscReal,3>{-1.0,1.0,0.0});
      } else if (x[1] < -1.0) { // Y below
        return TVec<PetscReal>(std::array<PetscReal,3>{-1.0,-1.0,0.0});
      } else { // Y interior
        return TVec<PetscReal>(std::array<PetscReal,3>{-1.0,x[1],0.0});
      }
    } else { // X interior
      if (x[1] > 1.0) { // Y above
        return TVec<PetscReal>(std::array<PetscReal,3>{x[0],1.0,0.0});
      } else if (x[1] < -1.0) { // Y below
        return TVec<PetscReal>(std::array<PetscReal,3>{x[0],-1.0,0.0});
      } else { // Y interior
        return TVec<PetscReal>(std::array<PetscReal,3>{x[0],x[1],0.0});
      }
    }
  }

  TVec<PetscReal> CPMeshGlobal::lineCP(const TVec<PetscReal>& x)
  {
    if (x[0] > 1.0) { // X Right
      return TVec<PetscReal>(std::array<PetscReal,2>{2.0,0.0});
    } else if (x[0] < -1.0) { // X Left
      return TVec<PetscReal>(std::array<PetscReal,2>{-2.0,0.0});
    } else { // X interior
      return TVec<PetscReal>(std::array<PetscReal,2>{x[0],0.0});
    }
  }

  TVec<PetscReal> CPMeshGlobal::triangulatedCP(CPMeshGlobal* mesh,
					       const TVec<PetscReal>& x)
  {
    PetscLogEvent TRI_CP;
    PetscLogEventRegister("Tri. CP",0,&TRI_CP);
    PetscLogEventBegin(TRI_CP,0,0,0,0);

    PetscInt idx;
    TVec<uint32_t> lat(mesh->realToIntPos(x));
    if (mesh->m_triCPTrimmed) {
      idx = mesh->getIDX(lat);
      if (idx >= 0 && idx < mesh->m_nActiveGlobal) {
        idx = mesh->m_permToLex[idx];
      }
    } else {
      idx = mesh->m_triMap.getDLoc(lat);
    }
    
    if (idx >= 0) {
      return mesh->m_triangleCP[idx];
    }
    
    PP_ERR("Tri CP lookup failed for lattice position: ");
    lat.print();
    PP_ERR("\n");
    ERRORTRACE("Tri CP lookup failed");
    return TVec<PetscReal>(3,-std::numeric_limits<PetscReal>::max());

    PetscLogEventEnd(TRI_CP,0,0,0,0);
  }

  void CPMeshGlobal::setSurfaceNormal(TVec<PetscReal>(*userSN)(const TVec<PetscReal>&))
  {
    if (m_probDef.mesh.surfType == Surfs::Circle) {
      surfaceNormal = circleSNorm;
    } else if (m_probDef.mesh.surfType == Surfs::Sphere) {
      surfaceNormal = sphereSNorm;
    } else if (m_probDef.mesh.surfType == Surfs::Arc) {
      surfaceNormal = arcSNorm;
    } else if (m_probDef.mesh.surfType == Surfs::Line) {
      surfaceNormal = lineSNorm;
    } else if (m_probDef.mesh.surfType == Surfs::Torus) {
      surfaceNormal = torusSNorm;
    } else if (m_probDef.mesh.surfType == Surfs::Plane) {
      surfaceNormal = planeSNorm;
    } else if (m_probDef.mesh.surfType == Surfs::Disc) {
      surfaceNormal = discSNorm;
    } else if (m_probDef.mesh.surfType == Surfs::Triangulated) {
      surfaceNormal =
	std::function<TVec<PetscReal>(const TVec<PetscReal>&)>(std::bind(derivedSNorm,this,std::placeholders::_1));
    } else if (m_probDef.mesh.surfType == Surfs::User) {
      surfaceNormal = userSN == NULL ?
	std::function<TVec<PetscReal>(const TVec<PetscReal>&)>(std::bind(derivedSNorm,this,std::placeholders::_1)) : userSN;
    } else {
      ERRORTRACE("Surface type did not match any that is predefined\n");
    }
  }

  TVec<PetscReal> CPMeshGlobal::sphereSNorm(const TVec<PetscReal>& x)
  {
    TVec<PetscReal> normal(x);
    normal.normalize();
    return normal;
  }

  TVec<PetscReal> CPMeshGlobal::circleSNorm(const TVec<PetscReal>& x)
  {
    if (x.getDim() == 2) {
      TVec<PetscReal> normal(x);
      normal.normalize();
      return normal;
    }
    
    TVec<PetscReal> y(x.getDim());
    y[0] = x[0];
    y[1] = x[1];
    y.normalize();
    TVec<PetscReal> normal = x - y;
    
    if (normal.norm() > 0.0) {
      normal.normalize();
      return normal;
    }
    
    return y; // To handle points on the surface
  }

  TVec<PetscReal> CPMeshGlobal::discSNorm(const TVec<PetscReal>& x)
  {
    if (x.norm()>1.0) {
      TVec<PetscReal> normal(x);
      normal.normalize();
      return normal;
    }
    
    return TVec<PetscReal>(2);
  }

  TVec<PetscReal> CPMeshGlobal::arcSNorm(const TVec<PetscReal>& x)
  {
    TVec<PetscReal> normal(x);
    normal.subtract(arcCP(x));
    normal.normalize();
    return normal;
  }

  TVec<PetscReal> CPMeshGlobal::torusSNorm(const TVec<PetscReal>& x)
  {
    PetscReal R = 0.67,rsq = x[0]*x[0] + x[1]*x[1],r = PetscSqrtReal(rsq);
    TVec<PetscReal> normal(3);
    normal[0] = (-x[0]*(R - r))/PetscSqrtReal(rsq*(x[2]*x[2] + (R - r)*(R - r)));
    normal[1] = (-x[1]*(R - r))/PetscSqrtReal(rsq*(x[2]*x[2] + (R - r)*(R - r)));
    normal[2] = x[2]/PetscSqrtReal(x[2]*x[2] + (R - r)*(R - r));
    normal.normalize();
    return normal;
  }

  TVec<PetscReal> CPMeshGlobal::planeSNorm(const TVec<PetscReal>& x)
  {
    std::array<PetscReal,3> tmp = {0.0,0.0,x[2]};
    TVec<PetscReal> normal(tmp);
    if (x[0] > 1.0) { // X Right
      if (x[1] > 1.0) { // Y above
        normal[0] = x[0] - 1.0;
        normal[1] = x[1] - 1.0;
      } else if (x[1] < -1.0) { // Y below
        normal[0] = x[0] - 1.0;
        normal[1] = x[1] + 1.0;
      } else { // Y interior
        normal[0] = x[0] - 1.0;
      }
    } else if (x[0] < -1.0) { // X Left
      if (x[1] > 1.0) { // Y above
        normal[0] = x[0] + 1.0;
        normal[1] = x[1] - 1.0;
      } else if (x[1] < -1.0) { // Y below
        normal[0] = x[0] + 1.0;
        normal[1] = x[1] + 1.0;
      } else { // Y interior
        normal[0] = x[0] + 1.0;
      }
    } else { // X interior
      if (x[1] > 1.0) { // Y above
        normal[1] = x[1] - 1.0;
      } else if (x[1] < -1.0) { // Y below
        normal[1] = x[1] + 1.0;
      } else if (x[2] < 1.e-16 && x[2] > -1.e-16) { // Y interior and Z degenerate
        normal[2] = 1.0;
      }
    }
    normal.normalize();
    return normal;
  }

  TVec<PetscReal> CPMeshGlobal::lineSNorm(const TVec<PetscReal>& x)
  {
    TVec<PetscReal> normal(0.0,1.0);
    if (x[0] > 1.0) { // X Right
      normal[0] = x[0] - 1.0;
    } else if (x[0] < -1.0) { // X Left
      normal[0] = x[0] + 1.0;
    }
    normal.normalize();
    return normal;
  }

  TVec<PetscReal> CPMeshGlobal::derivedSNorm(CPMeshGlobal* mesh,
					     const TVec<PetscReal>& x)
  {
    TVec<PetscReal> normal = x - mesh->closestPoint(x);
    normal.normalize();
    return normal;
  }

  void CPMeshGlobal::findPollPoints()
  {
    if (m_probDef.verbosity>1) {
      PPW_VONE("Find polling surface points and connectivity\n");
    }
    
    if (m_probDef.mesh.surfType == Surfs::Circle) {
      circlePollPoints();
    } else if (m_probDef.mesh.surfType == Surfs::Arc) {
      arcPollPoints();
    } else if (m_probDef.mesh.surfType == Surfs::Line) {
      linePollPoints();
    } else if (m_probDef.mesh.surfType == Surfs::Sphere) {
      spherePollPoints();
    } else if (m_probDef.mesh.surfType == Surfs::Torus) {
      torusPollPoints();
    } else if (m_probDef.mesh.surfType == Surfs::Plane) {
      planePollPoints();
    } else if (m_probDef.mesh.surfType == Surfs::Disc) {
      discPollPoints();
    } else if (m_probDef.mesh.surfType!=Surfs::Triangulated) {
      ERRORTRACE("No polling surface found");
    }
  }

  void CPMeshGlobal::circlePollPoints()
  {
    PetscReal dTheta = 2.0*g_pi/((PetscReal)m_nPollPts);
    for (PetscInt nP=0; nP<m_nPollPts; nP++) {
      PetscReal theta = nP*dTheta;
      if (m_probDef.mesh.dim == 2) {
        m_pollPos.emplace_back(std::array<PetscReal,2>{
	    PetscCosReal(theta),
	    PetscSinReal(theta)
	  });
      } else if (m_probDef.mesh.dim == 3) {
        m_pollPos.emplace_back(std::array<PetscReal,3>{
	    PetscCosReal(theta),
	    PetscSinReal(theta),
	    0
	  });
      }
      m_pollFaces.emplace_back(std::array<PetscInt,2>{nP,(nP + 1)%m_nPollPts});
    }
  }

  void CPMeshGlobal::discPollPoints()
  {
    PetscInt nMinor = m_nPollPts/2;
    PetscReal dTheta = 2.0*g_pi/m_nPollPts;
    PetscReal dR = 1.0/(nMinor - 1);
    
    // Origin and tris around it
    m_pollPos.emplace_back(std::array<PetscReal,2>{0.0,0.0});
    PetscReal theta = 0.0;
    for (PetscInt nT=0; nT<m_nPollPts; nT++) {
      theta = nT*dTheta;
      m_pollPos.emplace_back(std::array<PetscReal,2>{
	  dR*PetscCosReal(theta),
	  dR*PetscSinReal(theta)
	});
      PetscInt last = nT+2 <= m_nPollPts ? nT + 2 : 1;
      m_pollFaces.emplace_back(std::array<PetscInt,3>{0,nT + 1,last});
    }
    
    for (PetscInt nR=2; nR<nMinor; nR++) {
      PetscReal R = ((PetscReal)nR)*dR;
      for (PetscInt nT=1; nT<m_nPollPts; nT++) {
        theta = ((PetscReal)nT)*dTheta;
        m_pollPos.emplace_back(std::array<PetscReal,2>{
	    R*PetscCosReal(theta),
	    R*PetscSinReal(theta)
	  });
        m_pollFaces.emplace_back(std::array<PetscInt,3>{
	    (nR - 2)*m_nPollPts + nT,
	    (nR - 1)*m_nPollPts + nT,
	    (nR - 1)*m_nPollPts + nT + 1
	  });
        m_pollFaces.emplace_back(std::array<PetscInt,3>{
	    (nR - 2)*m_nPollPts + nT,
	    (nR - 2)*m_nPollPts + nT + 1,
	    (nR - 1)*m_nPollPts + nT + 1
	  });
      }
      m_pollPos.emplace_back(std::array<PetscReal,2>{
	  R*PetscCosReal(theta + dTheta),
	  R*PetscSinReal(theta + dTheta)
	});
      m_pollFaces.emplace_back(std::array<PetscInt,3>{
	  (nR - 1)*m_nPollPts,
	  nR*m_nPollPts,
	  (nR - 1)*m_nPollPts + 1
	});
      m_pollFaces.emplace_back(std::array<PetscInt,3>{
	  (nR - 1)*m_nPollPts,
	  (nR - 2)*m_nPollPts + 1,
	  (nR - 1)*m_nPollPts + 1
	});
    }
    
    m_nPollPts = m_pollPos.size();
  }

  void CPMeshGlobal::arcPollPoints()
  {
    PetscReal dTheta = 2.0/m_nPollPts;
    for (PetscInt nP=0; nP<m_nPollPts - 1; nP++) {
      PetscReal theta = nP*dTheta;
      m_pollPos.emplace_back(std::array<PetscReal,2>{cos(theta),
						     sin(theta)});
      m_pollFaces.emplace_back(std::array<PetscInt,2>{nP,nP + 1});
    }
    
    m_pollPos.emplace_back(std::array<PetscReal,2>{cos(2.0),sin(2.0)});
  }

  void CPMeshGlobal::linePollPoints()
  {
    PetscReal dx = 2.0/(m_nPollPts - 1);
    for (PetscInt nP=0; nP<m_nPollPts - 1; nP++) {
      m_pollPos.emplace_back(std::array<PetscReal,2>{nP*dx - 1.0,0.0});
      m_pollFaces.emplace_back(std::array<PetscInt,2>{nP,nP + 1});
    }
    
    m_pollPos.emplace_back(std::array<PetscReal,2>{1.0,0.0});
  }

  void CPMeshGlobal::spherePollPoints()
  {
    PetscInt nMinor = m_nPollPts/2;
    PetscReal dTheta = 2.0*g_pi/m_nPollPts;
    PetscReal dPhi = 2.0*g_pi/nMinor;
    
    // North pole and tris around it
    m_pollPos.emplace_back(std::array<PetscReal,3>{0.0,0.0,1.0});
    for (PetscInt nT=0; nT<m_nPollPts; nT++) {
      m_pollFaces.emplace_back(std::array<PetscInt,3>{
	  0,
	  nT + 1,
	  (nT + 2)%(m_nPollPts + 1)
	});
    }
    
    for (PetscInt nP=1; nP<nMinor - 1; nP++) { // Avoid the poles
      PetscReal phi = ((PetscReal)nP)*dPhi;
      for (PetscInt nT=0; nT<m_nPollPts; nT++) {
        PetscReal theta = ((PetscReal)nT)*dTheta;
        m_pollPos.emplace_back(std::array<PetscReal,3>{
	    sin(phi)*cos(theta),
	    sin(phi)*sin(theta),
	    cos(phi)
	  });
        PetscInt off = (nT + 2)%(m_nPollPts + 1);
        m_pollFaces.emplace_back(std::array<PetscInt,3>{
	    (nP - 1)*m_nPollPts + nT + 1,
	    nP*m_nPollPts + nT + 1,
	    nP*m_nPollPts + off
	  });
        m_pollFaces.emplace_back(std::array<PetscInt,3>{
	    (nP - 1)*m_nPollPts + nT + 1,
	    nP*m_nPollPts + off,
	    (nP - 1)*m_nPollPts + off
	  });
      }
    }
    
    // Finish out the final latitude and add in the south pole
    PetscInt finLat = m_pollPos.size() - 1;
    PetscReal phi = ((PetscReal)finLat)*dPhi;
    PetscInt sPoleIdx = finLat + m_nPollPts + 1;
    for (PetscInt nT=0; nT<m_nPollPts; nT++) {
      PetscReal theta = ((PetscReal)nT)*dTheta;
      m_pollPos.emplace_back(std::array<PetscReal,3>{
	  sin(phi)*cos(theta),
	  sin(phi)*sin(theta),
	  cos(phi)
	});
      PetscInt off = (nT + 2)%(m_nPollPts + 1);
      m_pollFaces.emplace_back(std::array<PetscInt,3>{
	  finLat + nT + 1,
	  sPoleIdx,
	  finLat + off
	});
    }
    
    m_pollPos.emplace_back(std::array<PetscReal,3>{0.0,0.0,-1.0});
    m_nPollPts = m_pollPos.size();
  }

  void CPMeshGlobal::torusPollPoints()
  {
    PetscInt nMinor = m_nPollPts/2;
    PetscReal dTheta = 2.0*g_pi/((PetscReal)m_nPollPts);
    PetscReal dPhi = 2.0*g_pi/((PetscReal)nMinor);
    
    for (PetscInt nP=0; nP<nMinor; nP++) {
      PetscReal phi = ((PetscReal)nP)*dPhi;
      for (PetscInt nT=0; nT<m_nPollPts; nT++) {
        PetscReal theta = ((PetscReal)nT)*dTheta;
        PetscReal r = 0.67 + 0.33*cos(phi);
        m_pollPos.emplace_back(std::array<PetscReal,3>{
	    r*cos(theta),
	    r*sin(theta),
	    0.33*sin(phi)
	  });
        PetscInt off = (nT + 1)%m_nPollPts;
        PetscInt nMul = (nP + 1)%nMinor;
        m_pollFaces.emplace_back(std::array<PetscInt,3>{
	    nP*m_nPollPts + nT,
	    nMul*m_nPollPts + nT,
	    nMul*m_nPollPts + off
	  });
        m_pollFaces.emplace_back(std::array<PetscInt,3>{
	    nP*m_nPollPts + nT,
	    nMul*m_nPollPts + off,
	    nP*m_nPollPts + off
	  });
      }
    }
    
    m_nPollPts = m_pollPos.size();
  }

  void CPMeshGlobal::planePollPoints()
  {
    PetscReal dx = 2.0/((PetscReal) (m_nPollPts - 1));
    
    for (PetscInt nY=0; nY<m_nPollPts; nY++) {
      for (PetscInt nX=0; nX<m_nPollPts; nX++) {
        m_pollPos.emplace_back(std::array<PetscReal,3>{
	    -1.0 + ((PetscReal)nX)*dx,
	    -1.0 + ((PetscReal)nY)*dx,
	    0.0
	  });
        if (nX<m_nPollPts - 1 && nY<m_nPollPts - 1) {
          m_pollFaces.emplace_back(std::array<PetscInt,3>{
	      nY*m_nPollPts + nX,
	      nY*m_nPollPts + nX + 1,
	      (nY + 1)*m_nPollPts + nX + 1
	    });
          m_pollFaces.emplace_back(std::array<PetscInt,3>{
	      nY*m_nPollPts + nX,
	      (nY + 1)*m_nPollPts + nX + 1,
	      (nY + 1)*m_nPollPts + nX
	    });
        }
      }
    }
    
    m_nPollPts = m_pollPos.size();
  }

// --------- Utility ----------
// Active node construction
  void CPMeshGlobal::findSliceBounds()
  {
    // Determine the local slice of the bounding box
    PetscInt stride = m_probDef.mesh.bbSize[0]/m_size;
    PetscInt locBndBoxLow = m_rank*stride;
    PetscInt locBndBoxHigh = (m_rank == m_size - 1) ?
      m_probDef.mesh.bbSize[0] : (m_rank + 1)*stride - 1; // inclusive
    
    // Set the locally considered slice size
    m_bndBoxUp[0] = locBndBoxHigh;
    m_bndBoxLo[0] = locBndBoxLow;
    
    for (PetscInt nC=1; nC<m_probDef.mesh.dim; nC++) {
      m_bndBoxUp[nC] = m_probDef.mesh.bbSize[nC];
    }
  }

  void CPMeshGlobal::findActiveSliced()
  {
    PetscLogEvent FINDACTIVESLICED;
    PetscLogEventRegister("Find sliced",0,&FINDACTIVESLICED);
    PetscLogEventBegin(FINDACTIVESLICED,0,0,0,0);
    
    if (m_probDef.verbosity>1) {
      PPW_VONE("Populate active nodes within slices\n");
    }
    
    // Bound slice
    findSliceBounds();
    
    // Room for new triangulated cp/dist storage if needed
    std::vector<PetscReal> tempDist;
    std::vector<PetscReal> tempCP;
    
    // Track the actual bounding box used by the slice
    TVec<uint32_t> bUp = m_bndBoxLo;
    TVec<uint32_t> bLo = m_bndBoxUp;
    TVec<PetscReal> xUp(m_probDef.mesh.dim,-std::numeric_limits<PetscReal>::max());
    TVec<PetscReal> xLo(m_probDef.mesh.dim,std::numeric_limits<PetscReal>::max());
    
    // Run over slice, adding all within tube
    std::vector<uint64_t> tempActive;
    TVec<uint32_t> p(m_bndBoxLo);
    do {
      PetscReal dist = PetscAbsReal(signedDist(intToRealPos(p)));
      if (dist < m_gamma) {
        tempActive.push_back(QOTree::vecToMorton(p));
        bUp.ewMax(p);
        bLo.ewMin(p);
      }
    } while(p.incMod(m_bndBoxLo,m_bndBoxUp));
    
    // Gather process active node counts to offset locally stored indices
    // to global indices
    m_nActive = tempActive.size();
    std::vector<PetscInt> recA(m_size,0);
    std::vector<PetscInt> displs(m_size,0);
    MPI_Allgather(&m_nActive,1,MPI_INT,recA.data(),1,MPI_INT,PETSC_COMM_WORLD);
    for (PetscInt nR=1; nR<m_size; nR++) {
      displs[nR] = displs[nR - 1] + recA[nR - 1];
    }
    m_nActiveGlobal = displs[m_size - 1] + recA[m_size - 1];
    
    // Locally considered range
    m_actLoIdx = displs[m_rank];
    m_actUpIdx = m_actLoIdx + m_nActive;
    if (m_probDef.verbosity>3) {
      PPW_VTHREE("Gather sliced mesh to all ranks\n");
    }
    m_nActive = m_nActiveGlobal;
    std::vector<uint64_t> recvLat(m_nActiveGlobal,0);
    MPI_Allgatherv(tempActive.data(),tempActive.size(),MPI_UINT64_T,
		   recvLat.data(),recA.data(),displs.data(),
		   MPI_UINT64_T,PETSC_COMM_WORLD);

    // Build full tree with correctly labeled nodes
    m_permToLex.resize(m_nActiveGlobal,0);
    for (PetscInt nA=0; nA<m_nActiveGlobal; nA++) {
      m_active.addLeaf(recvLat[nA],nA);
      m_permToLex[nA] = nA;
    }
    
    PetscLogEventEnd(FINDACTIVESLICED,0,0,0,0);
  }

  void CPMeshGlobal::findGhostNodes()
  {
    PetscLogEvent FINDGHOST;
    PetscLogEventRegister("Find ghost",0,&FINDGHOST);
    PetscLogEventBegin(FINDGHOST,0,0,0,0);
    
    if (m_probDef.verbosity>1) {
      PPW_VONE("Populate ghost node list\n");
    }
    
    // Run over all locally stored active nodes
    for (auto it=m_active.begin(); it!=m_active.end(); it++) {
      std::vector<TVec<uint32_t> > stencil(getMissingStencil(it->first));
      for (unsigned int nS=0; nS<stencil.size(); nS++) {
        if (!m_active.hasLeaf(stencil[nS]) && !m_ghost.hasLeaf(stencil[nS])) {
          // insert with correct index
          m_ghost.addLeaf(stencil[nS],m_nActiveGlobal + m_ghost.size());
          // Include ghosts in the bounding box
          m_bndBoxLo.ewMin(stencil[nS]);
          m_bndBoxUp.ewMax(stencil[nS]);
          // Build closest point for ghost node if surface is triangulated
          if (m_probDef.mesh.surfType == Surfs::Triangulated) {
            // set as average of closest points of all Neumann neighbors
            m_triangleCP.push_back(TVec<PetscReal>(3,0.0));
            PetscReal scl = 0.0;
            TVec<PetscInt> nbrs = getNeumannNbrs(stencil[nS]);
            for (unsigned int nN=0; nN<nbrs.getDim(); nN++) {
              PetscInt idx = m_active.getDLoc(nbrs[nN]);
              if (idx >= 0) {
                m_triangleCP.back() += m_triangleCP[idx];
                scl += 1.0;
              }
              m_triangleCP.back().invScale(scl);
            }        
          }
        }
      }
    }
    m_nGhost = m_ghost.size();
    m_ghostLoIdx = m_nActiveGlobal;
    m_ghostUpIdx = m_ghostLoIdx + m_nGhost;
    
    if (m_size > 1) {
      PetscInt gstStride = m_nGhost/m_size;
      m_ghostLoIdx += m_rank*gstStride;
      m_ghostUpIdx = m_rank == m_size - 1 ?
	m_nActiveGlobal + m_nGhost : m_ghostLoIdx + gstStride;
    }
    
    // Form global count on final process and broadcast out
    if (m_probDef.verbosity>3) {
      PPW_VTHREE("Broadcast global ghost node count\n");
    }
    m_nGhostGlobal = m_ghostUpIdx - m_nActiveGlobal;
    MPI_Bcast(&m_nGhostGlobal,1,MPI_INT,m_size - 1,PETSC_COMM_WORLD);
    
    PetscLogEventEnd(FINDGHOST,0,0,0,0);
  }

  PetscInt CPMeshGlobal::getNPollPts() const
  {
    return m_nPollPts;
  }

  PetscInt CPMeshGlobal::getNPollFaces() const
  {
    return m_pollFaces.size();
  }

  PetscInt CPMeshGlobal::getPartition(PetscInt a_idx) const
  {
    return m_part[a_idx];
  }

  PetscInt CPMeshGlobal::getIDX(const TVec<uint32_t>& a_lat) const
  {
    PetscLogEvent GET_IDX;
    PetscLogEventRegister("Get idx",0,&GET_IDX);
    PetscLogEventBegin(GET_IDX,0,0,0,0);
  
    PetscInt aIdx = m_active.getDLoc(a_lat);
    if (aIdx>=0) {
      PetscLogEventEnd(GET_IDX,0,0,0,0);
      return aIdx;
    }
    
    PetscLogEventEnd(GET_IDX,0,0,0,0);
    return m_ghost.getDLoc(a_lat);
  }

  TVec<uint32_t> CPMeshGlobal::getCenter() const
  {
    return m_center;
  }

  TVec<PetscReal> CPMeshGlobal::getCP(PetscInt a_idx) const
  {
    return closestPoint(intToRealPos(getLattice(a_idx)));
  }

  TVec<PetscReal> CPMeshGlobal::getSN(PetscInt a_idx) const
  {
    return surfaceNormal(intToRealPos(getLattice(a_idx)));
  }

  void CPMeshGlobal::alignDisjoint(PetscInt a_nLaps)
  {
    PetscLogEvent ALIGNDISJOINT;
    PetscLogEventRegister("Align disjoint interfaces",0,&ALIGNDISJOINT);
    PetscLogEventBegin(ALIGNDISJOINT,0,0,0,0);
    
    if (m_probDef.verbosity>2) {
      PPW_VTWO("Align disjoint interfaces\n");
    }
    
    for (PetscInt nL=0; nL<a_nLaps; nL++) {
      for (auto it=m_active.begin(); it!=m_active.end(); it++) {
        TVec<uint32_t> lat = QOTree::mortonToVec(it->first,m_probDef.mesh.dim);
        TVec<uint32_t> cpLat = realToIntPos(closestPoint(intToRealPos(lat)));
        PetscInt tPart = m_part[m_active.getDLoc(cpLat)];
        if (m_part[it->second] != tPart) {
          // Only swap if node has at least one neighbor in the target partition
          TVec<PetscInt> nbrs = getNeumannNbrs(it->second);
          bool reassign = false;
          for (unsigned int nN=0; nN<nbrs.getDim(); nN++) {
            if (m_part[nbrs[nN]] == tPart) {
              reassign = true;
              break;
            }
          }
          if (reassign) {
	    m_part[it->second] = tPart;
	  }
        }
      }
    }
    
    PetscLogEventEnd(ALIGNDISJOINT,0,0,0,0);
  }

  PetscInt CPMeshGlobal::getNActiveBC() const
  {
    return 0;
  }

  TVec<uint32_t> CPMeshGlobal::getLattice(PetscInt a_idx) const
  {
    if (a_idx>=0) {
      if (a_idx<m_nActiveGlobal) {
        return m_active.getVecByVal(a_idx);
      } else {
        return m_ghost.getVecByVal(a_idx);
      }
    }
    
    PP_ERR("Global lattice lookup failed. Sought a_idx: %d on rank: %d\n",
	   a_idx,m_rank);
    ERRORTRACE("Global lattice lookup failed");
    
    return TVec<uint32_t>(m_probDef.mesh.dim);
  }

  void CPMeshGlobal::getActivePos(double* points) const
  {
    for (PetscInt nA=m_actLoIdx; nA<m_actUpIdx; nA++) {
      TVec<PetscReal> x(intToRealPos(getLattice(nA)));
      for (PetscInt nD=0; nD<m_probDef.mesh.dim; nD++) {
        points[m_probDef.mesh.dim*(nA - m_actLoIdx) + nD] = x[nD];
      }
    }
  }

  void CPMeshGlobal::getPollPos(double* points) const
  {
    for (PetscInt nP=0; nP<m_nPollPts; nP++) {
      for (PetscInt nD=0; nD<m_probDef.mesh.dim; nD++) {
        points[m_probDef.mesh.dim*nP + nD] = m_pollPos[nP][nD];
      }
    }
  }

  void CPMeshGlobal::getPollFaces(int* faces) const
  {
    for (unsigned int nF=0; nF<m_pollFaces.size(); nF++) {
      for (PetscInt nP=0; nP<3; nP++) {
        faces[3*nF + nP] = m_pollFaces[nF][nP];
      }
    }
  }

  void CPMeshGlobal::getSurfNorm(double* surfNorms) const
  {
    for (PetscInt nA=m_actLoIdx; nA<m_actUpIdx; nA++) {
      TVec<PetscReal> sn(surfaceNormal(intToRealPos(getLattice(nA))));
      for (PetscInt nD=0; nD<m_probDef.mesh.dim; nD++) {
        surfNorms[m_probDef.mesh.dim*(nA - m_actLoIdx) + nD] = sn[nD];
      }
    }
  }

  void CPMeshGlobal::getPollSurfNorm(double* surfNorms) const
  {
    for (PetscInt nP=0; nP<m_nPollPts; nP++) {
      TVec<PetscReal> sn = surfaceNormal(m_pollPos[nP]);
      for (PetscInt nD=0; nD<m_probDef.mesh.dim; nD++) {
        surfNorms[m_probDef.mesh.dim*nP + nD] = sn[nD];
      }
    }
  }

  void CPMeshGlobal::getDistance(double* dist) const
  {
    for (PetscInt nA=m_actLoIdx; nA<m_actUpIdx; nA++) {
      dist[nA - m_actLoIdx] = signedDist(intToRealPos(getLattice(nA)));
    }
  }

  void CPMeshGlobal::getPartition(PetscInt* parts) const
  {
    for (PetscInt nA=m_actLoIdx; nA<m_actUpIdx; nA++) {
      parts[nA - m_actLoIdx] = m_part[nA];
    }
  }

  void CPMeshGlobal::getNodeColoring(PetscInt* colors) const
  {
    for (PetscInt nA=m_actLoIdx; nA<m_actUpIdx; nA++) {
      colors[nA - m_actLoIdx] = m_partColors[m_part[nA]];
    }
  }

  TVec<PetscInt> CPMeshGlobal::getInterpNodes(PetscInt a_idx)
  {
    PetscLogEvent GET_INODES;
    PetscLogEventRegister("Get int nodes",0,&GET_INODES);
    PetscLogEventBegin(GET_INODES,0,0,0,0);

    auto it = m_cpInterpNodes.find(a_idx);
    if (it == m_cpInterpNodes.end()) {
      try {
        findInterpInfo(a_idx);
      } catch(ErrorTrace& e) {
        PUSHERROR(e,"Interpolation failed on index " + std::to_string(a_idx));
        throw e;
      }
      PetscLogEventEnd(GET_INODES,0,0,0,0);
      return m_cpInterpNodes[a_idx];
    }
    
    PetscLogEventEnd(GET_INODES,0,0,0,0);
    return it->second;
  }

  TVec<PetscInt> CPMeshGlobal::getInterpNodes(const TVec<uint32_t>& a_lat)
  {
    try {
      return getInterpNodes(getIDX(a_lat));
    } catch(ErrorTrace& e) {
      PUSHERROR(e,"Interpolation failed");
      throw e;
    }
  }

  TVec<PetscReal> CPMeshGlobal::getInterpWeights(PetscInt a_idx)
  {
    auto it = m_cpInterpWeights.find(a_idx);
    
    if (it == m_cpInterpWeights.end()) {
      try {
        findInterpInfo(a_idx);
      } catch(ErrorTrace& e) {
        PUSHERROR(e,"Interpolation failed on index " + std::to_string(a_idx));
        throw e;
      }
      return m_cpInterpWeights[a_idx];
    }
    
    return it->second;
  }

  TVec<PetscReal> CPMeshGlobal::getInterpWeights(const TVec<uint32_t>& a_lat)
  {
    try {
      return getInterpWeights(getIDX(a_lat));
    } catch(ErrorTrace& e) {
      PUSHERROR(e,"Interpolation failed");
      throw e;
    }
  }

  TVec<PetscInt> CPMeshGlobal::getNeumannNbrs(const TVec<uint32_t>& a_lat)
  {
    TVec<PetscInt> nbrs(2*m_probDef.mesh.dim);
    
    for (PetscInt nC=0; nC<m_probDef.mesh.dim; nC++) {
      TVec<uint32_t> tmp(a_lat);
      tmp[nC] = a_lat[nC] - 1;
      nbrs[2*nC] = getIDX(tmp);
      tmp[nC] = a_lat[nC] + 1;
      nbrs[2*nC + 1] = getIDX(tmp);
    }
    
    return nbrs;
  }

  TVec<PetscInt> CPMeshGlobal::getNeumannNbrs(PetscInt a_idx)
  {
    return getNeumannNbrs(getLattice(a_idx));
  }

  TVec<PetscInt> CPMeshGlobal::getVoronoiNbrs(const TVec<uint32_t>& a_lat)
  {
    TVec<PetscInt> nbrs;
    TVec<uint32_t> low(a_lat);
    TVec<uint32_t> up(a_lat);
    
    for (PetscInt nD=0; nD<m_probDef.mesh.dim; nD++) {
      if (low[nD]>0) {
	low[nD] = low[nD] - 1;
      }
      if (up[nD]<std::numeric_limits<uint32_t>::max()) {
        up[nD] = up[nD] + 1;
      }
    }
    
    TVec<uint32_t> p = low;
    do {
      if (m_active.hasLeaf(p)) {
        if (p != a_lat) {
	  nbrs.append(getIDX(p));
	}
      }
    } while(p.incMod(low,up));
  
    return nbrs;
  }

  TVec<PetscInt> CPMeshGlobal::getVoronoiNbrs(PetscInt a_idx)
  {
    return getVoronoiNbrs(getLattice(a_idx));
  }

  void CPMeshGlobal::formPartsOfRank()
  {
    PetscInt base = m_probDef.mesh.nParts/m_size;
    
    for (PetscInt nR=0; nR<m_size - 1; nR++) {
      PofR.emplace_back(base);
      for (PetscInt nC=0; nC<base; nC++) {
	PofR[nR][nC] = nR*base + nC;
      }
    }
    
    PetscInt remain = m_probDef.mesh.nParts - (m_size - 1)*base;
    PofR.emplace_back(remain);
    
    for (PetscInt nC=0; nC<remain; nC++) {
      PofR[m_size - 1][nC] = (m_size - 1)*base + nC;
    }
  }

  TVec<PetscInt> CPMeshGlobal::getPartsOfRank(PetscInt a_rank) const
  {
    return PofR[a_rank];
  }

  void CPMeshGlobal::getPartExtents(PetscInt a_part,
				    PetscInt& actLoIdx,PetscInt& actUpIdx) const
  {
    if (a_part != 0) {
      actLoIdx = m_nActPart[a_part - 1];
      actUpIdx = m_nActPart[a_part];
    } else if (a_part == 0) {
      actLoIdx = 0;
      actUpIdx = m_nActPart[0];
    }
  }

  PetscInt CPMeshGlobal::getNColors() const
  {
    return m_nColors;
  }

  std::vector<PetscInt> CPMeshGlobal::getPartColors() const
  {
    return m_partColors;
  }

  std::vector<PetscInt> CPMeshGlobal::getPartColors(const TVec<PetscInt>& parts) const
  {
    std::vector<PetscInt> subPartColors;
    unsigned int nS=0;
    PetscInt nP=0;
    
    for (; nP<m_probDef.mesh.nParts && nS<parts.getDim(); nP++) {
      if (parts[nS] == nP) {
        subPartColors.push_back(m_partColors[nP]);
        nS++;
      }
    }
    
    return subPartColors;
  }

  void CPMeshGlobal::formNodeGraph(std::vector<PetscInt>& xadj,
				   std::vector<PetscInt>& adjncy)
  {
    PetscLogEvent FORMGRAPH;
    PetscLogEventRegister("Form Graph",0,&FORMGRAPH);
    PetscLogEventBegin(FORMGRAPH,0,0,0,0);
    
    if (m_probDef.verbosity > 2) {
      PPW_VTWO("Form graph representation of active nodes\n");
    }
    
    // Count number of edges (double count on purpose)
    PetscInt nEdges = 0;
    PetscInt actLocal = m_actUpIdx - m_actLoIdx;
    std::vector<uint64_t> actTemp(actLocal,0);
    for (auto it=m_active.begin(); it!=m_active.end(); it++) {
      if (it->second >= m_actLoIdx && it->second < m_actUpIdx) {
	actTemp[it->second - m_actLoIdx] = it->first;
	TVec<uint32_t> actVec(QOTree::mortonToVec(it->first,m_probDef.mesh.dim));
	for (PetscInt nC=0; nC<m_probDef.mesh.dim; nC++) {
	  TVec<uint32_t> next(actVec);
	  TVec<uint32_t> last(actVec);
	  next[nC] = actVec[nC] + 1;
	  last[nC] = actVec[nC] - 1;
	  if (m_active.hasLeaf(next)) {
	    nEdges++;
	  }
	  if (m_active.hasLeaf(last)) {
	    nEdges++;
	  }
	}
      }
    }
    
    // Form graph version of mesh
    xadj.resize(actLocal + 1,-1);
    xadj[0] = 0;
    
    // Needs to be twice the number of edges,but nEdges already holds that
    adjncy.resize(nEdges,-1);
    PetscInt adjCtr = 0;
    for (PetscInt nA=0; nA<actLocal; nA++) {
      TVec<uint32_t> actVec(QOTree::mortonToVec(actTemp[nA],m_probDef.mesh.dim));
      TVec<PetscReal> x(intToRealPos(actVec));
      PetscInt degP = 0;
      for (PetscInt nC=0; nC<m_probDef.mesh.dim; nC++) {
        TVec<uint32_t> next(actVec);
        TVec<uint32_t> last(actVec);
        next[nC] = actVec[nC] + 1;
        last[nC] = actVec[nC] - 1;
        PetscInt nIdx = m_active.getDLoc(next);
        PetscInt lIdx = m_active.getDLoc(last);
        if (nIdx>=0) {
          adjncy[adjCtr] = nIdx;
          degP++;
          adjCtr++;
        }
        if (lIdx>=0) {
          adjncy[adjCtr] = lIdx;
          degP++;
          adjCtr++;
        }
      }
      xadj[nA + 1] = degP + xadj[nA];
    }
    
    PetscLogEventEnd(FORMGRAPH,0,0,0,0);
  }

  PetscErrorCode CPMeshGlobal::partition()
  {
    PetscLogEvent PARTITION;
    PetscLogEventRegister("Partition Mesh",0,&PARTITION);
    PetscLogEventBegin(PARTITION,0,0,0,0);
    
    if (m_probDef.verbosity > 1) {
      PPW_VONE("Partition mesh\n");
    }

    // Create and call the petsc partitioner
    PetscInt ierr;
    PetscPartitioner part;
    PetscSection partSection;
    IS partition;
    const PetscInt *partIdxs;
    PetscInt nActLocal = m_actUpIdx - m_actLoIdx,off_gbl = 0;
    std::vector<PetscInt> xadj,adjncy;
    std::vector<PetscInt> globalLabels(m_nActiveGlobal,-1),localLabels(nActLocal,-1);
    
    ierr = PetscPartitionerCreate(PETSC_COMM_WORLD,&part); CHKERRQ(ierr);
    ierr = PetscPartitionerSetFromOptions(part);
    ierr = PetscPartitionerSetUp(part);
    
    ierr = PetscSectionCreate(PETSC_COMM_WORLD,&partSection); CHKERRQ(ierr);

    formNodeGraph(xadj,adjncy);
    
    ierr = PetscPartitionerPartition(part,m_probDef.mesh.nParts,
				     m_actUpIdx - m_actLoIdx,xadj.data(),
				     adjncy.data(),NULL,NULL,
				     partSection,&partition); CHKERRQ(ierr);

    // Extract indices and offset as needed
    ierr = ISGetIndices(partition,&partIdxs); CHKERRQ(ierr);
    for (PetscInt nA=0; nA<nActLocal; nA++) {
      localLabels[nA] = partIdxs[nA] + m_actLoIdx;
    }
    ISRestoreIndices(partition,&partIdxs);
    ISDestroy(&partition);

    // Gather labels to each process and permute
    m_part.resize(m_nActiveGlobal,-1);
    for (PetscInt nP=0; nP<m_probDef.mesh.nParts; nP++) {
      // Get offset and number of points locally in partition nP
      PetscInt pts_loc,off_loc,pts_gbl = 0;
      ierr = PetscSectionGetDof(partSection,nP,&pts_loc);    CHKERRQ(ierr);
      ierr = PetscSectionGetOffset(partSection,nP,&off_loc); CHKERRQ(ierr);
      
      // Gather number of points on each rank and convert to recv offsets
      std::vector<PetscInt> recA(m_size,0),displs(m_size,0);
      MPI_Allgather(&pts_loc,1,MPI_INT,recA.data(),1,MPI_INT,PETSC_COMM_WORLD);
      pts_gbl = recA[0];
      for (PetscInt nR=1; nR<m_size; nR++) {
        displs[nR] = displs[nR - 1] + recA[nR - 1];
	pts_gbl += recA[nR];
      }

      // gather local partition labels together
      MPI_Allgatherv(localLabels.data() + off_loc,pts_loc,MPI_INT,
		     globalLabels.data() + off_gbl,recA.data(),displs.data(),
		     MPI_INT,PETSC_COMM_WORLD);
      
      // Fill in ordered partition labels and cumulative size of partitions
      for (PetscInt p=0; p<pts_gbl; p++) {
	m_part[off_gbl + p] = nP;
      }
      off_gbl += pts_gbl;
      m_nActPart[nP] = nP == 0 ? pts_gbl : pts_gbl + m_nActPart[nP - 1];
    }
    
    // Build index maps
    for (PetscInt nA=0; nA<m_nActiveGlobal; nA++) {
      m_permToLex[globalLabels[nA]] = nA;
    }
    
    std::unordered_map<uint64_t,PetscInt> actCopy;
    actCopy.swap(m_active.getMap());
    m_active.deleteTree();
    for (auto it=actCopy.begin(); it!=actCopy.end(); ++it) {
      m_active.addLeaf(it->first,m_permToLex[it->second]);
    }
    
    // Update rank local index bounds
    PetscInt fPart = m_rank*(m_probDef.mesh.nParts/m_size) - 1;
    PetscInt lPart = (m_rank + 1)*(m_probDef.mesh.nParts/m_size) - 1;
    m_actLoIdx = fPart>=0 ? m_nActPart[fPart] : 0;
    m_actUpIdx = m_nActPart[lPart];

    // Tidy up
    PetscSectionDestroy(&partSection);
    PetscPartitionerDestroy(&part);
    
    PetscLogEventEnd(PARTITION,0,0,0,0);

    return ierr;
  }

  // Subdomain coloring for multiplicative methods
  void CPMeshGlobal::applyColor(PetscInt vert,std::set<PetscInt>& adj,
                                std::pair<PetscInt,PetscInt>& color,
                                std::unordered_map<PetscInt,
				std::set<PetscInt> >& forbid)
  {
    m_partColors[vert] = color.second;
    color.first++; // New subdomain using this color
    // this color is forbidden for all neighbors of this subdomain
    for (const PetscInt& v : adj) {
      forbid[v].insert(color.second);
    }
  }

  void CPMeshGlobal::colorSubdomains()
  {
    PetscLogEvent COLORSUBDOMAINS;
    PetscLogEventRegister("Color the subdomains",0,&COLORSUBDOMAINS);
    PetscLogEventBegin(COLORSUBDOMAINS,0,0,0,0);
  
    if (m_probDef.verbosity>2) {
      PPW_VTWO("Color subdomains\n");
    }
    
    std::vector<std::set<PetscInt> > colAdjV(m_probDef.mesh.nParts,
					     std::set<PetscInt>());
    std::list<std::pair<std::set<PetscInt>,PetscInt> > colAdj;
    std::list<std::pair<PetscInt,PetscInt> > colCnts; // pair of <count,color>
    std::unordered_map<PetscInt,std::set<PetscInt> > forbid; // Forbidden colors
    
    // Build up adjacency sets
    for (auto it=m_active.begin(); it!=m_active.end(); it++) {
      PetscInt itP = m_part[it->second];
      const TVec<PetscInt>& nbrs = getVoronoiNbrs(it->second);
      for (unsigned int nC=0; nC<nbrs.getDim(); nC++) {
        PetscInt idx = nbrs[nC];
        if (m_part[idx] != itP) {colAdjV[itP].insert(m_part[idx]); }
      }
    }
    
    // Put into list of pairs for sorted (by num of nbrs descending) version
    for (PetscInt nP=0; nP<m_probDef.mesh.nParts; nP++) {
      colAdj.push_front(std::make_pair(colAdjV[nP],nP));
    }
    
    colAdj.sort([forbid](const std::pair<std::set<PetscInt>,
			 PetscInt>& lhs,
			 const std::pair<std::set<PetscInt>,PetscInt>& rhs) {
      if (lhs.first.size() == rhs.first.size()) {
	auto lIt = forbid.find(lhs.second);
	auto rIt = forbid.find(rhs.second);
	if (rIt == forbid.end()) { return true; }
	else if (lIt == forbid.end()) { return false; }
	else if (lIt->second.size()!=rIt->second.size()) {
	  return lIt->second.size()>rIt->second.size();
	}
	return lhs.second<rhs.second;
      }
      return lhs.first.size()>rhs.first.size();
    });
    
    // Color the graph
    colCnts.push_front(std::make_pair(0,0));
    for (auto lIt=colAdj.begin(); lIt!=colAdj.end(); lIt++) {
      colCnts.sort(); // Prioritize colors that have been used less
      auto cIt = colCnts.begin();
      auto fIt = forbid.find(lIt->second);
      if (fIt!=forbid.end()) { // This subdomain has forbidden colors
        while(forbid[lIt->second].count(cIt->second)>0) {
          cIt++;
          if (cIt == colCnts.end()) { break; }
        }
        if (cIt!=colCnts.end()) { // use color (*cIt)->second
          applyColor(lIt->second,lIt->first,*cIt,forbid);
        } else { // Need new color
          colCnts.push_front(std::pair<PetscInt,PetscInt>(0,m_nColors));
          m_nColors++;
          applyColor(lIt->second,lIt->first,colCnts.front(),forbid);
        }
      } else { // nothing forbidden
        applyColor(lIt->second,lIt->first,*cIt,forbid);
      }
    }
  
    PetscLogEventEnd(COLORSUBDOMAINS,0,0,0,0);
  }

  // By index
  void CPMeshGlobal::findInterpInfo(PetscInt a_idx)
  {
    PetscLogEvent INTERP_INFO;
    PetscLogEventRegister("Interp single",0,&INTERP_INFO);
    PetscLogEventBegin(INTERP_INFO,0,0,0,0);

    TVec<uint32_t> offset(m_probDef.mesh.dim,m_probDef.mesh.ord/2);
    TVec<uint32_t> modVec(m_probDef.mesh.dim,m_probDef.mesh.ord);
    
    // Pull in lattice vector and associated info
    TVec<uint32_t> p = getLattice(a_idx);
    TVec<PetscReal> x = closestPoint(intToRealPos(p));
    TVec<PetscInt> intNodes(m_nIntNodes);
    TVec<PetscReal> intWeights(m_nIntNodes);
    
    // index of mesh point closest to this points CP
    TVec<uint32_t> baseIntCP = realToIntPos(x);
    
    // Base point of hypercube
    baseIntCP.subtract(offset);
    TVec<uint32_t> loopVec(m_probDef.mesh.dim);
    
    // Get indices of all nodes in hypercube and set weights
    TVec<PetscReal> totWt(findTotalWeight(baseIntCP,x));
    std::vector<TVec<PetscReal> > pointWeights;
    for (PetscInt nD=0; nD<m_probDef.mesh.dim; nD++) {
      pointWeights.emplace_back(findPointWeights(baseIntCP,x,totWt,nD));
    }
    
    for (PetscInt nI=0; nI<m_nIntNodes; nI++) {
      TVec<uint32_t> intCP = baseIntCP + loopVec;
      intNodes[nI] = getIDX(intCP);
      PetscReal ptWt = 1.0;
      for (PetscInt nD=0; nD<m_probDef.mesh.dim; nD++) {
        ptWt *= pointWeights[nD][loopVec[nD]];
      }
      intWeights[nI] = ptWt;
      if (intNodes[nI]<0 || intNodes[nI]>=m_nActiveGlobal) {
        ERRORTRACE("Interpolation stencil exceeds active nodes");
      }
      loopVec.incMod(modVec);
    }
    
    // Apply final weight to enforce global BCs if needed
    intWeights.scale(globalBCWeight(intToRealPos(p)));
    m_cpInterpNodes.insert(std::make_pair(a_idx,intNodes));
    m_cpInterpWeights.insert(std::make_pair(a_idx,intWeights));
  
    PetscLogEventEnd(INTERP_INFO,0,0,0,0);
  }

  void CPMeshGlobal::findPollInterpInfo(PetscInt a_idx,TVec<PetscInt>& intNodes,
					TVec<PetscReal>& intWeights) const
  {
    PetscLogEvent POLL_INT;
    PetscLogEventRegister("Poll Interp",0,&POLL_INT);
    PetscLogEventBegin(POLL_INT,0,0,0,0);
  
    TVec<uint32_t> offset(m_probDef.mesh.dim,m_probDef.mesh.ord/2);
    TVec<uint32_t> modVec(m_probDef.mesh.dim,m_probDef.mesh.ord);
    
    // index of mesh point closest to this point
    TVec<uint32_t> baseIntCP = realToIntPos(m_pollPos[a_idx]);
    
    // Base point of hypercube
    baseIntCP.subtract(offset);
    TVec<uint32_t> loopVec(m_probDef.mesh.dim);
    
    // Get indices of all nodes in hypercube and set weights
    TVec<PetscReal> totWt(findTotalWeight(baseIntCP,m_pollPos[a_idx]));
    std::vector<TVec<PetscReal> > pointWeights;
    
    for (PetscInt nD=0; nD<m_probDef.mesh.dim; nD++) {
      pointWeights.emplace_back(findPointWeights(baseIntCP,m_pollPos[a_idx],
						 totWt,nD));
    }
    
    for (PetscInt nI=0; nI<m_nIntNodes; nI++) {
      TVec<uint32_t> intCP = baseIntCP + loopVec;
      intNodes[nI] = getIDX(intCP);
      if (intNodes[nI]<0 || intNodes[nI]>=m_nActiveGlobal) {
        ERRORTRACE("Interpolation stencil exceeds active nodes");
      }
      PetscReal ptWt = 1.0;
      for (PetscInt nD=0; nD<m_probDef.mesh.dim; nD++) {
        ptWt *= pointWeights[nD][loopVec[nD]];
      }
      intWeights[nI] = ptWt;
      loopVec.incMod(modVec);
    }
  
    PetscLogEventEnd(POLL_INT,0,0,0,0);
  }

// Routines for dealing with triangulations
// -------------  m_probDef.mesh.dim == 3 ONLY ---------------------
// Roughly adapted from Colin MacDonald and Steve Ruuth's methods, see:
// tri2cp_helper.c at https://github.com/cbm755/cp_matrices
// Loop over all triangles and populate the distance and CP functions
  void CPMeshGlobal::parseTriangulation()
  {
    PetscLogEvent PARSE_TRI;
    PetscLogEventRegister("Parse tris serial",0,&PARSE_TRI);
    PetscLogEventBegin(PARSE_TRI,0,0,0,0);
  
    if (m_probDef.verbosity>2) {
      PPW_VTWO("Parsing triangulation\n");
    }
    
    try {
      // Read Ply file and form vertex list
      PetscInt nVertices;
      PetscInt nFaces;
      readPlyHead(&nVertices,&nFaces);
      std::vector<VertexStruct> vertices(nVertices);
      std::vector<FaceStruct> faces(nFaces);
      readPlyBody(nVertices,nFaces,vertices,faces);
      
      // Bound slice
      findSliceBounds();
      
      // Find all bounding spheres for the faces
      std::vector<PetscReal> bndSphereRadii(nFaces,0.);
      std::vector<std::array<PetscReal,3> > bndSphereCenters(nFaces);
      for (PetscInt nF=0; nF<nFaces; nF++) {
        findTriBndSphereOpt(faces[nF],vertices,
			    &bndSphereRadii[nF],bndSphereCenters[nF]);
      }
      
      // Loop over all faces, query points within bounding sphere and project on face
      // If the distance is better than the current one, replace it
      if (m_probDef.verbosity > 3) {
	PPW_VTHREE("Looping over faces\n");
      }
      std::vector<PetscReal> tempDist,tempCP;
      for (PetscInt nF=0; nF<nFaces; nF++) {
        PetscReal del = m_probDef.mesh.delta;
        uint32_t latRad = ((PetscInt) ceil(bndSphereRadii[nF]/del) ) + 2;
        TVec<uint32_t> intCen = realToIntPos(bndSphereCenters[nF]);
        std::array<uint32_t,3> fCubeLo({
            intCen[0]>latRad ? intCen[0] - latRad : 0,
            intCen[1]>latRad ? intCen[1] - latRad : 0,
            intCen[2]>latRad ? intCen[2] - latRad : 0
          });
        std::array<uint32_t,3> fCubeUp({
	    intCen[0] + latRad,
	    intCen[1] + latRad,
	    intCen[2] + latRad
	  });
	
        // Only query faces in/near local box
        if ((m_bndBoxLo<=fCubeUp && m_bndBoxUp>=fCubeLo)) {
          // Squeeze local cube to fit in this slice
          // cubes outside the slice will collapse and the lower loops won't run
          fCubeLo[0] = fCubeLo[0]<m_bndBoxLo[0] ? m_bndBoxLo[0] : fCubeLo[0];
          fCubeUp[0] = fCubeUp[0]>m_bndBoxUp[0] ? m_bndBoxUp[0] : fCubeUp[0];
	  
          // Loop over cube containing bounding sphere
          for (uint32_t i=fCubeLo[0]; i<=fCubeUp[0]; i++) {
            for (uint32_t j=fCubeLo[1]; j<=fCubeUp[1]; j++) {
              for (uint32_t k=fCubeLo[2]; k<=fCubeUp[2]; k++) {
                std::array<uint32_t,3> intP({i,j,k});
                std::array<PetscReal,3> rlP = intToRealPos(intP);
                PetscReal rsq = 0.0;
		rsq += PetscPowReal(rlP[0] - bndSphereCenters[nF][0],2.0);
		rsq += PetscPowReal(rlP[1] - bndSphereCenters[nF][1],2.0);
		rsq += PetscPowReal(rlP[2] - bndSphereCenters[nF][2],2.0);
		
                // only add if within bounding sphere
                if (rsq < 1.2*bndSphereRadii[nF]*bndSphereRadii[nF]) {
                  std::array<PetscReal,3> cp;
                  PetscReal dist = cpToTri(faces[nF],rlP,cp);
                  PetscInt idx = m_triMap.getDLoc(intP);
                  if (idx<0) { // new lattice point
                    m_triMap.addLeaf(intP,m_triMap.size());
                    tempDist.push_back(dist);
                    tempCP.push_back(cp[0]);
                    tempCP.push_back(cp[1]);
                    tempCP.push_back(cp[2]);
                  } else if (dist<tempDist[idx]) { // update existing point
                    tempDist[idx] = dist;
                    tempCP[3*idx] = cp[0];
                    tempCP[3*idx + 1] = cp[1];
                    tempCP[3*idx + 2] = cp[2];
                  }
                }
              }
            }
          }
        }
      }
      
      // Loop over all vertices,occasionally they get missed by the above scheme
      if (m_probDef.verbosity>3) { PPW_VTHREE("Looping over vertices\n"); }
      for (PetscInt nV=0; nV<nVertices; nV++) {
        TVec<PetscReal> vert(std::array<PetscReal,3>({
	      vertices[nV].x,
	      vertices[nV].y,
	      vertices[nV].z
	    }));
        uint32_t latRad = m_probDef.mesh.ord + 1;
        TVec<uint32_t> intCen = realToIntPos(vert);
        std::array<uint32_t,3> fCubeLo({
	    intCen[0]>latRad ? intCen[0] - latRad : 0,
	    intCen[1]>latRad ? intCen[1] - latRad : 0,
            intCen[2]>latRad ? intCen[2] - latRad : 0
          });
        std::array<uint32_t,3> fCubeUp({
	    intCen[0] + latRad,
	    intCen[1] + latRad,
	    intCen[2] + latRad
	  });
	
        // Only query faces in/near local box
        if ((m_bndBoxLo<=fCubeUp && m_bndBoxUp>=fCubeLo)) {
          // Squeeze local cube to fit in this slice
          // cubes outside the slice will collapse and the lower loops won't run
          fCubeLo[0] = fCubeLo[0]<m_bndBoxLo[0] ? m_bndBoxLo[0] : fCubeLo[0];
          fCubeUp[0] = fCubeUp[0]>m_bndBoxUp[0] ? m_bndBoxUp[0] : fCubeUp[0];
	  
          // Loop over cube containing bounding sphere
          for (uint32_t i=fCubeLo[0]; i<=fCubeUp[0]; i++) {
            for (uint32_t j=fCubeLo[1]; j<=fCubeUp[1]; j++) {
              for (uint32_t k=fCubeLo[2]; k<=fCubeUp[2]; k++) {
                std::array<uint32_t,3> intP({i,j,k});
                std::array<PetscReal,3> rlP = intToRealPos(intP);
                PetscReal dist = PetscSqrtReal(
                  (rlP[0] - vert[0])*(rlP[0] - vert[0]) +
                  (rlP[1] - vert[1])*(rlP[1] - vert[1]) +
                  (rlP[2] - vert[2])*(rlP[2] - vert[2]));
                PetscInt idx = m_triMap.getDLoc(intP);
                if (idx<0) { // new lattice point
                  m_triMap.addLeaf(intP,m_triMap.size());
                  tempDist.push_back(dist);
                  tempCP.push_back(vert[0]);
                  tempCP.push_back(vert[1]);
                  tempCP.push_back(vert[2]);
                } else if (dist<tempDist[idx]) { // update existing point
                  tempDist[idx] = dist;
                  tempCP[3*idx] = vert[0];
                  tempCP[3*idx + 1] = vert[1];
                  tempCP[3*idx + 2] = vert[2];
                }
              }
            }
          }
        }
      }
      
      // Gather full map to all nodes
      PetscInt mapSize = m_triMap.size();
      std::vector<uint64_t> tmpMap(mapSize);
      for (auto it=m_triMap.begin(); it!=m_triMap.end(); it++) {
        tmpMap[it->second] = it->first;
      }
      m_triMap.deleteTree();
      
      std::vector<PetscInt> recA(m_size,0);
      MPI_Allgather(&mapSize,1,MPI_INT,recA.data(),1,MPI_INT,PETSC_COMM_WORLD);
      std::vector<PetscInt> displs(m_size,0);
      for (PetscInt nR=1; nR<m_size; nR++) {
        displs[nR] = displs[nR - 1] + recA[nR - 1];
      }
      std::vector<uint64_t> recMap(displs[m_size - 1] + recA[m_size - 1],0);
      MPI_Allgatherv(tmpMap.data(),mapSize,MPI_UINT64_T,recMap.data(),recA.data(),
                     displs.data(),MPI_UINT64_T,PETSC_COMM_WORLD);
      for (PetscInt nM=0; nM<displs[m_size - 1] + recA[m_size - 1]; nM++) {
        m_triMap.addLeaf(recMap[nM],nM);
      }
      
      // Gather distances and closest points
      m_triangleDist.resize(recMap.size(),0.0);
      m_triangleCP.resize(recMap.size(),0.0);
      MPI_Allgatherv(tempDist.data(),tempDist.size(),MPI_DOUBLE,
		     m_triangleDist.data(),recA.data(),displs.data(),
		     MPI_DOUBLE,PETSC_COMM_WORLD);
      for (PetscInt nR=0; nR<m_size; nR++) {
        recA[nR] *= 3;
      }
      for (PetscInt nR=1; nR<m_size; nR++) {
        displs[nR] = displs[nR - 1] + recA[nR - 1];
      }
      std::vector<PetscReal> recCP(3*recMap.size(),0.0);
      MPI_Allgatherv(tempCP.data(),tempCP.size(),MPI_DOUBLE,recCP.data(),recA.data(),
                     displs.data(),MPI_DOUBLE,PETSC_COMM_WORLD);
      for (unsigned int nM=0; nM<recMap.size(); nM++) {
        m_triangleCP[nM] = TVec<PetscReal>(std::array<PetscReal,3>{recCP[3*nM],
								   recCP[3*nM + 1],
								   recCP[3*nM + 2]});
      }
    } catch(ErrorTrace& e) {
      PUSHERROR(e,"Triangulated surface failed to parse");
      throw e;
    }

    PetscLogEventEnd(PARSE_TRI,0,0,0,0);
  }
  
// Makes some restrictive assumptions on the file layout and contents
  void CPMeshGlobal::readPlyHead(PetscInt* nVertices,PetscInt* nFaces)
  {
    if (m_probDef.verbosity>3) {
      PPW_VTHREE("Reading header of PLY: %s\n",m_probDef.mesh.triPlyFile);
    }
    
    std::ifstream plyFile;
    plyFile.open(m_probDef.mesh.triPlyFile);
    if (!plyFile.is_open()) {
      ERRORTRACE("Failed to open ply file");
    }
    
    std::string line;
    PetscInt maxHead = 100;
    for (PetscInt nL=0; nL<maxHead; nL++) {
      getline(plyFile,line);
      std::istringstream lStrm(line);
      std::string token;
      while(getline(lStrm,token,' ')) {
        if (token.compare("element") == 0) {
          getline(lStrm,token,' ');
          if (token.compare("vertex") == 0) {
            getline(lStrm,token,' ');
            std::stringstream ss(token);
            ss >> *nVertices;
          } else if (token.compare("face") == 0) {
            getline(lStrm,token,' ');
            std::stringstream ss(token);
            ss >> *nFaces;
          }
        }          
      }
    }
    
    plyFile.close();
  }

// Makes some restrictive assumptions on the file layout and contents
  void CPMeshGlobal::readPlyBody(PetscInt nVertices,PetscInt nFaces,
				 std::vector<VertexStruct>& vertices,
				 std::vector<FaceStruct>& faces)
  {
    if (m_probDef.verbosity>3) {
      PPW_VTHREE("Reading body of PLY: %s\n",m_probDef.mesh.triPlyFile);
    }
    
    std::ifstream plyFile;
    plyFile.open(m_probDef.mesh.triPlyFile);
    if (!plyFile.is_open()) {
      ERRORTRACE("Failed to open ply file");
    }
    
    // bypass head
    std::string line;
    while(getline(plyFile,line)) {
      if (line.compare("end_header") == 0) { break; }
    }
    
    // Get all vertices read in
    PetscReal maxX = -std::numeric_limits<PetscReal>::max();
    PetscReal minX = std::numeric_limits<PetscReal>::max();
    PetscReal maxY = maxX;
    PetscReal minY = minX;
    PetscReal maxZ = maxX;
    PetscReal minZ = minX;
    for (PetscInt nV=0; nV<nVertices; nV++) {
      getline(plyFile,line);
      std::istringstream lStrm(line);
      std::string fTok;
      
      // x value
      getline(lStrm,fTok,' ');
      vertices[nV].x = stod(fTok);
      maxX = vertices[nV].x>maxX ? vertices[nV].x : maxX;
      minX = vertices[nV].x<minX ? vertices[nV].x : minX;
      
      // y value
      getline(lStrm,fTok,' ');
      vertices[nV].y = stod(fTok);
      maxY = vertices[nV].y>maxY ? vertices[nV].y : maxY;
      minY = vertices[nV].y<minY ? vertices[nV].y : minY;
      
      // z value
      getline(lStrm,fTok,' ');
      vertices[nV].z =stod(fTok);
      maxZ = vertices[nV].z>maxZ ? vertices[nV].z : maxZ;
      minZ = vertices[nV].z<minZ ? vertices[nV].z : minZ;
    }
    
    for (PetscInt nF=0; nF<nFaces; nF++) {
      PetscInt nv;
      getline(plyFile,line);
      std::istringstream lStrm(line);
      std::string token;
      // number of vertices to a face
      getline(lStrm,token,' ');
      std::stringstream ss(token);
      ss >> nv;
      if (nv!=3) {
      ERRORTRACE("Non-triangular face in ply file");
      }
      getline(lStrm,token,' ');
      std::stringstream s1(token);
      s1 >> faces[nF].v1;
      getline(lStrm,token,' ');
      std::stringstream s2(token);
      s2 >> faces[nF].v2;
      getline(lStrm,token,' ');
      std::stringstream s3(token);
      s3 >> faces[nF].v3;
      
      // Populate poll point list if desired
      if (m_probDef.pp.ioLevel == IOLevel::All ||
	 m_probDef.pp.ioLevel == IOLevel::Poll) {
        m_pollFaces.emplace_back(std::array<PetscInt,3>{faces[nF].v1,
							faces[nF].v2,
							faces[nF].v3});
      }
    }
    plyFile.close();
    
    // Re scale surface
    PetscReal cenX = (maxX + minX)/2.0;
    PetscReal cenY = (maxY + minY)/2.0;
    PetscReal cenZ = (maxZ + minZ)/2.0;
    PetscReal scale = 2.0/(maxX - minX);
    
    if ((maxY - minY) > (maxX - minX)) {
      scale = 2.0/(maxY - minY);
    }
    
    if ((maxZ - minZ) > (maxX - minX) && (maxZ - minZ) > (maxY - minY)) {
      scale = 2.0/(maxZ - minZ);
    }
    
    // Shift and scale vertices
    for (PetscInt nV=0; nV<nVertices; nV++) {
      vertices[nV].x = scale*(vertices[nV].x - cenX);
      vertices[nV].y = scale*(vertices[nV].y - cenY);
      vertices[nV].z = scale*(vertices[nV].z - cenZ);
      
      // Populate poll point list if desired
      if (m_probDef.pp.ioLevel == IOLevel::All ||
	 m_probDef.pp.ioLevel == IOLevel::Poll) {
        m_pollPos.emplace_back(std::array<PetscReal,3>{vertices[nV].x,
						       vertices[nV].y,
						       vertices[nV].z});
      }
    }
    
    m_probDef.mesh.bbSize[0] = 2*((int) (scale*(maxX - minX)/m_probDef.mesh.delta) +
				  m_probDef.mesh.ord + 2);
    m_probDef.mesh.bbSize[1] = 2*((int) (scale*(maxY - minY)/m_probDef.mesh.delta) +
				  m_probDef.mesh.ord + 2);
    m_probDef.mesh.bbSize[2] = 2*((int) (scale*(maxZ - minZ)/m_probDef.mesh.delta) +
				  m_probDef.mesh.ord + 2);
    m_center[0] = m_probDef.mesh.bbSize[0]/2;
    m_center[1] = m_probDef.mesh.bbSize[1]/2;
    m_center[2] = m_probDef.mesh.bbSize[2]/2;
    if ((m_probDef.pp.ioLevel == IOLevel::All ||
        m_probDef.pp.ioLevel == IOLevel::Poll) && m_probDef.mesh.nPoll>0) {
      m_nPollPts = nVertices;
      m_probDef.mesh.nPoll = nVertices;
    }
  }

/*  5
  
     V
     |\
     | \
     |  \
     |   \
   6 |    \   4
     | 0   \
     |      \
     |_______\
     Q        U
   1      2       3 */
  
  PetscReal CPMeshGlobal::cpToTri(const FaceStruct& face,
				  const std::array<PetscReal,3>& pos,
				  std::array<PetscReal,3>& CP)
  {
    // define ref coords
    PetscReal org[3] = {m_pollPos[face.v1][0],
			m_pollPos[face.v1][1],
			m_pollPos[face.v1][2]};
    PetscReal r[3] = {m_pollPos[face.v2][0] - org[0],
		      m_pollPos[face.v2][1] - org[1],
		      m_pollPos[face.v2][2] - org[2]};
    PetscReal s[3] = {m_pollPos[face.v3][0] - org[0],
		      m_pollPos[face.v3][1] - org[1],
		      m_pollPos[face.v3][2] - org[2]};
    PetscReal rXs[3] = {r[1]*s[2] - r[2]*s[1],
			r[2]*s[0] - r[0]*s[2],
			r[0]*s[1] - r[1]*s[0]};
    PetscReal det = rXs[0]*rXs[0] +
      r[0]*(s[1]*rXs[2] - s[2]*rXs[1]) +
      s[0]*(r[2]*rXs[1] - r[1]*rXs[2]);
    
    // project p onto r-s plane
    PetscReal q1 = (s[1]*rXs[2] - s[2]*rXs[1])*(pos[0] - org[0]) +
      (s[2]*rXs[0] - s[0]*rXs[2])*(pos[1] - org[1]) +
      (s[0]*rXs[1] - s[1]*rXs[0])*(pos[2] - org[2]);
    q1 /= det;
    PetscReal q2 = (r[2]*rXs[1] - r[1]*rXs[2])*(pos[0] - org[0]) +
      (r[0]*rXs[2] - r[2]*rXs[0])*(pos[1] - org[1]) +
      (r[1]*rXs[0] - r[0]*rXs[1])*(pos[2] - org[2]);
    q2 /= det;
    
    // All output relative to origin
    CP[0] = org[0];
    CP[1] = org[1];
    CP[2] = org[2];
    
    if (q1<0 && q2<0) { // origin
      // Do nothing,this is base case
    } else if (q1>1.0 && q2<q1 - 1.0) { // r vertex
      CP[0] = m_pollPos[face.v2][0];
      CP[1] = m_pollPos[face.v2][1];
      CP[2] = m_pollPos[face.v2][2];
    } else if (q2>1.0 && q2>q1+1.0) { // s vertex
      CP[0] = m_pollPos[face.v3][0];
      CP[1] = m_pollPos[face.v3][1];
      CP[2] = m_pollPos[face.v3][2];
    } else if (q2<0.0) { // bottom edge
      CP[0] += q1*r[0];
      CP[1] += q1*r[1];
      CP[2] += q1*r[2];
    } else if (q1<0.0) { // left edge
      CP[0] += q2*s[0];
      CP[1] += q2*s[1];
      CP[2] += q2*s[2];
    } else if (q1 + q2>1.0) { // hypo edge
      CP[0] += (1.0 + q1 - q2)*r[0]/2.0 + (1.0 + q2 - q1)*s[0]/2.0;
      CP[1] += (1.0 + q1 - q2)*r[1]/2.0 + (1.0 + q2 - q1)*s[1]/2.0;
      CP[2] += (1.0 + q1 - q2)*r[2]/2.0 + (1.0 + q2 - q1)*s[2]/2.0;
    } else { // point is internal
      CP[0] += q1*r[0] + q2*s[0];
      CP[1] += q1*r[1] + q2*s[1];
      CP[2] += q1*r[2] + q2*s[2];
    }
    return PetscSqrtReal((pos[0] - CP[0])*(pos[0] - CP[0]) +
			 (pos[1] - CP[1])*(pos[1] - CP[1]) +
			 (pos[2] - CP[2])*(pos[2] - CP[2]));
  }

  void CPMeshGlobal::findTriBndSphereOpt(const FaceStruct& face,
					 const std::vector<VertexStruct>& vertices,
					 PetscReal* radius,
					 std::array<PetscReal,3>& center)
  {
    /* Wikipedia: The useful minimum bounding circle of three points is
       defined either by the circumcircle (where three points are on the
       minimum bounding circle) or by the two points of the longest side
       of the triangle (where the two points define a diameter of the
       circle). */
    PetscReal ang[3],a[3],b[3],c[3];
    PetscReal xba,yba,zba,xca,yca,zca;
    PetscReal R=0.0,minang=0.0;
    unsigned short minangi;

    a[0] = vertices[face.v1].x;
    a[1] = vertices[face.v1].y;
    a[2] = vertices[face.v1].z;
    b[0] = vertices[face.v2].x;
    b[1] = vertices[face.v2].y;
    b[2] = vertices[face.v2].z;
    c[0] = vertices[face.v3].x;
    c[1] = vertices[face.v3].y;
    c[2] = vertices[face.v3].z;

    xba = b[0] - a[0];
    yba = b[1] - a[1];
    zba = b[2] - a[2];
    xca = c[0] - a[0];
    yca = c[1] - a[1];
    zca = c[2] - a[2];

    ang[0] = xca*xba + yca*yba + zca*zba;
    ang[1] = (-xba)*(c[0] - b[0]) + (-yba)*(c[1] - b[1]) + (-zba)*(c[2] - b[2]);
    ang[2] = (-xca)*(b[0] - c[0]) + (-yca)*(b[1] - c[1]) + (-zca)*(b[2] - c[2]);

    minang = ang[0];
    minangi = 0;
    if (ang[1]<minang) {
      minang = ang[1];
      minangi = 1;
    }
    
    if (ang[2]<minang) {
      minang = ang[2];
      minangi = 2;
    }
    
    if (minang<0) {
      /* obtuse triangle, use longest side */
      if (minangi == 0) {
        /* m = (B + C)/2 */
        center[0] = (b[0] + c[0])/2.0;
        center[1] = (b[1] + c[1])/2.0;
        center[2] = (b[2] + c[2])/2.0;
        R = ((b[0] - c[0])*(b[0] - c[0]) +
	     (b[1] - c[1])*(b[1] - c[1]) +
	     (b[2] - c[2])*(b[2] - c[2]))/4.0;
      } else if (minangi == 1) {
        /* m = (A + C)/2 */
        center[0] = (a[0] + c[0])/2.0;
        center[1] = (a[1] + c[1])/2.0;
        center[2] = (a[2] + c[2])/2.0;
        R = (xca*xca + yca*yca + zca*zca)/4.0;
      } else {
        /* m = (A + B)/2 */
        center[0] = (a[0] + b[0])/2.0;
        center[1] = (a[1] + b[1])/2.0;
        center[2] = (a[2] + b[2])/2.0;
        R = (xba*xba + yba*yba + zba*zba)/4.0;
      }
      R = PetscSqrtReal(R);
    } else {
      /* acute triangle, use circumcenter
       
         uses code from
         http://www.ics.uci.edu/~eppstein/junkyard/circumcenter.html
       
         |                                                           |
         | |c-a|^2 [(b-a)x(c-a)]x(b-a) + |b-a|^2 (c-a)x[(b-a)x(c-a)] |
         |                                                           |
         r = -------------------------------------------------------------,
         2 | (b-a)x(c-a) |^2
         
         |c-a|^2 [(b-a)x(c-a)]x(b-a) + |b-a|^2 (c-a)x[(b-a)x(c-a)]
         m = a + ---------------------------------------------------------.
         2 | (b-a)x(c-a) |^2
      */
      PetscReal balength,calength,xcrossbc,ycrossbc,zcrossbc,denominator; 
      /* Squares of lengths of the edges incident to `a'. */
      balength = xba*xba + yba*yba + zba*zba;
      calength = xca*xca + yca*yca + zca*zca;
      
      /* Cross product of these edges. */
      /* Take your chances with floating-point roundoff.  (code on
         webpage could also use a more robust library for this). */
      xcrossbc = yba*zca - yca*zba;
      ycrossbc = zba*xca - zca*xba;
      zcrossbc = xba*yca - xca*yba;
      
      /* Calculate the denominator of the formulae. */
      denominator = 0.5/(xcrossbc*xcrossbc + ycrossbc*ycrossbc + zcrossbc*zcrossbc);
      
      /* Calculate offset (from `a') of circumcenter. */
      center[0] = ((balength*yca - calength*yba)*zcrossbc -
		   (balength*zca - calength*zba)*ycrossbc)*denominator;
      center[1] = ((balength*zca - calength*zba)*xcrossbc -
		   (balength*xca - calength*xba)*zcrossbc)*denominator;
      center[2] = ((balength*xca - calength*xba)*ycrossbc -
		   (balength*yca - calength*yba)*xcrossbc)*denominator;
      
      /*R = sqrt(xcirca*xcirca + ycirca*ycirca + zcirca*zcirca) */
      R = sqrt(center[0]*center[0] + center[1]*center[1] + center[2]*center[2]);
      center[0] += a[0];
      center[1] += a[1];
      center[2] += a[2];
    }
    
    *radius = R + 2.0*(m_gamma + m_probDef.mesh.delta);
  }

  void CPMeshGlobal::findTriBndSphereCen(FaceStruct* face,VertexStruct* vertices,
					 PetscReal* radius,PetscReal* center)
  {
    PetscReal a[3],b[3],c[3];
    PetscReal rr = 0.0,rra = 0.0,rrb = 0.0,rrc = 0.0;
    
    // Vertex vectors
    a[0] = vertices[face->v1].x;
    a[1] = vertices[face->v1].y;
    a[2] = vertices[face->v1].z;
    b[0] = vertices[face->v2].x;
    b[1] = vertices[face->v2].y;
    b[2] = vertices[face->v2].z;
    c[0] = vertices[face->v3].x;
    c[1] = vertices[face->v3].y;
    c[2] = vertices[face->v3].z;
    
    // Centroid
    center[0] = (a[0] + b[0] + c[0])/3.;
    center[1] = (a[1] + b[1] + c[1])/3.;
    center[2] = (a[2] + b[2] + c[2])/3.;
    
    // Radius of circumsphere
    rra = (center[0] - a[0])*(center[0] - a[0]) +
      (center[1] - a[1])*(center[1] - a[1]) +
      (center[2] - a[2])*(center[2] - a[2]);
    rra = (center[0] - b[0])*(center[0] - b[0]) +
      (center[1] - b[1])*(center[1] - b[1]) +
      (center[2] - b[2])*(center[2] - b[2]);
    rra = (center[0] - c[0])*(center[0] - c[0]) +
      (center[1] - c[1])*(center[1] - c[1]) +
      (center[2] - c[2])*(center[2] - c[2]);
    
    rr = PetscMax(PetscMax(rra,rrb),rrc);
    *radius = PetscSqrtReal(rr) + m_gamma;
  }

} // Close namespace
