import numpy as np
import mayavi.mlab as mlab
import h5py
import glob
import sys
import matplotlib.pyplot as plt
import matplotlib.tri as tri

def subSampleData(data,N=250000):
    # Can only reasonably subsample cloud data
    if "cloud_pos" in data.keys():
        # Only subsample if more than N entries are present
        if data["cloud_pos"].shape[0] > N:
            nCP = data["cloud_pos"].shape[0]
            stride = nCP//N
            for k in data.keys():
                if "cloud" in k:
                    data[k] = data[k][0::stride]
            print("Subsampled cloud data from",nCP,"to",data["cloud_pos"].shape[0],"points")
        
def dataFromFile(fStr):
    print('Opening ', fStr)
    f = h5py.File(fStr,'r')
    data = {}
    for k in f.keys():
        data[k] = f[k][()]
    # Time shouldn't be array-valued
    data['time'] = data['time'][0]
    data['filename'] = fStr
    print('Time: %e' % data['time'])
    f.close()
    subSampleData(data)
    return data

def dataFromGlob(gStr):
    dset = []
    for fStr in glob.iglob(gStr):
        dset.append(dataFromFile(fStr))
    return dset

def plotPoll2(d,fld,idx=0,cmap='CMRmap',fname=''):
    # Generate triangulation
    triangles = tri.Triangulation(d['poll_pos'][:,0],d['poll_pos'][:,1],d['poll_conn'])
    # Create figure, plot, and either show/save figure
    plt.figure(figsize=(9.6,5.4),dpi=200)
    plt.gca().set_aspect('equal')
    if len(d[fld].shape)>1:
        plt.tripcolor(triangles,d[fld][:,idx],cmap=cmap,shading='gouraud')
        plt.colorbar()
    else:
        plt.tripcolor(triangles,d[fld],cmap=cmap,shading='gouraud')
        plt.colorbar()
    if fname=='':
        plt.show()
    else:
        plt.savefig(fname)
        plt.close()

def plotCloud2(d,fld,idx=0,cmap='CMRmap',fname=''):
    # Create figure, plot, and either show/save figure
    plt.figure(figsize=(9.6,5.4),dpi=200)
    plt.gca().set_aspect('equal')
    if len(d[fld].shape)>1:
        plt.scatter(d['cloud_pos'][:,0],d['cloud_pos'][:,1],s=3,c=d[fld][:,idx],cmap=cmap)
        plt.colorbar()
    else:
        plt.scatter(d['cloud_pos'][:,0],d['cloud_pos'][:,1],s=3,c=d[fld],cmap=cmap)
        plt.colorbar()
    if fname=='':
        plt.show()
    else:
        plt.savefig(fname)
        plt.close()

def plotPoll3(d,fld,idx=0,cmap='CMRmap',fname=''):
    # Set whether or not a plot window appears
    optOff = mlab.options.offscreen
    if fname=='':
        mlab.options.offscreen = False
    else:
        mlab.options.offscreen = True
    if len(d[fld].shape)>1:
        mlab.triangular_mesh(d['poll_pos'][:,0],d['poll_pos'][:,1],d['poll_pos'][:,2],d['poll_conn'],scalars=d[fld][:,idx],colormap=cmap)
    else:
        mlab.triangular_mesh(d['poll_pos'][:,0],d['poll_pos'][:,1],d['poll_pos'][:,2],d['poll_conn'],scalars=d[fld],colormap=cmap)
    if fname=='':
        mlab.show()
    else:
        mlab.savefig(fname)
    mlab.options.offscreen = optOff

def plotCloud3(d,fld,idx=0,cmap='CMRmap',fname=''):
    optOff = mlab.options.offscreen
    if fname=='':
        mlab.options.offscreen = False
    else:
        mlab.options.offscreen = True
    if len(d[fld].shape)>1:
        zp = d[fld][:,idx] if d['cloud_pos'].shape[1]<3 else d['cloud_pos'][:,2]
        mlab.points3d(d['cloud_pos'][:,0],d['cloud_pos'][:,1],zp,d[fld][:,idx],colormap=cmap,scale_mode='none',scale_factor=0.08)
    else:
        zp = d[fld] if d['cloud_pos'].shape[1]<3 else d['cloud_pos'][:,2]
        mlab.points3d(d['cloud_pos'][:,0],d['cloud_pos'][:,1],zp,d[fld],colormap=cmap,scale_mode='none',scale_factor=0.08)
    if fname=='':
        mlab.show()
    else:
        mlab.savefig(fname)
    mlab.options.offscreen = optOff

def plot2d(d,argv):
    if(argv[2]=="poll"):
        fld = "poll_" + argv[3]
        if(not(fld in d.keys())):
           print("Requested field is not present, available fields are:")
           print(d.keys())
           print("Note: prefix 'poll_' is not required")
           exit()
        if(len(argv) > 4):
            plotPoll2(d,fld,idx=int(argv[4]))
        else:
            plotPoll2(d,fld)
    else:
        fld = "cloud_" + argv[3]
        if(not(fld in d.keys())):
           print("Requested field is not present, available fields are:")
           print(d.keys())
           print("Note: prefix 'cloud_' is not required")
           exit()
        if(len(argv) > 4):
            plotCloud2(d,fld,idx=int(argv[4]))
        else:
            plotCloud2(d,fld)

def plot3d(d,argv):
    if(argv[2]=="poll"):
        fld = "poll_" + argv[3]
        if(not(fld in d.keys())):
           print("Requested field is not present, available fields are:")
           print(d.keys())
           print("Note: prefixes 'cloud_' or 'poll_' are not required")
           exit()
        if(len(argv) > 4):
            plotPoll3(d,fld,idx=int(argv[4]))
        else:
            plotPoll3(d,fld)
    else:
        fld = "cloud_" + argv[3]
        cmap = "Vega20" if argv[3] == "part" else "CMRmap"
        if(not(fld in d.keys())):
           print("Requested field is not present, available fields are:")
           print(d.keys())
           print("Note: prefixes 'cloud_' or 'poll_' are not required")
           exit()
        if(len(argv) > 4):
            plotCloud3(d,fld,idx=int(argv[4]),cmap=cmap)
        else:
            plotCloud3(d,fld,cmap=cmap)

def saveFrames(dset,pcmd):
    for d in dset:
        ln = d['filename'].split('.')
        del ln[-1]
        fname = '.'.join(ln)+'.png'
        pcmd(d,fname)

if __name__ == "__main__":
    helpStr = '''
cpmplot: A simple plotting tool for visualizing the data files generated by DD-CPM. 
The calling convention is:
    python cpmplot.py FILE DATATYPE FIELD [IDX]
where:
    FILE is an h5 file generated by the DD-CPM library
    DATATYPE is either cloud or poll or set the type of data being plotted
    FIELD is the name of the data to plot, typically sol
    IDX optionally sets the index of the data for multicomponent solutions, defaults to 0
An example call is:
    python cpmplot.py ../build/data/Sphere20_2_12_4/Sphere20_2_12_4ORAS0.h5 cloud 0
which plots the "u" component of the solution on the point cloud surrounding a sphere. Similarly:
    python cpmplot.py ../build/data/Sphere20_2_12_4/Sphere20_2_12_4ORAS0.h5 poll 1
would plot the "v" component on the pollling surface (assuming this data file came from a multicomponent problem).
    '''
    if(sys.argv[1]=="help" or sys.argv[1]=="-h"):
        print(helpStr)
        exit()
    # Open dataset
    d = dataFromFile(sys.argv[1])
    # Find dimension of embedding space
    dim = 2
    if("poll_pos" in d.keys()):
        dim = d["poll_pos"].shape[1]
    else:
        dim = d["cloud_pos"].shape[1]
    # Make appropriate plot
    if(dim == 2):
        plot2d(d,sys.argv)
    else:
        plot3d(d,sys.argv)
