// Set up and solve the Schnackenberg equation
// Author: Ian May
/*! \file DDCPSchnackenberg.cpp
    \brief This driver code shows how to setup and solve the Schnackenberg equations, \f$\partial_t u - D_u\Delta_{\mathcal{S}}u=500(u^2v-u-0.048113)\f$, \f$\partial_t v - D_v\Delta_{\mathcal{S}}v=500(1.202813-u^2v)\f$, intrinsic to a surface \f$\mathcal{S}\f$, where \f$D_u\f$ and \f$D_v\f$ are positive diffusivities of the species. Additionally, this driver code demonstrates how user defined surfaces are specified to the dd-cpm library.
*/

static char helpStr[] = "This example solves the Schnackenberg equation intrinsic to a surface. The diffusion terms are solved implicitly while the nonlinear reaction terms are handled explicitly.\n\n\
Consider solving on a disc in parallel:\n\
    mpiexec -n 4 bin/DDCPSchnackenberg.ex -infile inputFiles/disc.icpm -cp_pc_ras -mesh_res 50 -mesh_nparts 12 -mesh_nover 4 -mesh_npoll 400 -dd_trans_robfo -dd_osm_alpha 2 -dd_osm_alpha_cross 20 -time_final 100 -time_dt 1.e-8 -pp_plotfreq 20 -sub_pc_type ilu -ts_monitor\n\n\
or solve on a user defined surface (hemisphere) \n\
    mpiexec -n 4 bin/DDCPSchnackenberg.ex -infile inputFiles/user.icpm -cp_pc_ras -mesh_res 20 -mesh_nparts 12 -mesh_nover 4 -dd_trans_robfo -dd_osm_alpha 4 -dd_osm_alpha_cross 20 -time_final 100 -time_dt 1.e-3 -pp_plotfreq 100 -sub_pc_type ilu -ts_monitor\n\n";

#include <random> // Needed for ICs

// DDCPM library master header
#include "ddcpm.H"

// Library namespace
using namespace DDCPM;

//! User provided closest point function
/*! \param x Position in \f$\mathbb{R}^d\f$, not constrained to the surface
  \returns cp Closest point to x on the surface
*/
TVec<PetscReal> hemiCP(const TVec<PetscReal>& x)
{
  TVec<PetscReal> cp = x;
  cp[2] = x[2]>0. ? x[2] : -x[2];
  cp.normalize();
  return cp;
}

//! User provided distance function
/*! \param x Position in \f$\mathbb{R}^d\f$, not constrained to the surface
  \returns Distance to the closest point
*/
PetscReal hemiDist(const TVec<PetscReal>& x)
{
  if(x[2] >= 0.) {
    return x.norm() - 1.;
  }
  PetscReal pd = PetscSqrtReal(x[0]*x[0]+x[1]*x[1]) - 1.;
  return PetscSqrtReal(x[2]*x[2] + pd*pd);
}

//! User provided surface normal function
/*! \param x Position in \f$\mathbb{R}^d\f$, not constrained to the surface
  \returns Surface normal vector at cp(x)
*/
TVec<PetscReal> hemiSN(const TVec<PetscReal>& x)
{
  TVec<PetscReal> cp = x;
  cp[2] = x[2]>0. ? x[2] : 0.;
  cp.normalize();
  return cp;
}

//! User provided boundary conditions
/*! \param x Position in \f$\mathbb{R}^d\f$, not constrained to the surface
  \returns Multiplier on extension operation to give desired BC
*/
PetscReal hemiBCs(const TVec<PetscReal>& x)
{
  PetscReal th = PetscAtan2Real(x[1],x[0]), marker = 0.5 + PetscSinReal(3.0*th);
  // Homogeneous Dirichlet if x<0, homogeneous Neumann otherwise
  // Note this only applies to points that map to the boundary, so z<0 required
  return (marker<0. && x[2]<0.) ? -1 : 1.;
}

std::default_random_engine gen;
std::uniform_real_distribution<PetscReal> rdist(0.0,1.0);

//! Initial condition for Schnackenberg equation
/*! \param x Position in \f$\mathbb{R}^d\f$, not constrained to the surface
  \returns {u,v} as a vector to be consistent with multicomponent equations
*/
std::vector<PetscScalar> icSchnackenberg(const TVec<PetscReal>& x)
{
  PetscScalar u = 0.02 + 0.01*PetscSinReal(5.0*x[0])*PetscCosReal(3.0*x[1]);
  PetscScalar v = 4.0 + PetscSinReal(5.0*x[0]*x[1])*PetscSinReal(5.0*x[0]*x[0]*x[0]*x[1]);
  return {u,v};
}

const PetscReal scPar_Du = 0.001;
const PetscReal scPar_Dv = 0.1;
const PetscReal scPar_a = 0.003;
const PetscReal scPar_b = 0.7;
const PetscReal scPar_L = 0.06;
//! Reaction terms for Schnackenberg equation
/*! \param x Position in \f$\mathbb{R}^d\f$, constrained to the surface
  \param t Solution time when called
  \returns Reaction terms as a vector
*/
std::vector<PetscScalar> rhsSchnackenberg(const TVec<PetscReal>& x, PetscReal t, const PetscScalar* u)
{
  (void) x;
  (void) t;
  return {(scPar_a - u[0] + u[0]*u[0]*u[1] + scPar_L*u[1]), (scPar_b - u[0]*u[0]*u[1] - scPar_L*u[1])};
}

int main(int argc, char **args)
{
  // Initialize PETSc
  PetscInitialize(&argc,&args,(char*)0,helpStr);
  try {
    // Get input file name from command line
    PetscBool infSet;
    char inFile[100];
    PetscOptionsGetString(NULL, NULL, "-infile", inFile, sizeof(inFile), &infSet);
    // Problem Definition
    ProblemDefinition pd;
    if(infSet) {
      pd.setFromFile(inFile);
    } else {
      PP_WARN("An input file must be supplied\n");
      PetscFinalize();
      return 0;
    }
    pd.setFromCLI(); // let command line options override the input file
    pd.printConfig();
    // Global post processor
    CPPostProc globalProc(pd);
    // Differential operators
    GridFunc lapU("LaplacianSecondOrder", pd.mesh.dim, pd.mesh.delta, -scPar_Du);
    GridFunc lapV("LaplacianSecondOrder", pd.mesh.dim, pd.mesh.delta, -scPar_Dv);
    std::vector<GridFunc*> schnackOp = {&lapU, &lapV};
    DiffEq equation(pd,schnackOp,&rhsSchnackenberg,&icSchnackenberg);
    // Construct global mesh, perform any partitioning required
    CPMeshGlobal globalMesh(pd, equation, &hemiCP, &hemiDist, &hemiSN, &hemiBCs);
    // Construct global problem, and (O)RAS sub-problems if requested
    ProblemGlobal globalProblem(pd, globalMesh, equation, true);
    // Construct monitor context, this provides a hook to write solution files
    TsCtx tsMon(pd, globalMesh, globalProblem, globalProc);
    // Solve the problem
    globalProblem.solveTransient(&CPPostProc::tsMonitor, &tsMon);
  } catch(ErrorTrace& e) {
    e.printTrace();
  }
  PetscFinalize();
  
  return 0;
}
