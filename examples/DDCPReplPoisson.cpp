static char helpStr[] = "This example solves the Gray-Scott equation intrinsic to a surface. The diffusion terms are solved implicitly while the nonlinear reaction terms are handled explicitly.\n\
Consider solving on a circle with ARKIMEX3 and ORAS preconditioned GMRES as an implicit solver:\n\
    bin/DDCPGrayScott.ex -infile inputFiles/circle.icpm -cp_pc_ras -mesh_res 200 -mesh_nparts 12 -mesh_nover 4 -dd_trans_robfo -dd_osm_alpha 4 -ksp_converged_reason -time_final 2500 -sub_pc_type ilu -ts_monitor\n\n\
or solve on the unit disc with homogeneous Neumann boundary.cpp\n\
    mpiexec -n 4 bin/DDCPGrayScott.ex -infile inputFiles/disc.icpm -cp_pc_ras -mesh_res 100 -mesh_nparts 12 -mesh_nover 4 -mesh_npoll 800 -dd_trans_robfo -dd_osm_alpha 4 -dd_osm_alpha_cross 20 -time_final 6000 -sub_pc_type ilu -ts_monitor\n\n\
or solve on the torus (this will take a while and you should increase the number of processes if you can):\n\
    mpiexec -n 4 bin/DDCPGrayScott.ex -infile inputFiles/torus.icpm -cp_pc_ras -mesh_res 30 -mesh_nparts 12 -mesh_nover 2 -dd_trans_dirfo -time_final 10000 -pp_plotfreq 50 -pp_out_poll -mesh_npoll 400 -sub_pc_type ilu -ts_monitor\n\n";

// DDCPM library master header
#include "ddcpm.H"

// Library namespace
using namespace DDCPM;

std::vector<PetscScalar> rhsReplPoisson(const TVec<PetscReal>& x)
{
  PetscReal phi = PetscAtan2Real(x[1],x[0]);
  PetscReal sp = PetscSinReal(5.*(phi+0.5)),sm = PetscSinReal(5.*(phi-0.5));
  PetscScalar u = 1.0 - PetscExpReal(-8.*sp*sp);
  PetscScalar v = PetscExpReal(-10.*sm*sm);
  return {u,v};
}

int main(int argc, char **args)
{
  // Initialize PETSc
  PetscInitialize(&argc,&args,(char*)0,helpStr);
  try {
    // Get input file name from command line
    PetscBool infSet;
    char inFile[100];
    PetscOptionsGetString(NULL, NULL, "-infile", inFile, sizeof(inFile), &infSet);
    // Problem Definition
    ProblemDefinition pd;
    if(infSet) {
      pd.setFromFile(inFile);
    } else {
      PP_WARN("An input file must be supplied\n");
      PetscFinalize();
      return 0;
    }
    pd.setFromCLI();
    pd.printConfig();
    
    CPPostProc globalProc(pd);
    
    GridFunc shift("Identity", pd.mesh.dim, pd.mesh.delta, 1.0);
    GridFunc lapU("LaplacianSecondOrder", pd.mesh.dim, pd.mesh.delta, 1.0);
    GridFunc slU = shift - lapU;
    GridFunc lapV("LaplacianSecondOrder", pd.mesh.dim, pd.mesh.delta, 2.0);
    GridFunc slV = shift - lapV;
    
    std::vector<GridFunc*> replOp = {&slU, &slV};
    DiffEq equation(pd, replOp, &rhsReplPoisson);
    
    CPMeshGlobal globalMesh(pd, equation);
    
    ProblemGlobal globalProblem(pd, globalMesh, equation, false);
    globalProblem.solveStationary();
    
    globalProc.writeData(0, 0, globalMesh, globalProblem);
    
  } catch(ErrorTrace& e) {
    e.printTrace();
  }
  PetscFinalize();
  
  return 0;
}
