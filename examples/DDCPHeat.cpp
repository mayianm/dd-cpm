// Set up and solve the heat equation
// Author: Ian May
/*! \file DDCPHeat.cpp
    \brief This driver code shows how to setup and solve the heat equation, \f$\partial_t u - \Delta_{\mathcal{S}}u=f(x,t)\f$, intrinsic to a surface \f$\mathcal{S}\f$.
*/

static char helpStr[] = "This example solves the heat equation intrinsic to a surface.\n\
Consider solving on a circle with ARKIMEX3 and ORAS preconditioned GMRES as an implicit solver:\n\
    bin/DDCPHeat.ex -infile inputFiles/circle.icpm -cp_pc_ras -mesh_res 200 -mesh_nparts 12 -mesh_nover 4 -dd_trans_robfo -dd_osm_alpha 4 -time_final 10 -sub_pc_type ilu -sub_pc_factor_levels 1 -sub_pc_factor_mat_ordering_type rcm -ksp_converged_reason -ts_monitor\n\n\
or the same in parallel:\n\
    mpiexec -n 4 bin/DDCPHeat.ex -infile inputFiles/circle.icpm -cp_pc_ras -mesh_res 200 -mesh_nparts 12 -mesh_nover 4 -dd_trans_robfo -dd_osm_alpha 4 -time_final 10 -sub_pc_type ilu -sub_pc_factor_levels 1 -sub_pc_factor_mat_ordering_type rcm -ksp_converged_reason -ts_monitor\n\n\
or solve on a torus with RAS preconditioned GMRES as an implicit solver\n\
    mpiexec -n 4 bin/DDCPHeat.ex -infile inputFiles/torus.icpm -cp_pc_ras -mesh_res 20 -mesh_nparts 12 -mesh_nover 4 -dd_trans_robfo -dd_osm_alpha 4 -dd_osm_alpha_cross 20 -time_final 10 -sub_pc_type ilu -sub_pc_factor_levels 1 -sub_pc_factor_mat_ordering_type rcm -ts_monitor\n\n";

// DDCPM library master header
#include "ddcpm.H"

// Library namespace
using namespace DDCPM;

//! Initial condition for heat equation
/*! \param x Position in \f$\mathbb{R}^d\f$, not constrained to the surface
  \returns {u} as a vector to be consistent with multicomponent equations
*/
std::vector<PetscScalar> icHeat(const TVec<PetscReal>& x)
{
  (void) x;
  return {0.};
}

//! Reaction terms for heat equation
/*! \param x Position in \f$\mathbb{R}^d\f$, constrained to the surface
  \param t Solution time when called
  \returns Forcing term as a vector
*/
std::vector<PetscScalar> rhsHeat(const TVec<PetscReal>& x, PetscReal t, const PetscScalar *u)
{
  (void) u;
  PetscReal phi = PetscAtan2Real(x[1],x[0]);
  return {PetscCosReal(t)*PetscSinReal(3.*phi)};
}

int main(int argc, char **args)
{
  // Initialize PETSc
  PetscInitialize(&argc,&args,(char*)0,helpStr);
  try {
    // Get input file name from command line
    PetscBool infSet;
    char inFile[100];
    PetscOptionsGetString(NULL,NULL,"-infile",inFile,sizeof(inFile),&infSet);
    // Problem Definition
    ProblemDefinition pd;
    if(infSet) {
      pd.setFromFile(inFile);
    } else {
      PP_WARN("An input file must be supplied\n");
      PetscFinalize();
      return 0;
    }
    pd.setFromCLI(); // let command line options override the input file
    pd.printConfig();
    // Global post processor
    CPPostProc globalProc(pd);
    // Differential operators
    // Note that time is not included here
    // Note also that the diffusivity coefficient is negative
    GridFunc laplacian("LaplacianSecondOrder",pd.mesh.dim,pd.mesh.delta,-1.);
    DiffEq equation(pd,&laplacian,&rhsHeat,&icHeat);
    // Construct global mesh, perform any partitioning required
    CPMeshGlobal globalMesh(pd,equation);
    // Construct global problem, and (O)RAS sub-problems if requested
    ProblemGlobal globalProblem(pd,globalMesh,equation,true);
    // Construct monitor context, this provides a hook to write solution files
    // as the solution progresses
    TsCtx tsMon(pd,globalMesh,globalProblem,globalProc);
    globalProblem.solveTransient(&CPPostProc::tsMonitor,&tsMon);
  } catch(ErrorTrace& e) {
    e.printTrace();
  }
  PetscFinalize();
  
  return 0;
}
