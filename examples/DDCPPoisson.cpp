// Set up and solve a shifted Poisson equation
// Author: Ian May
/*! \file DDCPPoisson.cpp
    \brief This driver code shows how to setup and solve the shifted Poisson equation, \f$\left(c-\Delta_{\mathcal{S}}\right)u=f(x)\f$, intrinsic to a surface \f$\mathcal{S}\f$, with a positive real constant \f$c\f$.
*/

static char helpStr[] = "This example solves the shifted Poisson equation intrinsic to a surface.\n\
Consider solving on a circle with ORAS preconditioned GMRES:\n\
    bin/DDCPPoisson.ex -infile inputFiles/circle.icpm -cp_pc_ras -mesh_res 200 -mesh_nparts 12 -mesh_nover 4 -dd_trans_robfo -dd_osm_alpha 4 -ksp_converged_reason\n\n\
or solve on a sphere in parallel:\n\
    mpiexec -n 4 bin/DDCPPoisson.ex -infile inputFiles/sphere.icpm -cp_pc_ras -mesh_res 40 -mesh_nparts 12 -mesh_nover 4 -dd_trans_robfo -dd_osm_alpha 4 -sub_pc_type ilu -sub_pc_factor_levels 1 -sub_pc_factor_mat_ordering_type rcm -ksp_converged_reason\n\n\
or the same using the MUMPS direct solver\n\
    mpiexec -n 4 bin/DDCPPoisson.ex -infile inputFiles/sphere.icpm -mesh_res 40 -mesh_nparts 1 -ksp_type preonly -pc_type lu -pc_factor_mat_type mumps\n\n";

// DDCPM library master header
#include "ddcpm.H"

// Library namespace
using namespace DDCPM;

//! Forcing function \f$f\f$ in the equation \f$(c-\Delta_{\mathcal{S}})u = f\f$
/*! \param x Position in \f$\mathbb{R}^d\f$, but is constrained to the surface
  \returns Value of \f$f\f$ as a vector to be consistent with multicomponent equations
*/
std::vector<PetscScalar> forcingFunc(const TVec<PetscReal>& x)
{
  const PetscReal pi = 4.*PetscAtan2Real(1., 1.);
  PetscReal theta = PetscAtan2Real(x[1], x[0]);
  PetscReal phi = x.getDim()>2 ? PetscAtan2Real(sqrt(x[0]*x[0]+x[1]*x[1]), x[2]) : 1.3;
  return {phi*(pi-phi)*PetscSinReal(3.*phi)*(PetscSinReal(theta)+PetscCosReal(10.*theta))/2.};
}

// Main takes in CLI arguments and forwards them to PETSc to be parsed as needed
int main(int argc, char **args)
{
  // Initialize PETSc
  PetscInitialize(&argc,&args,(char*)0,helpStr);
  try {
    // Get input file name from command line
    PetscBool infSet;
    char inFile[100];
    PetscOptionsGetString(NULL, NULL, "-infile", inFile, sizeof(inFile), &infSet);
    // Problem Definition object
    // This reads in settings from the input file, applies overrides from the
    // command line, and passes these settings to all subsequent objects
    ProblemDefinition pd;
    if(infSet) {
      pd.setFromFile(inFile);
    } else {
      PP_WARN("An input file must be supplied\n");
      PetscFinalize();
      return 0;
    }
    pd.setFromCLI(); // Let the command line arguments override settings
    pd.printConfig(); // Dump settings to stdout for reference
    // Global post processor object
    // This writes solutions as HDF5 files
    CPPostProc globalProc(pd);
    // Differential operator objects
    // The grid functions define the building blocks of the overall operator
    // They can be composed into more complicated grid functions, allowing
    // a nearly direct translation from PDE to discretization
    GridFunc laplacian("LaplacianSecondOrder", pd.mesh.dim, pd.mesh.delta, 1);
    GridFunc shift("Identity", pd.mesh.dim, pd.mesh.delta, 1.);
    GridFunc shiftLap = shift - laplacian; // Total grid function for \f$c-\Delta_{\mathcal{S}}\f$
    // The equation combines the grid function with the forcing function
    DiffEq equation(pd, &shiftLap, &forcingFunc);
    // Construct a global mesh object
    // This finds all active and ghost nodes necessary to define the CPM discretization
    // This also partitions the mesh if needed by the (O)RAS preconditioner
    CPMeshGlobal globalMesh(pd, equation);
    // Construct global problem, and solve the equation
    // This combines all of the above information to produce the global matrix,
    // right hand side, and (O)RAS sub-problems if desired
    ProblemGlobal globalProblem(pd, globalMesh, equation, false);
    globalProblem.solveStationary();
    // Write out the full solution, right hand side, and subproblem info if present
    globalProc.writeData(0, 0, globalMesh, globalProblem);
  } catch(ErrorTrace& e) {
    e.printTrace();
  }
  PetscFinalize();
  
  return 0;
}
