// Set up and solve the Gray-Scott equation
// Author: Ian May
/*! \file DDCPGrayScott.cpp
    \brief This driver code shows how to setup and solve the Gray-Scott equations, \f$\partial_t u - D_u\Delta_{\mathcal{S}}u=F(x)(1-u)-uv^2\f$, \f$\partial_t v - D_v\Delta_{\mathcal{S}}v=uv^2-(F(x)+K(x))v\f$, intrinsic to a surface \f$\mathcal{S}\f$, where \f$D_u\f$ and \f$D_v\f$ are positive diffusivities of the species and \f$F\f$ and \f$K\f$ are the (positive) feed and kill rates.
*/

static char helpStr[] = "This example solves the Gray-Scott equation intrinsic to a surface. The diffusion terms are solved implicitly while the nonlinear reaction terms are handled explicitly.\n\
Consider solving on a circle with ARKIMEX3 and ORAS preconditioned GMRES as an implicit solver:\n\
    bin/DDCPGrayScott.ex -infile inputFiles/circle.icpm -cp_pc_ras -mesh_res 200 -mesh_nparts 12 -mesh_nover 4 -dd_trans_robfo -dd_osm_alpha 4 -ksp_converged_reason -time_final 2500 -sub_pc_type ilu -ts_monitor\n\n\
or solve on the unit disc with homogeneous Neumann boundary.cpp\n\
    mpiexec -n 4 bin/DDCPGrayScott.ex -infile inputFiles/disc.icpm -cp_pc_ras -mesh_res 100 -mesh_nparts 12 -mesh_nover 4 -mesh_npoll 800 -dd_trans_robfo -dd_osm_alpha 4 -dd_osm_alpha_cross 20 -time_final 6000 -sub_pc_type ilu -ts_monitor\n\n\
or solve on the torus (this will take a while and you should increase the number of processes if you can):\n\
    mpiexec -n 4 bin/DDCPGrayScott.ex -infile inputFiles/torus.icpm -cp_pc_ras -mesh_res 30 -mesh_nparts 12 -mesh_nover 2 -dd_trans_dirfo -time_final 10000 -pp_plotfreq 50 -pp_out_poll -mesh_npoll 400 -sub_pc_type ilu -ts_monitor\n\n";

// DDCPM library master header
#include "ddcpm.H"

// Library namespace
using namespace DDCPM;

//! Initial condition for Gray-Scott equation
/*! \param x Position in \f$\mathbb{R}^d\f$, not constrained to the surface
  \returns {u,v} as a vector to be consistent with multicomponent equations
*/
std::vector<PetscScalar> icGrayScott(const TVec<PetscReal>& x)
{
  PetscReal phi = PetscAtan2Real(x[1],x[0]);
  PetscReal sp = PetscSinReal(5.*(phi+0.5)),sm = PetscSinReal(5.*(phi-0.5));
  PetscScalar u = 1.0 - PetscExpReal(-8.*sp*sp);
  PetscScalar v = PetscExpReal(-10.*sm*sm);
  return {u,v};
}

const PetscReal gsPar_Du = 0.00006;
const PetscReal gsPar_Dv = 0.00003;
const PetscReal gsPar_F = 0.03;
const PetscReal gsPar_k = 0.06;
const PetscReal gsPar_rk = 0.001;
//! Reaction terms for Gray-Scott equation
/*! \param x Position in \f$\mathbb{R}^d\f$, constrained to the surface
  \param t Solution time when called
  \returns Reaction terms as a vector
*/
std::vector<PetscScalar> rhsGrayScott(const TVec<PetscReal>& x, PetscReal t, const PetscScalar* u)
{
  (void) t;
  PetscReal phi = PetscAtan2Real(x[1],x[0]);
  PetscScalar k = gsPar_k + gsPar_rk*PetscCosReal(phi);
  return {-u[0]*u[1]*u[1] + gsPar_F*(1.-u[0]), u[0]*u[1]*u[1] - (gsPar_F+k)*u[1]};
}

int main(int argc, char **args)
{
  // Initialize PETSc
  PetscInitialize(&argc,&args,(char*)0,helpStr);
  try {
    // Get input file name from command line
    PetscBool infSet;
    char inFile[100];
    PetscOptionsGetString(NULL, NULL, "-infile", inFile, sizeof(inFile), &infSet);
    // Problem Definition
    ProblemDefinition pd;
    if(infSet) {
      pd.setFromFile(inFile);
    } else {
      PP_WARN("An input file must be supplied\n");
      PetscFinalize();
      return 0;
    }
    pd.setFromCLI(); // let command line options override the input file
    pd.printConfig();
    // Global post processor
    CPPostProc globalProc(pd);
    // Differential operators
    // Note that time is not included here, and that each has a different
    // diffusivity coefficient
    GridFunc lapU("LaplacianSecondOrder", pd.mesh.dim, pd.mesh.delta, -gsPar_Du);
    GridFunc lapV("LaplacianSecondOrder", pd.mesh.dim, pd.mesh.delta, -gsPar_Dv);
    // Arrange the LHS operators into a vector to show that
    // this is a multicompenent equation
    std::vector<GridFunc*> grayScottOp = {&lapU, &lapV};
    DiffEq equation(pd, grayScottOp,&rhsGrayScott,&icGrayScott);
    // Construct global mesh, perform any partitioning required
    CPMeshGlobal globalMesh(pd, equation);
    // Construct global problem, and (O)RAS sub-problems if requested
    ProblemGlobal globalProblem(pd, globalMesh, equation, true);
    // Construct monitor context, this provides a hook to write solution files
    // as the solution progresses
    TsCtx tsMon(pd, globalMesh, globalProblem, globalProc);
    // Solve the problem
    globalProblem.solveTransient(&CPPostProc::tsMonitor, &tsMon);
  } catch(ErrorTrace& e) {
    e.printTrace();
  }
  PetscFinalize();
  
  return 0;
}
