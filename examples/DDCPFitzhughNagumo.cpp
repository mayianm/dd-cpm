// Set up and solve the Fitzhugh-Nagumo equation
// Author: Ian May
/*! \file DDCPFitzhughNagumo.cpp
    \brief This driver code shows how to setup and solve the Fitzhugh-Nagumo equations, \f$\partial_t u - D_u\Delta_{\mathcal{S}}u=(0.1-u)(u-1)u-v\f$, \f$\partial_t v - D_v\Delta_{\mathcal{S}}v=0.01(u/2-v)\f$, intrinsic to a surface \f$\mathcal{S}\f$, where \f$D_u\f$ and \f$D_v\f$ are positive diffusivities of the species.
*/

static char helpStr[] = "This example solves the Fitzhugh-Nagumo equation intrinsic to a surface. The diffusion terms are solved implicitly while the nonlinear reaction terms are handled explicitly.\n\
Consider solving on a circle with ARKIMEX3 and ORAS preconditioned GMRES as an implicit solver:\n\
    bin/DDCPFitzhughNagumo.ex -infile inputFiles/circle.icpm -cp_pc_ras -mesh_res 200 -mesh_nparts 12 -mesh_nover 4 -dd_trans_robfo -dd_osm_alpha 4 -time_final 600 -sub_pc_type ilu -ts_monitor\n\n\
or the same in parallel:\n\
    mpiexec -n 4 bin/DDCPFitzhughNagumo.ex -infile inputFiles/circle.icpm -cp_pc_ras -mesh_res 200 -mesh_nparts 12 -mesh_nover 4 -dd_trans_robfo -dd_osm_alpha 4 -time_final 600 -sub_pc_type ilu -ts_monitor\n\n\
or solve on a triangulated surface:\n\
    mpiexec -n 4 bin/DDCPFitzhughNagumo.ex -infile inputFiles/triang.icpm -cp_pc_ras -mesh_res 25 -mesh_nparts 12 -mesh_nover 2 -dd_trans_dirfo -time_final 250 -sub_pc_type ilu -ts_monitor\n\n";

// DDCPM library master header
#include "ddcpm.H"

// Library namespace
using namespace DDCPM;

//! Initial condition for Fitzhugh-Nagumo equation
/*! \param x Position in \f$\mathbb{R}^d\f$, not constrained to the surface
  \returns {u,v} as a vector to be consistent with multicomponent equations
*/
std::vector<PetscScalar> icFitzhughNagumo(const TVec<PetscReal>& x)
{
  PetscScalar u = 0, v = 0;
  if(x.getDim() == 2) {
    if(x[0]>0 && x[1]>0 && x.norm()>0.75) {
      u = 1.0;
    } else if(x[0]>0 && x[1]<0) {
      v = 1.0;
    }
  } else if(x.getDim() == 3) {
    if(x[0]>0 && x[1]>0 && x[2]>0) {
      u = 1.0;
    } else if(x[0]>0 && x[1]<0 && x[2]>0) {
      v = 1.0;
    }
  }
  return {u,v};
}

const PetscReal fnPar_Du = 1.e-4;
const PetscReal fnPar_Dv = 1.e-7;
//! Reaction terms for Fitzhugh-Nagumo equation
/*! \param x Position in \f$\mathbb{R}^d\f$, constrained to the surface
  \param t Solution time when called
  \returns Reaction terms as a vector
*/
std::vector<PetscScalar> rhsFitzhughNagumo(const TVec<PetscReal>& x, PetscReal t, const PetscScalar* u)
{
  (void) x;
  (void) t;
  return {(0.1-u[0])*(u[0]-1.)*u[0] - u[1], 0.01*(0.5*u[0] - u[1])};
}

int main(int argc, char **args)
{
  // Initialize PETSc
  PetscInitialize(&argc,&args,(char*)0,helpStr);
  try {
    // Get input file name from command line
    PetscBool infSet;
    char inFile[100];
    PetscOptionsGetString(NULL, NULL, "-infile", inFile, sizeof(inFile), &infSet);
    // Problem Definition
    ProblemDefinition pd;
    if(infSet) {
      pd.setFromFile(inFile);
    } else {
      PP_WARN("An input file must be supplied\n");
      PetscFinalize();
      return 0;
    }
    pd.setFromCLI(); // let command line options override the input file
    pd.printConfig();
    // Global post processor
    CPPostProc globalProc(pd);
    // Differential operators
    // Note that time is not included here, and that each has a different
    // diffusivity coefficient
    GridFunc lapU("LaplacianSecondOrder", pd.mesh.dim, pd.mesh.delta, -fnPar_Du);
    GridFunc lapV("LaplacianSecondOrder", pd.mesh.dim, pd.mesh.delta, -fnPar_Dv);
    // Arrange the LHS operators into a vector to show that
    // this is a multicompenent equation
    std::vector<GridFunc*> fitzNagOp = {&lapU, &lapV};
    DiffEq equation(pd,fitzNagOp,&rhsFitzhughNagumo,&icFitzhughNagumo);
    // Construct global mesh
    CPMeshGlobal globalMesh(pd, equation);
    // Construct global problem
    ProblemGlobal globalProblem(pd, globalMesh, equation, true);
    // Construct monitor context and solve problem
    TsCtx tsMon(pd, globalMesh, globalProblem, globalProc);
    globalProblem.solveTransient(&CPPostProc::tsMonitor, &tsMon);
  } catch(ErrorTrace& e) {
    e.printTrace();
  }
  PetscFinalize();
  
  return 0;
}
