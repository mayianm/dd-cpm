// Set up and solve a shifted Biharmonic equation
// Author: Ian May
/*! \file DDCPBiharmonic.cpp
    \brief This driver code shows how to setup and solve the shifted Biharmonic equation, \f$\left(c+\Delta^2_{\mathcal{S}}\right)u=f(x)\f$, intrinsic to a surface \f$\mathcal{S}\f$, with a positive real constant \f$c\f$.
*/

static char helpStr[] = "This example solves the shifted Biharmonic equation intrinsic to a surface.\n\
Consider solving on a circle with the petsc built in direct solver:\n\
    bin/DDCPBiharmonic.ex -infile inputFiles/circle.icpm -mesh_res 200 -mesh_nparts 1 -ksp_type preonly -pc_type lu -ksp_converged_reason\n\n\
or the same in parallel using the MUMPS parallel direct solver:\n\
    mpiexec -n 4 bin/DDCPBiharmonic.ex -infile inputFiles/circle.icpm -mesh_res 200 -mesh_nparts 1 -ksp_type preonly -pc_type lu -pc_factor_mat_type mumps -ksp_converged_reason\n\n\
or on a sphere:\n\
    mpiexec -n 4 bin/DDCPBiharmonic.ex -infile inputFiles/sphere.icpm -mesh_res 20 -mesh_nparts 1 -ksp_type preonly -pc_type lu -pc_factor_mat_type mumps -ksp_converged_reason\n\n";

// DDCPM library master header
#include "ddcpm.H"

// Library namespace
using namespace DDCPM;

//! Forcing function \f$f\f$ in the equation \f$(c+\Delta^2_{\mathcal{S}})u = f\f$
/*! \param x Position in \f$\mathbb{R}^d\f$, but is constrained to the surface
  \returns Value of \f$f\f$ as a vector to be consistent with multicomponent equations
*/
std::vector<PetscScalar> forcingFunc(const TVec<PetscReal>& x)
{
  const PetscReal pi = 4.*PetscAtan2Real(1., 1.);
  PetscReal phi = PetscAtan2Real(x[1], x[0]);
  PetscReal theta = x.getDim()>2 ? PetscAtan2Real(sqrt(x[0]*x[0]+x[1]*x[1]), x[2]) : 1.3;
  return {1.e4*theta*(theta-pi/2.)*(theta-pi)*PetscCosReal(7.*phi)};
}

// Main takes in CLI arguments and forwards them to PETSc to be parsed as needed
int main(int argc, char **args)
{
  // Initialize PETSc
  PetscInitialize(&argc,&args,(char*)0,helpStr);
  try {
    // Get input file name from command line
    PetscBool infSet;
    char inFile[100];
    PetscOptionsGetString(NULL, NULL, "-infile", inFile, sizeof(inFile), &infSet);
    // Problem Definition object
    // This reads in settings from the input file, applies overrides from the
    // command line, and passes these settings to all subsequent objects
    ProblemDefinition pd;
    if(infSet) {
      pd.setFromFile(inFile);
    } else {
      PP_WARN("An input file must be supplied\n");
      PetscFinalize();
      return 0;
    }
    pd.setFromCLI(); // Let the command line arguments override settings
    pd.printConfig(); // Dump settings to stdout for reference
    // Global post processor object
    // This writes solutions to in HDF5 format
    CPPostProc globalProc(pd);
    // Differential operator objects
    // The grid functions define the building blocks of the overall operator
    // They can be composed into more complicated grid functions, allowing
    // a nearly direct translation from PDE to discretization
    GridFunc laplacian("LaplacianSecondOrder", pd.mesh.dim, pd.mesh.delta, 1);
    GridFunc shift("Identity", pd.mesh.dim, pd.mesh.delta, 1.);
    GridFunc shiftBH = shift + laplacian*laplacian; // Total grid function for \f$c+\Delta^2_{\mathcal{S}}\f$
    // The equation combines the grid function with the forcing function
    DiffEq equation(pd, &shiftBH, &forcingFunc);
    // Construct a global mesh object
    // This finds all active and ghost nodes necessary to define the CPM discretization
    CPMeshGlobal globalMesh(pd, equation);
    // Construct global problem
    // This combines all of the above information to produce the global matrix,
    // right hand side, and DD sub-problems if desired
    ProblemGlobal globalProblem(pd, globalMesh, equation, false);
    // Solve problem
    globalProblem.solveStationary();
    // Write out the full solution and right hand side
    globalProc.writeData(0, 0, globalMesh, globalProblem);
  } catch(ErrorTrace& e) {
    e.printTrace();
  }
  PetscFinalize();
  
  return 0;
}
