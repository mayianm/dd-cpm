# Bring in petsc rules and variables
include ${PETSC_DIR}/lib/petsc/conf/variables
include ${PETSC_DIR}/lib/petsc/conf/rules

# Redefine cxx compiler if needed?
PETSC_CXXCOMPILE ?= ${CXX} -c ${CXX_FLAGS} ${CXXFLAGS} ${CXXCPPFLAGS} ${PSOURCECXX}

# set local directories
OBJDIR = obj
LIBDIR = lib
BINDIR = bin

# Grab all source files from src and examples
LIB_SOURCES = $(wildcard src/*.cpp)
EXAMPLE_SOURCES = $(wildcard examples/*.cpp)

# Generate object, dependency, and executable names
OBJ = $(patsubst src/%.cpp,$(OBJDIR)/%.o,$(LIB_SOURCES))
DEP = $(patsubst src/%.cpp,$(OBJDIR)/%.d,$(LIB_SOURCES))
EXAMPLES = $(patsubst examples/%.cpp,$(BINDIR)/%.ex,$(EXAMPLE_SOURCES))

.PHONY: examples clean test

examples: $(EXAMPLES)

clean::
	$(RM) $(OBJ) $(DEP) $(LIBDIR)/libcp.a $(EXAMPLES)

-include $(DEP)

$(OBJDIR)/%.o: src/%.cpp
	mkdir -p $(OBJDIR)
	$(PETSC_CXXCOMPILE) -g -MMD -MP -I ./include -c $< -o $@

$(OBJDIR)/%.o: examples/%.cpp
	mkdir -p $(OBJDIR)
	$(PETSC_CXXCOMPILE) -g -I ./include -c $< -o $@

$(LIBDIR)/libcp.a: $(OBJ)
	mkdir -p $(LIBDIR)
	$(AR) -crs $@ $^

$(BINDIR)/%.ex: $(OBJDIR)/%.o $(LIBDIR)/libcp.a
	mkdir -p $(BINDIR)
	$(CXXLINKER) -g $< -L $(LIBDIR) -lcp $(PETSC_LIB) -o $@

test: $(BINDIR)/DDCPPoisson.ex
	-chmod +x test/test.sh
	test/test.sh
